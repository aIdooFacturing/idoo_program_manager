const loadPage = () =>{
//	createMenuTree("maintenance", "Alarm_Manager")
	createMachineInfo()
	createBox()
	createCircleChartDiv()
	createAlarmDiv()
	createBarChartDiv()
}
	
const boxItemList = [
	["일일 생산 목표", "targetCnt"]
	, ["일일 생산량", "prdcCnt"]
	, ["사이클당 생산량", "cntPerCycle"]
	, ["일 사이클 횟수", "cycleCnt"]
	, ["일 평균 사이클 시간 (분)", "avrgCycleTime"]
	, ["일일 시간당 생산량", "prdcCntPerHour"]
	, ["Feed override", "feedOvrrd"]
	, ["Spindle load", "spdLd"]
];

const createBox = () =>{
	let boxes = ""
	$(boxItemList).each((idx, data) => {
		const boxWidth = getElSize(218 * 2)
		//const leftMargin = getElSize(16 * 2) + marginWidth
		
		
		const leftMargin = (boxWidth * idx) + (getElSize(16 * 2) * (idx + 1))
		
		boxes += 
			`
				<div id="${data[1]}>"
					style="
						width : ${boxWidth}px
						; height : ${getElSize(192 * 2)}px
						; background : url(${ctxPath}/images/FL/bg_info_board_daily.svg)
						; float : left
						; top : ${getElSize(274 * 2) + marginHeight}px
						; left : ${leftMargin + marginWidth}px
						; color : white
						; text-align : center
						; font-weight : bolder
						; font-size : ${getElSize(18 * 2)}px
						; position : absolute
					"
				>
					<span style="margin-top : ${getElSize(24 * 2)}px; display : inline-block">${data[0]}</span><Br>
					<span style="margin-top : ${getElSize(35 * 2)}px; display : inline-block; font-size : ${getElSize(60 * 2)}px; color : #00C6FF;">0</span>
				</div>
			`
	})
	
	$("#container").append(boxes)
} 

let dvcName = "DCM37100F";
let prgmHeader = "01";
let prgm = "SCHEDULE PROGRAM";

const createMachineInfo = () =>{
	const div = 
		`
			<img src="${ctxPath}/images/FL/default/bg_3rd_titlebar.svg"
				style="
					width : ${getElSize(1888 * 2)}px
					; top : ${getElSize(194 * 2) + marginHeight}px
					; left : ${getElSize(16 * 2) + marginWidth}px
					; position : absolute
				"
			>
			
			<img src="${ctxPath}/images/FL/icon/ico_mc.svg"
				style="
					width : ${getElSize(40 * 2)}px
					; top : ${getElSize(205 * 2) + marginHeight}px
					; left : ${getElSize(40 * 2) + marginWidth}px
					; position : absolute
				"
			>
			
			<div id="dvc_name_n_pgm_info"
				style="
					font-size : ${getElSize(24 * 2)}px
					; color : white
					; position : absolute
					; top : ${getElSize(214 * 2)}px
					; left : ${getElSize(99 * 2) + marginWidth}px
				"
			>
				<span style="color : #6699F0">${dvcName}</span> / [${prgmHeader}] ${prgm}
			</div>
			
		`
	$("#container").append(div)
	
	const arrow_left = 
		`
			<img src="${ctxPath}/images/FL/btn_arrow_left_default.svg" class="arrow"
				style = 
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(13 * 2)}px
						; top : ${getElSize(216.5 * 2) + marginHeight}px
						; left : ${getElSize(1664.5 * 2) + marginWidth}px
					"
			>
		`
	
	
	const arrow_right = 
		`
			<img src="${ctxPath}/images/FL/btn_arrow_right_default.svg" class="arrow"
				style = 
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(13 * 2)}px
						; top : ${getElSize(216.5 * 2) + marginHeight}px
						; left : ${getElSize(1864.5 * 2) + marginWidth}px
					"
			>
		`
	$("#container").append(arrow_left, arrow_right)	
	
	$(".arrow").hover((el)=>{
		const src = $(el.currentTarget).attr("src").replace("default", "pushed")
		$(el.currentTarget).attr("src", src)
	}, (el)=>{
		const src = $(el.currentTarget).attr("src").replace("pushed", "default")
		$(el.currentTarget).attr("src", src)
	})
	
	const date = 
		`
			<span
				id="date"
				style=
					"
						color : white
						; position : absolute
						; z-index : 2
						; font-size : ${getElSize(24 * 2)}px
						; top : ${getElSize(213 * 2) + marginHeight}px
						; left : ${getElSize(1709 * 2) + marginWidth}px
					"
			>
				${getToday().substr(0,10)}
			</span>
		`
	$("#container").append(date)			
}

const createCircleChartDiv = () =>{
	const div = 
		`
			<div
				style="
					width : ${getElSize(686 * 2)}px
					; height : ${getElSize(582 * 2)}px
					; background : url(${ctxPath}/images/FL/bg_monitoring_3.svg)
					; position : absolute
					; top : ${getElSize(482 * 2)}px
					; left : ${getElSize(16 * 2) + marginWidth}px
				"
			>
				
			</div>
		`
	$("#container").append(div)
	
	const title = 
		`
			<span style = 
				"
					position : absolute
					; font-weight : bolder
					; z-index : 2
					; font-size  : ${getElSize(24 * 2)}px
					; top : ${getElSize(506 * 2) + marginHeight}px
					; left : ${getElSize(48 * 2) + marginWidth}px
					; color : white;
					
				"
			>
				일 누적 가동 현황
			</span>
		`
	$("#container").append(title)
	
	creataStatusLabel()
}

const creataStatusLabel = () =>{
	const incycleLabel = 
		`
			<img src="${ctxPath}/images/FL/incycle_label.svg"
				style=
				"
					position : absolute
					; width : ${getElSize(240 * 2)}px
					; top : ${getElSize(562 * 2) + marginHeight}px
					; left : ${getElSize(446 * 2) + marginWidth}px
					; z-index : 2
				"
			>
		`
	const waitLabel = 
		`
			<img src="${ctxPath}/images/FL/wait_label.svg"
				style=
				"
					position : absolute
					; width : ${getElSize(240 * 2)}px
					; top : ${getElSize(658 * 2) + marginHeight}px
					; left : ${getElSize(446 * 2) + marginWidth}px
					; z-index : 2
				"
			>
		`
	const alarmLabel = 
		`
			<img src="${ctxPath}/images/FL/alarm_label.svg"
				style=
				"
					position : absolute
					; width : ${getElSize(240 * 2)}px
					; top : ${getElSize(706 * 2) + marginHeight}px
					; left : ${getElSize(446 * 2) + marginWidth}px
					; z-index : 2
				"
			>
		`
	const noConnLabel = 
		`
			<img src="${ctxPath}/images/FL/noConn_label.svg"
				style=
				"
					position : absolute
					; width : ${getElSize(240 * 2)}px
					; top : ${getElSize(754 * 2) + marginHeight}px
					; left : ${getElSize(446 * 2) + marginWidth}px
					; z-index : 2
				"
			>
		`	
		
	const modeLabel = 
		`
		<img src="${ctxPath}/images/FL/mode_auto_label.svg"
				style=
				"
					position : absolute
					; width : ${getElSize(206 * 2)}px
					; height : ${getElSize(48 * 2)}px
					; top : ${getElSize(498 * 2) + marginHeight}px
					; left : ${getElSize(480 * 2) + marginWidth}px
					; z-index : 2
				"
			>
		`

	const mode_title = 
		`
			<span
				style=
				"
					position : absolute
					; font-size : ${getElSize(16 * 2)}px
					; top : ${getElSize(512 * 2) + marginHeight}px
					; left : ${getElSize(492 * 2) + marginWidth}px
					; z-index : 2
					; color : white
				"
			>MODE</span>
		`
	const auto_title = 
		`
			<span
				style=
				"
					position : absolute
					; font-size : ${getElSize(16 * 2)}px
					; top : ${getElSize(512 * 2) + marginHeight}px
					; left : ${getElSize(568 * 2) + marginWidth}px
					; z-index : 2
					; color : black
				"
			>AUTOMATIC</span>
		`
		
	$("#container").append(incycleLabel, waitLabel, alarmLabel, noConnLabel, modeLabel, mode_title, auto_title)
	
	const incycle_title = 
		`
			<span class="normal_font_weight"
				style=
				"
					position : absolute
					; font-size : ${getElSize(18 * 2)}px
					; top : ${getElSize(569 * 2) + marginHeight}px
					; left : ${getElSize(518 * 2) + marginWidth}px
					; z-index : 2
					; color : white
				
				"
			>In-cycle</span>
		`
	const cut_title = 
		`
			<span class="normal_font_weight"
				style=
				"
					position : absolute
					; font-size : ${getElSize(18 * 2)}px
					; top : ${getElSize(614 * 2) + marginHeight}px
					; left : ${getElSize(518 * 2) + marginWidth}px
					; z-index : 2
					; color : white
				
				"
			>Cut</span>
		`
	const wait_title = 
		`
			<span class="normal_font_weight"
				style=
				"
					position : absolute
					; font-size : ${getElSize(18 * 2)}px
					; top : ${getElSize(665 * 2) + marginHeight}px
					; left : ${getElSize(518 * 2) + marginWidth}px
					; z-index : 2
					; color : white
				
				"
			>STOP</span
		`
	const alarm_title = 
		`
			<span class="normal_font_weight"
				style=
				"
					position : absolute
					; font-size : ${getElSize(18 * 2)}px
					; top : ${getElSize(713 * 2) + marginHeight}px
					; left : ${getElSize(518 * 2) + marginWidth}px
					; z-index : 2
					; color : white
				
				"
			>Alarm</span
		`	
	const noConn_title = 
		`
			<span class="normal_font_weight"
				style=
				"
					position : absolute
					; font-size : ${getElSize(18 * 2)}px
					; top : ${getElSize(761 * 2) + marginHeight}px
					; left : ${getElSize(518 * 2) + marginWidth}px
					; z-index : 2
					; color : white
				
				"
			>None</span
		`	
	$("#container").append(incycle_title, cut_title, wait_title, alarm_title, noConn_title)
	
	
	const incycle_hr = 
		`
			<span
				style=
				"
					color : white
					; font-size : ${getElSize(18 * 2)}px
					; left : ${getElSize(657 * 2) + marginWidth}px
					; top : ${getElSize(573 * 2) + marginHeight}px  
					; position : absolute
					; z-index : 2
				"
			>
				hr
			</span>
		`
	const cut_hr = 
		`
			<span
				style=
				"
					color : white
					; font-size : ${getElSize(18 * 2)}px
					; left : ${getElSize(657 * 2) + marginWidth}px
					; top : ${getElSize(618 * 2) + marginHeight}px  
					; position : absolute
					; z-index : 2
				"
			>
				hr
			</span>
		`
	const wait_hr = 
		`
			<span
				style=
				"
					color : white
					; font-size : ${getElSize(18 * 2)}px
					; left : ${getElSize(657 * 2) + marginWidth}px
					; top : ${getElSize(669 * 2) + marginHeight}px  
					; position : absolute
					; z-index : 2
				"
			>
				hr
			</span>
		`
	const alarm_hr = 
		`
			<span
				style=
				"
					color : white
					; font-size : ${getElSize(18 * 2)}px
					; left : ${getElSize(657 * 2) + marginWidth}px
					; top : ${getElSize(717 * 2) + marginHeight}px  
					; position : absolute
					; z-index : 2
				"
			>
				hr
			</span>
		`
	const noConn_hr = 
		`
			<span
				style=
				"
					color : white
					; font-size : ${getElSize(18 * 2)}px
					; left : ${getElSize(657 * 2) + marginWidth}px
					; top : ${getElSize(765 * 2) + marginHeight}px  
					; position : absolute
					; z-index : 2
				"
			>
				hr
			</span>
		`			
	$("#container").append(incycle_hr, cut_hr, wait_hr, alarm_hr, noConn_hr)
	
	
	const incycleVal = 
		`
			<span id="incycleVal"
				style=
					"
						position : absolute
						; z-index : 2
						; font-size : ${getElSize(32 * 2)}px
						; color : #00C6FF
						; top : ${getElSize(565 * 2) + marginHeight}px
						; left : ${getElSize(602 * 2) + marginWidth}px
					"
			>
				0.0
			</span>
		`
	const cutVal = 
		`
			<span id="cutVal"
				style=
					"
						position : absolute
						; z-index : 2
						; font-size : ${getElSize(32 * 2)}px
						; color : #00C6FF
						; top : ${getElSize(610 * 2) + marginHeight}px
						; left : ${getElSize(602 * 2) + marginWidth}px
					"
			>
				0.0
			</span>
		`		
	$("#container").append(incycleVal, cutVal)	
	
	const waitVal = 
		`
			<span id="waitVal"
				style=
					"
						position : absolute
						; z-index : 2
						; font-size : ${getElSize(32 * 2)}px
						; color : #00C6FF
						; top : ${getElSize(661 * 2) + marginHeight}px
						; left : ${getElSize(602 * 2) + marginWidth}px
					"
			>
				0.0
			</span>
		`	
	const alarmVal = 
		`
			<span id="alarmVal"
				style=
					"
						position : absolute
						; z-index : 2
						; font-size : ${getElSize(32 * 2)}px
						; color : #00C6FF
						; top : ${getElSize(709 * 2) + marginHeight}px
						; left : ${getElSize(602 * 2) + marginWidth}px
					"
			>
				0.0
			</span>
		`	
		
	const noConnVal = 
		`
			<span id="noConnVal"
				style=
					"
						position : absolute
						; z-index : 2
						; font-size : ${getElSize(32 * 2)}px
						; color : #00C6FF
						; top : ${getElSize(757 * 2) + marginHeight}px
						; left : ${getElSize(602 * 2) + marginWidth}px
					"
			>
				0.0
			</span>
		`			
	$("#container").append(incycleVal, cutVal, waitVal, alarmVal, noConnVal)
}

const createAlarmDiv = () =>{
	const div = 
		`
			<div
				style="
					width : ${getElSize(1188 * 2)}px
					; height : ${getElSize(144 * 2)}px
					; background : url(${ctxPath}/images/FL/bg_monitoring_1.svg)
					; position : absolute
					; top : ${getElSize(482 * 2) + marginHeight}px
					; left : ${getElSize(718 * 2) + marginWidth}px
				"
			>
				
			</div>
		`
	$("#container").append(div)
	
	const alarm_tag = 
		`
			<img src="${ctxPath}/images/FL/ico_alarm_message.svg"
				style = 
				"
					position : absolute
					; z-index : 2
					; width : ${getElSize(72 * 2)}px
					; height : ${getElSize(24 * 2)}px
					; top : ${getElSize(501 * 2) + marginHeight}px
					; left : ${getElSize(734 * 2) + marginWidth}px
				"
			
			>
			
		`
	
	
	const alarm_title = 
		`
			<span
				style = 
				"
					position : absolute
					; z-index : 2
					; font-size : ${getElSize(16 * 2)}px
					; color : black
					; top : ${getElSize(505 * 2) + marginHeight}px
					; left : ${getElSize(748 * 2) + marginWidth}px					
				"
			>
				Alarm
			</span>
		`
	
	const alarm_div = 
		`
			<div id="alarm_div"
				style = 
				"
					position : absolute
					; z-index : 2
					; width : ${getElSize(1059 * 2)}px
					; height : ${getElSize(100 * 2)}px
					; top : ${getElSize(501 * 2) + marginHeight}px
					; left : ${getElSize(814 * 2) + marginWidth}px
					; color : white
					; font-size : ${getElSize(18 * 2)}px
					; overflow : auto
					
				"
			>
			
			</div>	
		`
	$("#container").append(alarm_tag, alarm_title, alarm_div)		
	
	$("#alarm_div").html(alarm_msg)
	
	
	$("body::-webkit-scrollbar-track").css({
		"-webkit-box-shadow" : "inset 0 0 6px rgba(0,0,0,0.3)",
		"border-radius" : "10px",
		"background-color" : "#F5F5F5"
	})

	$("body::-webkit-scrollbar-thumb").css({
		"border-radius" : "10px",
		"-webkit-box-shadow" : "inset 0 0 6px rgba(0,0,0,.3)",
		"background-color" : "#555"
	})

	$("body::-webkit-scrollbar").css({
		"width" : "12px",
		"background-color" : "#F5F5F5"
	})
}

const alarm_msg = "PS5074 - 어드레스 지령이 중복 되었습니다.<br><br>PS1016 - EOB가 없음.<br><br><Br>SR1960 - 호출 에러(메모리 카드)<br>PS0064 - 사상형상이 간단하게 변하지 않습"
	
const createBarChartDiv = () =>{
	const div = 
		`
			<div
				style="
					width : ${getElSize(1188 * 2)}px
					; height : ${getElSize(422 * 2)}px
					; background : url(${ctxPath}/images/FL/bg_monitoring_2.svg)
					; position : absolute
					; top : ${getElSize(642 * 2)}px
					; left : ${getElSize(718 * 2) + marginWidth}px
				"
			>
				
			</div>
		`
	const title = 
		`
			<span style = 
				"
					position : absolute
					; font-weight : bolder
					; z-index : 2
					; font-size  : ${getElSize(24 * 2)}px
					; top : ${getElSize(658 * 2) + marginHeight}px
					; left : ${getElSize(734 * 2) + marginWidth}px
					; color : white;
					
				"
			>
				일 가동 현황
			</span>
		`
		
	const spdOvvrd_label = 
		`
			<img src="${ctxPath}/images/FL/info_spindle_over.svg"
				style =
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(32 * 2)}px
						; top : ${getElSize(1035 * 2) + marginHeight}px
						; left : ${getElSize(1507 * 2) + marginWidth}px 
					"
			>
		`
	const spdOvvrd_title = 
		`
			<span
				style =
					"
						position : absolute
						; z-index : 2
						; color : white
						; font-size : ${getElSize(18 * 2)}px
						; top : ${getElSize(1028 * 2) + marginHeight}px
						; left : ${getElSize(1560.5 * 2) + marginWidth}px 
					"
			>
				Spindle Override
			</span>
		`		
		
	const spdLd_label = 
		`
			<img src="${ctxPath}/images/FL/info_spindle_load.svg"
				style =
					"
						position : absolute
						; z-index : 2
						; width : ${getElSize(32 * 2)}px
						; top : ${getElSize(1035 * 2) + marginHeight}px
						; left : ${getElSize(1714.5 * 2) + marginWidth}px 
					"
			>
		`	
	const spdLd_title = 
		`
			<span
				style =
					"
						position : absolute
						; z-index : 2
						; color : white
						; font-size : ${getElSize(18 * 2)}px
						; top : ${getElSize(1028 * 2) + marginHeight}px
						; left : ${getElSize(1770.5 * 2) + marginWidth}px 
					"
			>
				Spindle Load
			</span>
		`			
	$("#container").append(div, title, spdOvvrd_label, spdOvvrd_title, spdLd_label, spdLd_title)
}
$(document).ready(function () {
    $('body').find('.tree').fadeOut(0);
    
//    $('body').find('.tree-item').append($('<a/>', {
//    	href : "https://www.google.co.kr",
//		title : '클릭하면 구글로 갑니당 !'
//    }));

    $('.tree-title').click(function () {
        setStatus($(this));
    });
    
//    $(document).on('click', '.tree-item', (function(){
//    	$(this).append($('<a/>', {
//			href : "https://www.google.co.kr",
//			title : '클릭하면 구글로 갑니당 !'
//    	}));
//    }));
  
});

/**
 * Set the list opened or closed
 * */
function setStatus(node){
    var elements = [];
    $(node).each(function(){
        elements.push($(node).nextAll());
    });
    for (var i = 0; i < elements.length; i++) {
        if (elements[i].css('display') == 'none'){
            elements[i].fadeIn(0);
        }else{
            elements[i].fadeOut(0);
        }
    }
    if (elements[0].css('display') != 'none') {
        $(node).addClass('active');
    }else{
        $(node).removeClass('active');
    }
}

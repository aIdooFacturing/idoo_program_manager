var appVer = "v.1.0 ";
var shopId = 1;

var targetWidth = 3840;
var targetHeight = 2160;

var originWidth = window.innerWidth;
var originHeight = window.innerHeight;

var contentWidth = originWidth;
var contentHeight = targetHeight/(targetWidth/originWidth);

var screen_ratio = getElSize(240);

if(originHeight/screen_ratio<9){
	contentWidth = targetWidth/(targetHeight/originHeight)
	contentHeight = originHeight; 
};

function getElSize(n){
	return contentWidth/(targetWidth/n);
};

function setElSize(n) {
	return Math.floor(targetWidth / (contentWidth / n));
};

var marginWidth = (originWidth-contentWidth)/2;
var marginHeight = (originHeight-contentHeight)/2;





const chkKeyEvt = (evt) =>{
	if(evt.keyCode == 13){
		login()
	}
}

function login(){
	var url = ctxPath + "/login.do";
	var $id = $("#id").val();
	var $pwd = $("#pwd").val();
	var param = "id=" + $id + 
				"&pwd=" + $pwd + 
				"&shopId=" + shopId;
	
	$("body").loading("start")
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			$("body").loading("stop")
			console.log(data.message)
			if(data.message=="success"){
				setCookie("login_time", new Date().getTime(), 1);
				setCookie("login", true, 1);
				setCookie("user_id", $id, 1);
				
				//loginSuccess();
				//drawLogout();
				location.href = ctxPath + "/main.do"
			}else{
				alert("계정 정보가 올바르지 않습니다.")
				//$("#errMsg").html("계정 정보가 올바르지 않습니다.")
			}
		}
	});
};


function chkKeyCd(evt){
	if(evt.keyCode==13) login();
};


let appName = "";
	


const monitor_menu_map = new JqMap();
monitor_menu_map.put("Dashboard", ["전체 장비 가동 상태", "/iDOO_Dashboard/index.do"])
monitor_menu_map.put("Single_Chart_Status", ["장비별 일 생산 / 가동 현황", "/iDOO_Single_Chart_Status/index.do"])
monitor_menu_map.put("24hrChart", ["전체 장비 24hr chart", "/iDOO_24hrChart/index.do"])

const analysis_menu_map = new JqMap();
analysis_menu_map.put("Performance_Report_Chart", ["가동 실적 분석", "/iDOO_Performance_Report_Chart/index.do"])
analysis_menu_map.put("Performance_Report_Daily", ["장비열 일 생산 / 장비별 일가동 실적 분석", "/iDOO_Performance_Report_Daily/index.do"])

const im_menu_map = new JqMap();
const kpi_menu_map = new JqMap();
const qm_menu_map = new JqMap();
const tm_menu_map = new JqMap();
const om_map = new JqMap();

const maintenance_map = new JqMap();
maintenance_map.put("Alarm_Manager", ["장비 알람 이력", "/iDOO_Alarm_Manager/index.do"])
maintenance_map.put("Program_Manager", ["프로그램 관리", "/iDOO_Program_Manager/index.do"])

const config_map = new JqMap();
config_map.put("Device_Status", ["보유 장비 현황", "/iDOO_Device_Status/index.do"])
config_map.put("Catch_Phrase", ["Catch Phrase", "/iDOO_Catch_Phrase/index.do"])
config_map.put("Account", ["계정설정", "/iDOO_account/index.do"])


const menu_tree = new JqMap()
menu_tree.put("monitoring",monitor_menu_map)
menu_tree.put("ayalysis", analysis_menu_map)

menu_tree.put("im",[])
menu_tree.put("kpi",[])
menu_tree.put("qm",[])
menu_tree.put("tm",[])
menu_tree.put("om",[])

menu_tree.put("maintenance",maintenance_map)
menu_tree.put("config", config_map)

	
const createMenuTree = (menu, cat) =>{
	drawMainIcon(menu)
	
	const catList = menu_tree.get(menu).keys()
	
	let cat_div = 
		`
			<div id="cat_div" style="
				width : ${getElSize(1920 * 2)}px
				; height : ${getElSize(64 * 2)}px
				; top : ${getElSize(114 * 2) + marginHeight}px
				; left : ${getElSize(0) + marginWidth}px
				; position : absolute
			"
			></div
		`
	$("#container").append(cat_div)
		
	let cat_title = ""
	$(catList).each((idx, data) =>{
		let color = "white";
		let weight = "normal";
		let firstMargin = 0
		
		if(data.toLowerCase() == cat.toLowerCase()){
			color = "#00C6FF";
			weight = "bolder"
		}
		
		if(idx == 0){
			firstMargin = getElSize(286 * 2)
		}
		
		cat_title += 
			`
			<span style="
				color : ${color}
				; font-size : ${getElSize(24 * 2)}px
				; margin-left :  ${getElSize(20 * 2) + firstMargin}px
				; margin-right :  ${getElSize(20 * 2)}px
				; cursor : pointer
				; font-weight : ${weight}
				; margin-top : ${getElSize(20 * 2)}px
				; display : inline-block
				
				"
				 onclick="movePage('${menu_tree.get(menu).get(data)[1]}')"
				>
					${menu_tree.get(menu).get(data)[0]}
			</span>
			
			`
	})
	
	$("#cat_div").append(cat_title)	
}

const movePage = (url) =>{
	console.log(url)
}

const indexPath = "/iDOO";


function drawFlag(){
	var ko = "<img src=" + ctxPath + "/images/ko.png id='ko' class='flag'/>";
	var cn = "<img src=" + ctxPath + "/images/cn.png id='cn' class='flag'/>";
	var en = "<img src=" + ctxPath + "/images/en.png id='en' class='flag'/>";
	var de = "<img src=" + ctxPath + "/images/de.png id='de' class='flag'/>";
	
	var div = "<div id='flagDiv'>" + cn 
									+ ko 
									+ en 
									+ de
									+ "</div>";
	$("body").prepend(div);
	
	$("#flagDiv").css({
		"position" : "absolute",
		"left" : marginWidth,
		"top" : marginHeight + getElSize(20)
	});
	
	$(".flag").css({
		"width" : getElSize(100),
		"cursor" : "pointer"
	}).click(changeLang);
	
	$(".flag").css("filter", "grayscale(100%)")
	var lang = window.localStorage.getItem("lang");
	$("#" +lang).css("filter", "grayscale(0%)");
};


function changeLang(){
	var lang = this.id;
	var url = window.location.href;
	var param = "?lang=" + lang;
	
	window.localStorage.setItem("lang", lang)
	
	if(url.indexOf("fromDashBoard")!=-1){
		param = "&lang=" + lang;
		if(url.indexOf("&lang")!=-1){
			url = url.substr(0, url.lastIndexOf("&lang"));
		}
	}else{
		url = url.substr(0, url.lastIndexOf("?"))
	}
	
	location.href = url + param;
	
	$(".flag").css("filter", "grayscale(100%)")
	$("#" +lang).css("filter", "grayscale(0%)");
}

function decode(str){
	return decodeURIComponent(str).replace(/\+/gi, " ")
};


function showCorver(){
	$("#corver").css({
		"z-index" : 9999,
		"background-color" : "black",
		//"opacity" : 0.6
	});
};

function closeCorver(){
	$("#corver").css({
		"z-index" : -999,
	});
}


function getBanner(){
	var url = ctxPath + "/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" : - window.innerWidth
				})
				
				$("#intro").html(data.msg).css({
					"right" : - $("#intro").width()
				})
			}
			
			bannerWidth = $("#intro").width() + getElSize(100); 
			$("#intro").width(bannerWidth);
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			//twinkle();
			bannerAnim();
		}
	});		
}

var twinkle_opct = false;
function twinkle(){
	var opacity;
	if(twinkle_opct){
		opacity = 0;
	}else{
		opacity = 1;
	}
	$("#intro").css("opacity",opacity);
	
	twinkle_opct = !twinkle_opct;
	setTimeout(twinkle, 300)
};

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth - getElSize(10));
	$("#intro").animate({
		"right" : window.innerWidth  - getElSize(100)
	},8000 * 2, function(){
		$("#intro").css("right" , - $("#intro").width())
		//$(this).remove();
		bannerAnim();
	});
};

function chkBanner(){
	if(window.localStorage.getItem("banner")=="true"){
		//getBanner();
		$("#intro_back").css("display","block");
	}else{
		$("#intro").html("");
		$("#intro_back").css("display","none");
	}
};


const setElement = () =>{
	$("#container").css({
		//"background-color" : "f",
		"width" : contentWidth,
		"height" : contentHeight,
		"margin-top" : (marginHeight +getElSize(80 * 2)+getElSize(15 * 2)),
		"margin-left" : marginWidth
	})
	
}

let handle = 0;
const time = () =>{
	$("#time").html(getToday());
	 handle = requestAnimationFrame(time)
};

const getToday = () =>{
	const date = new Date();
	const year = date.getFullYear();
	const month = addZero(String(date.getMonth() + 1))
    const day = addZero(String(date.getDate()))

    const hour = addZero(String(date.getHours()))
    const minute = addZero(String(date.getMinutes()))
    const second = addZero(String(date.getSeconds()))

    return year + "." + month + "." + day + " " + hour + ":" + minute + ":" + second
}




const createHeader = () =>{
	 const div = `
		 			<div id="header"
		 			style=
		 			"
		 				height : ${getElSize(68)}px
		 				; background: #000000
		 			"
		 			>
		 				
		 			<img id="clock" src="${ctxPath}/images/FL/default/ico_clock.svg" 
		 					style ="
		 						width : ${getElSize(15 * 2)}px
		 						; height : ${getElSize(15 * 2) + marginHeight}px
		 						; position : absolute
		 						; top : ${getElSize(7 * 2)}px
		 						; left : ${getElSize(1645 * 2) + marginWidth}px
		 					"
		 				>
		 				
		 				<div id="time"
		 					style = "
		 						font-size : ${getElSize(18 * 2)}px
		 						; position : absolute
		 						; width : ${getElSize(170 * 2)}px
		 						; top : ${getElSize(4 * 2) + marginHeight}px
		 						; left : ${getElSize(1668 * 2) + marginWidth}px
		 						; color : #ffffff
		 						
		 					"
		 				>두산 공작기계_Tiny</div>
		 				<div id="lang"
		 					style = "
		 						font-size : ${getElSize(18 * 2)}px
		 						; position : absolute
		 						; top : ${getElSize(4 * 2) + marginHeight}px
		 						; left : ${getElSize(1848 * 2) + marginWidth}px
		 						; color : #ffffff
		 						; text-decoration : underline
		 						; color : #09E1FF
		 						; cursor : pointer
		 						
		 					"
		 				>KOR</div>
		 			</div>
		 		`
	$("#container").prepend(div)	 
	
	if(is_login){
		let logout = 
			`
				<img id="logout" src="${ctxPath}/images/FL/default/btn_logout_default.svg" 
 					style ="
 						width : ${getElSize(86 * 2)}px
 						; height : ${getElSize(26 * 2)}px
 						; top : ${getElSize(4 * 2) + marginHeight}px
 						; left : ${getElSize(8 * 2) + marginWidth}px
 						; cursor : pointer;
 						; position : absolute
 					"
 				>
			`
			
		let comName = 
			`
			<div id="comName"
				style = "
					font-size : ${getElSize(18 * 2)}px
					; position : absolute
					; top : ${getElSize(4 * 2) + marginHeight}px
					; left : ${getElSize(1474 * 2) + marginWidth}px
					; color : #ffffff
					
				"
			>두산 공작기계_Tiny</div>	
			`
		$("#header").append(logout, comName)
		
	}
}

const createTitle = () =>{
	const div =
		`
			
		 		
		 				<img id="blue_bar" src="${ctxPath}/images/FL/default/title_blue_bar.svg" 
		 					style ="
		 						position : absolute
		 						; width : ${getElSize(290 * 2)}px
		 						; height : ${getElSize(80 * 2)}px
		 						; top : ${getElSize(34 * 2) + marginHeight}px
		 						; left : ${getElSize(0 * 2) + marginWidth}px
		 						; z-index : 2
		 				
		 					"
		 				>	
		 				<img id="title_bar" src="${ctxPath}/images/FL/default/bg_2nd_titlebar.svg" 
		 					style ="
		 						width : ${getElSize(1920 * 2)}px
		 						; top : ${getElSize(34 * 2) + marginHeight}px
		 						; left : ${getElSize(0 * 2) + marginWidth}px
		 						; position : absolute
		 					"
		 				>
		 				
		 	
		`
		
		
	$("#container").append(div)
	
	const middle_bar = 
		`<img id="middle_bar" 
			style = "
				width : ${getElSize(1920 * 2)}px
				
				; position : absolute
				; left : ${getElSize(0) + marginWidth}px
				; top : ${getElSize(114 * 2) + marginHeight}px
				
			
			"
			src="${ctxPath}/images/FL/default/bg_middle_bar.svg"
		 > 		
		`
		
	$("#container").append(middle_bar)
	
	
	
	const factory_logo = 
		`
			<img src="${ctxPath}/images/FL/default/ico_factory.svg"
				style = 
				"
					position : absolute
					; z-index : 2
					; width : ${getElSize(42 * 2)}px
					; left : ${getElSize(323 * 2) + marginWidth}px
					; top : ${getElSize(62 * 2) + marginHeight}px
				"
			>
		`
		
	const idoo_control_logo = 
		`
			<img src="${ctxPath}/images/FL/logo/aidoo_control_h_w.svg"
				style = 
				"
					position : absolute
					; z-index : 2
					; width : ${getElSize(212.4 * 2)}px
					; left : ${getElSize(1679 * 2) + marginWidth}px
					; top : ${getElSize(52 * 2) + marginHeight}px
				"
			>
		`
		
	const factory_title = 
		`
			<span
				style = 
				"
					position : absolute
					; z-index : 2
					; font-size : ${getElSize(30 * 2)}px
					; top : ${getElSize(62 * 2) + marginHeight}px
					; left : ${getElSize(378 * 2) + marginWidth}px
					; color : #00C6FF
				"
			>
			두산 공작기계
			</span>
		`
		
	const factory_sub_title = 
		`
			<span
				style = 
				"
					position : absolute
					; z-index : 2
					; font-size : ${getElSize(24 * 2)}px
					; top : ${getElSize(65 * 2) + marginHeight}px
					; left : ${getElSize(560 * 2) + marginWidth}px
					; color : #ffffff
					; opacity : 0.7
				"
			>
			(창원 남산 공장)
			</span>
		`		
	$("#container").append(idoo_control_logo, factory_logo, factory_title, factory_sub_title)
	
	//widget icons
	
	const home_ic = 
		`
			<img src="${ctxPath}/images/FL/default/btn_home_default.svg" class="widget" action="home"
				style = 
				"
					position : absolute
					; z-index : 2
					; width : ${getElSize(48 * 2)}px
					; left : ${getElSize(16 * 2) + marginWidth}px
					; top : ${getElSize(50 * 2) + marginHeight}px
					; cursor : pointer
				"
			>
		`
	const undo_ic = 
		`
			<img src="${ctxPath}/images/FL/default/btn_undo_default.svg" class="widget" action="undo"
				style = 
				"
					position : absolute
					; z-index : 2
					; width : ${getElSize(48 * 2)}px
					; left : ${getElSize(72 * 2) + marginWidth}px
					; top : ${getElSize(50 * 2) + marginHeight}px
					; cursor : pointer
				"
			>
		`	
	const touchlock_ic = 
		`
			<img src="${ctxPath}/images/FL/default/btn_touchlock_default.svg" class="widget" action="touchLock"
				style = 
				"
					position : absolute
					; z-index : 2
					; width : ${getElSize(48 * 2)}px
					; left : ${getElSize(128 * 2) + marginWidth}px
					; top : ${getElSize(50 * 2) + marginHeight}px
					; cursor : pointer
				"
			>
		`		
	const capture_ic = 
		`
			<img src="${ctxPath}/images/FL/default/btn_capture_default.svg" class="widget" action="capture"
				style = 
				"
					position : absolute
					; z-index : 2
					; width : ${getElSize(48 * 2)}px
					; left : ${getElSize(184 * 2) + marginWidth}px
					; top : ${getElSize(50 * 2) + marginHeight}px
					; cursor : pointer
				"
			>
		`				
	$("#container").append(home_ic, undo_ic, touchlock_ic, capture_ic)
	
	$(".widget").hover((el) =>{
		const src = $(el.currentTarget).attr("src").replace("default.svg", "pushed.svg")
		$(el.currentTarget).attr("src", src)
	}, (el) =>{
		const src = $(el.currentTarget).attr("src").replace("pushed.svg", "default.svg")
		$(el.currentTarget).attr("src", src)
	}).click((el)=>{
		const action = $(el.currentTarget).attr("action")
	
		if(action == "home"){
			location.href = indexPath + "/main.do"
		}
	})
}

const drawMainIcon = (ic) =>{
	const img = 
		`
			<img src="${ctxPath}/images/FL/menu/${ic}.svg"
				style = 
				"
					width : ${getElSize(40 * 2)}px
					; height : ${getElSize(40 * 2)}px
					; position : absolute
					; top : ${getElSize(126 * 2) + marginHeight}px
					; left : ${getElSize(12 * 2) + marginWidth}px
					; z-index : 2
				"
			>
		`
	
	const title = 
		`
			<div id="title"
				style = "
					font-size : ${getElSize(20 * 2)}px
					; position : absolute
					; top : ${getElSize(134 * 2) + marginHeight}px
					; left : ${getElSize(60 * 2) + marginWidth}px
					; color : #ffffff
					; z-index : 2
					
				"
			>${ic.toUpperCase()}</div>
		`
		
	$("#container").append(img, title)
}

const bindEvt = () =>{
	$("#logout").off().on("click", ()=>{
		deleteCookie("login")
		deleteCookie("login_time")
		deleteCookie("user_id")
		
		window.sessionStorage.removeItem("login_time");
		location.href = ctxPath + "/index.do"
	})
	
	$("#logout").hover(()=>{
		$("#logout").attr("src", ctxPath + "/images/FL/default/btn_logout_pushed.svg")
	}, ()=>{
		$("#logout").attr("src", ctxPath + "/images/FL/default/btn_logout_default.svg")
	})
};

const chkLogin = () =>{
	if(!is_login && location.href.indexOf(indexPath + "/index.do") == -1){
		
		location.href = indexPath + "/index.do"
	}
}

const getComName = () => {
	var url = ctxPath + "/chart/getComName.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			$("#title_right").html(decode(data));
		}
	});
};

const addZero = (str) => {
	if(str.length==1) str = "0" + str;
	return str;
}

const setCookie = (name, value, exp)  => {
	var date = new Date();
    date.setTime(date.getTime() + exp * 24 * 60 * 60 * 1000);
    //date.setTime(date.getTime() + 60 * 60 * 1000);

    document.cookie = name + '=' + value + ';expires=' + date.toUTCString() + '; path=/';
};

const getCookie = (name) => {
    var value = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    return value? value[2] : null;
};

let is_login = Boolean(getCookie("login"))

const deleteCookie = (name) => {
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
}

$(function(){
	//chkLogin()
	time();
	
	
//	$("#home").click(function(){
//		var url = location.href.substr(0,location.href.indexOf(ctxPath));
//    	//location.href = "/aIdoo/chart/index.do"
//		location.href = "/aIdoo_exhibit/chart/index.do"
//	});
		
	setElement()
	
	appName = location.href.split("/")[3]
	createHeader()
	
	if(appName != indexPath.substr(1)){
		createTitle()
	}
	
	bindEvt()

	
	loadPage()
	
	
	
	
	
	
	
	
	getComName();
	
	//drawFlag();
	
	
//	var is_login = window.sessionStorage.getItem("login");
//	var login_time = window.sessionStorage.getItem("login_time");
//	var time = new Date().getTime();
//	if(login==null || (login !=null && (time - login_time) / 1000 > 60 * 20)) {
//		showCorver();
//		$("#loginForm").css("display", "block");
//		$("#email").focus();
//	}else{
//		loginSuccess();
//		drawLogout();
//	}
//	loginSuccess();
//	var url = location.href
//	   
//	if(location.href.lastIndexOf("aIdoo_exhibit") != -1 || location.href.lastIndexOf("aIdoo_namsan_exhibit") != -1){
//		chkBanner();
//	}
	
	
});
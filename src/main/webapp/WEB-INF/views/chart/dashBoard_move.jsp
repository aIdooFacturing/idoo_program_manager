<!-- 
최소한의 화면으로 구성된
시작화면 Template
 -->
 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>

<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
.k-grid .k-state-selected  {
  background-color: black !important;
  color: #000000;
}
.k-grid .k-alt.k-state-selected {
  background-color: black !important;
  color: #000000;
}
</style> 
<script type="text/javascript">
	var draw;
	var dragok = false;
	var jsonBox = [];
	var beforejsonBox = [];
	var kendotable;
	$(function(){
		createNav("monitor_nav", 0);	
		
		setEl();
		time();
		
		drawGroupDiv();
		getTable();
		//redrawPieChart();
		//getCreateImg();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		$("#canvas").css({
			"z-index" : 8888,
			"position" : "absolute",
			//"top" : marginHeight,
			"left" : $("#home")[0].clientWidth
//			"left" : marginWidth
		});
		
		canvas = document.getElementById("canvas");
		ctx = canvas.getContext("2d");
		canvas.width = contentWidth;
		canvas.height = contentHeight;
		BB = canvas.getBoundingClientRect();
		offsetX = marginWidth;
		offsetY = marginHeight;
//		console.log(offsetY)

		kendotable = $("#m_status").kendoGrid({
			height:getElSize(850),
			/* dataSource:jsonBox, */
			selectable: true,
			// 선택한 행으로 스크롤 이동하기
			change: function(e) {
				// chkClick = 장비 선택했을때만 이동시키기 위해,, dragok = 장비 드래그 중에는 적용안하기 위해
				if(dragok==false && chkClick){
					var scrollContentOffset = this.element.find("tbody").offset().top;
		            var selectContentOffset = this.select().offset().top;
		            var distance = selectContentOffset - scrollContentOffset;

		            this.element.find(".k-grid-content").animate({
		                scrollTop: distance
		            }, 200);
				}
	        },
	        save: function(e) {
/* 	            console.log(e.type);
	            console.log(jsonBox[78])
	            console.log(jsonBox[78].x=2600) */
	            for(i=0,len=jsonBox.length; i<len; i++){
	            	if(i==0){
	            		/* console.log(jsonBox[i].dvcId + " : " +e.model.dvcId)
	            		console.log(jsonBox[i].name + " : " +e.model.name)
	            		console.log(jsonBox[i].name==e.model.name)
	            		console.log(jsonBox[i].name==e.model.name) */
	            	}
	            	if(jsonBox[i].dvcId==e.model.dvcId && jsonBox[i].name==e.model.name){
	            		if(e.values.x!=undefined){
		            		jsonBox[i].x=Number(e.values.x)
	            		}
	            		if(e.values.y!=undefined){
		            		jsonBox[i].y=Number(e.values.y)
	            		}
	            	}
	            }
	            
	            drawBox();
        		setTimeout(function() {
	       	        ctx.clearRect(0, 0, 5500, 5500);
        			drawBox();
        		}, 500)
	            setTimeout(function() {
	       	        ctx.clearRect(0, 0, 5500, 5500);
        			drawBox();
        		}, 1000)
        		 setTimeout(function() {
	       	        ctx.clearRect(0, 0, 5500, 5500);
        			drawBox();
        		}, 1500)
			},
			editable : true,
			columns:[{title : "장비 위치 정보",
				columns:[{
					title : "dvcId"
					,field : "dvcId"
					,editable : false
					,width : getElSize(200)
				},{
					title : "name"
					,field : "name"
					,width : getElSize(500)
				},{
					title : "X 좌표"
					,field : "x"
					,editor: demicalNumberX
					,width : getElSize(200)
				},{
					title : "Y 좌표"
					,field : "y"
					,editor: demicalNumberY
					,width : getElSize(200)
				}]
			}]
		}).data("kendoGrid");
		
		
		//kendo grid 클릭시,,, 더블클릭시 에는 dblclick 으로 바꿔주면됨.
		$("#m_status").on("click", "tbody>tr", function (e) {
			var dataItem = $("#m_status").data("kendoGrid").dataSource.getByUid(this.dataset.uid);
			console.log(dataItem)
			
		    $("#testC").css({
		    	"z-index" : 99999
		    	,"width" : getElSize(120)
		    	,"height" : getElSize(130)
		    	,"background" : "white"
		    })

		    /*  $('#testC').animate({
		    	top: getElSize(dataItem.y)-$('#testC').height()+getElSize(20),
		    	left: getElSize(dataItem.x)+getElSize(30)
			},100); */
	
			$("#testC").css({
		    	top: getElSize(dataItem.y)-$('#testC').height()+getElSize(20),/* -getElSize($("#svg_td").offset().top)+getElSize(marginHeight)+getElSize(70), */
		    	left: getElSize(dataItem.x)+getElSize(30)
			});
	
		    setTimeout(function() {
		    	$("#testC").css({
			    	"z-index" : 99999
			    	,"background" : "black"
			    })
	    	}, 350)
		    setTimeout(function() {
		    	$("#testC").css({
			    	"z-index" : 99999
			    	,"background" : "white"
			    })
	    	}, 700)
		    setTimeout(function() {
		    	$("#testC").css({
			    	"z-index" : 1
			    	,"background" : "black"
			    })
	    	}, 1050)
	    	
	    	
		    
		})
		chkBanner();
		
	});

	window.onload = function() {
		setTimeout(function() {
			drawBox() 
		}, 500);
		setTimeout(function() {
			drawBox() 
		}, 1000);
		setTimeout(function() {
			drawBox() 
			canvas.onmousedown = myDown;
			canvas.onmouseup = myUp;
			canvas.onmousemove = myMove;
			
		}, 2000)
//		drawBox()
/* 		$("#canvas").mousedown(function(e) {
			  console.log(e);
		}); */
	}
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#svg").css({
			"width" : $("#table").width() - $("#svg_td").offset().left + marginWidth ,
			"height" : originHeight,
			"left" : $("#svg_td").offset().left,
			"position" :"absolute",
//			"z-index" : 9999
		});
		
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	//x좌표
	function demicalNumberX(container, options){
		$('<input name="' + options.field + '"/>')
	     .appendTo(container)
	     .kendoNumericTextBox({
	         decimals: 0
	         ,restrictDecimals:true
	         ,min:469
	         ,max:4000
	     })
	}
	//y좌표 
	function demicalNumberY(container, options){
		$('<input name="' + options.field + '"/>')
	     .appendTo(container)
	     .kendoNumericTextBox({
	         decimals: 0
	         ,restrictDecimals:true
	         ,min:383
	         ,max:2040
	     })
	}
	
	function drawGroupDiv(){
		//LFA RR
		var table = "<table class='gr_table' id='lfa_rr'>" + 
						"<tr>" + 
							"<Td class='label'>TA RR<br>JC RR</td>" +
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" +
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#lfa_rr").css({
			"left" : getElSize(500) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#lfa_rr").css({
			"width" : getElSize(190)
		});
		
		//YP FRT (cnc)
		var table = "<table class='gr_table' id='yp_frt'>" + 
						"<tr>" + 
							//"<Td class='label'>YP FRT (CNC)<br>${kr}, ${na}</td>" +
							"<Td class='label'>TA FRT / OS FRT</td>" +
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#yp_frt").css({
			"left" : getElSize(730) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#yp_frt td").css({
			"width" : getElSize(250)
		});
	
		//UM FRT (CNC)
		var table = "<table class='gr_table' id='um_frt'>" + 
						"<tr>" + 
							"<Td class='label'>OS FRT/YP FRT</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#um_frt").css({
			"left" : getElSize(1030) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#um_frt td").css({
			"width" : getElSize(400)
		});
		
		//TA RR
		var table = "<table class='gr_table' id='ta_Rr'>" + 
						"<tr>" + 
							"<Td class='label'>LFA RR</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#ta_Rr").css({
			"left" : getElSize(1490) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#ta_Rr td").css({
			"width" : getElSize(250)
		});
		
		//TA FRT
		var table = "<table class='gr_table' id='ta_frt'>" + 
						"<tr>" + 
							"<Td class='label'>YP FRT</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#ta_frt").css({
			"left" : getElSize(1800) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#ta_frt td").css({
			"width" : getElSize(250)
		});
		
		
		//JC RR
		var table = "<table class='gr_table' id='jc_rr'>" + 
						"<tr>" + 
							"<Td class='label'>UM FRT</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#jc_rr").css({
			"left" : getElSize(2100) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#jc_rr td").css({
			"width" : getElSize(250)
		});
		
		//
		var table = "<table class='gr_table' id='jc_rr2'>" + 
						"<tr>" + 
							"<Td class='label'>TM RR</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#jc_rr2").css({
			"left" : getElSize(2400) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#jc_rr2 td").css({
			"width" : getElSize(250)
		});
		
		
		//HR PU/FRT
		var table = "<table class='gr_table' id='hr_pu'>" + 
						"<tr>" + 
							"<Td class='label'>OS RR</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#hr_pu").css({
			"left" : getElSize(2700) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#hr_pu td").css({
			"width" : getElSize(300)
		});
		
		
		//HR PU/FRT
		var table = "<table class='gr_table' id='hr_pu2'>" + 
						"<tr>" + 
							"<Td class='label'>TM/FRT</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#hr_pu2").css({
			"left" : getElSize(3040) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#hr_pu2 td").css({
			"width" : getElSize(250)
		});
		
		//HR PU/FRT
		var table = "<table class='gr_table' id='hr_pu3'>" + 
						"<tr>" + 
							"<Td class='label'>HR FRT</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#hr_pu3").css({
			"left" : getElSize(3340) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#hr_pu3 td").css({
			"width" : getElSize(250)
		});
		
		$("#editDvc").css({
			"position": "absolute",
			"left" :getElSize(3344)+marginWidth,
			"top" : getElSize(285) + marginHeight , 
			"z-index" : 9999
		})
		$("#editDvc input").css({
			"font-size" : getElSize(50),
			"text-decoration": "none",
			"display": "inline-block",
			"padding-left": getElSize(15),
			"padding-right": getElSize(15),
			"line-height": getElSize(80) + "px",
			"width" : getElSize(220),
			"cursor": "pointer",
		    "border-radius": "12px",
/* 			"height" : getElSize(100),
			"width" : getElSize(550) */
		})
		$("#editSave").css({
		    "background-color": "blue",
		    "color":"#EAEAEA",
		})
		$("#editExit").css({
		    "background-color": "red",
		    "color":"#EAEAEA",
		})
		
		//TQ FRT
		var table = "<table class='gr_table' id='tq_frt'>" + 
						"<tr>" + 
							"<Td class='label'>TQ FRT</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#tq_frt").css({
			"left" : getElSize(3640) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#tq_frt td").css({
			"width" : getElSize(190)
		});
		
		
		
		
		
		
		var table = "<table class='gr_table' id='yp_rr'>" + 
							"<tr>" + 
								"<Td class='label'>FS RR</td>" + 
							"</tr>" + 
							"<tr>" + 
								"<Td class='icon'></td>" + 
							"</tr>" +
						"</table>";
					
					$("#container").append(table);
					
					$("#yp_rr").css({
						"left" : getElSize(2330) + marginWidth,
						"top" : getElSize(1180) + marginHeight,	
					});
					
					$("#yp_rr td").css({
						"width" : getElSize(470)
					});

		var table = "<table class='gr_table' id='yp_rr2'>" + 
					"<tr>" + 
						"<Td class='label'>TA FRT / UM RR</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";
			
			$("#container").append(table);
			
			$("#yp_rr2").css({
				"left" : getElSize(2850) + marginWidth,
				"top" : getElSize(1180) + marginHeight,	
			});
			
			$("#yp_rr2 td").css({
				"width" : getElSize(150)
			});
			
			
			var table = "<table class='gr_table' id='yp_rr3'>" + 
					"<tr>" + 
						"<Td class='label'>UM RR / LFA RR</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";
			
			$("#container").append(table);
			
			$("#yp_rr3").css({
				"left" : getElSize(3040) + marginWidth,
				"top" : getElSize(1180) + marginHeight,	
			});
			
			$("#yp_rr3 td").css({
				"width" : getElSize(250)
			});
		
			
			var table = "<table class='gr_table' id='yp_rr4'>" + 
					"<tr>" + 
						"<Td class='label'>YP RR / YP FRT</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";
			
			$("#container").append(table);
			
			$("#yp_rr4").css({
				"left" : getElSize(3340) + marginWidth,
				"top" : getElSize(1180) + marginHeight,	
			});
			
			$("#yp_rr4 td").css({
				"width" : getElSize(250)
			});
			
			
			//UM FRT
			var table = "<table class='gr_table' id='yp_rr5'>" + 
			"<tr>" + 
				"<Td class='label'>UM FRT</td>" + 
			"</tr>" + 
			"<tr>" + 
				"<Td class='icon'></td>" + 
			"</tr>" +
			"</table>";
		
			$("#container").append(table);
			
			$("#yp_rr5").css({
				"left" : getElSize(3640) + marginWidth,
				"top" : getElSize(1180) + marginHeight,	
			});
			
			$("#yp_rr5 td").css({
				"width" : getElSize(190)
			});
			
		//UM FRT (MCT)
		/* var table = "<table class='gr_table' id='um_frt_m'>" + 
						"<tr>" + 
							"<Td class='label'>UM FRT (MCT)</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#um_frt_m").css({
			"left" : getElSize(3640) + marginWidth,
			"top" : getElSize(1400) + marginHeight,	
		});
		
		$("#um_frt_m td").css({
			"width" : getElSize(190)
		});
		 */
		
		 /* 
		//YP RR 
		var table = "<table class='gr_table' id='yp_rr'>" + 
						"<tr>" + 
							"<Td class='label'>YP RR<br>${kr}</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#yp_rr").css({
			"left" : getElSize(3200) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#yp_rr td").css({
			"width" : getElSize(174)
		});
		
		//YP FRT 
		var table = "<table class='gr_table' id='yp_frt_m'>" + 
						"<tr>" + 
							"<Td class='label'>YP FRT<br>(MCT)</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#yp_frt_m").css({
			"left" : getElSize(3378) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#yp_frt_m td").css({
			"width" : getElSize(174)
		});
		
		//UM RR
		var table = "<table class='gr_table' id='um_rr'>" + 
						"<tr>" + 
							"<Td class='label'>UM RR<Br>${up}</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#um_rr").css({
			"left" : getElSize(2790) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#um_rr td").css({
			"width" : getElSize(174)
		});
		
		//LFA RR
		var table = "<table class='gr_table' id='lfa_rr_m'>" + 
						"<tr>" + 
							"<Td class='label'>LFA RR<Br>(MCT)</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#lfa_rr_m").css({
			"left" : getElSize(2968) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#lfa_rr_m td").css({
			"width" : getElSize(174)
		});
		
		//YP RR
		var table = "<table class='gr_table' id='yp_rr_a'>" + 
						"<tr>" + 
							"<Td class='label'>YP RR<br>${na}</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#yp_rr_a").css({
			"left" : getElSize(2390) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#yp_rr_a td").css({
			"width" : getElSize(174)
		});
		
		//UM_RR
		var table = "<table class='gr_table' id='um_rr_n'>" + 
						"<tr>" + 
							"<Td class='label'>UM_RR<br>${kr}</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	 	
		$("#container").append(table);
				
		$("#um_rr_n").css({
			"left" : getElSize(2568) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#um_rr_n td").css({
			"width" : getElSize(174)
		});
		
		//FS RR
		var table = "<table class='gr_table' id='fs_rr'>" + 
						"<tr>" + 
							"<Td class='label'>FS RR</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#fs_rr").css({
			"left" : getElSize(1980) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#fs_rr td").css({
			"width" : getElSize(350)
		}); */
		
		//machine status
//		var table = "<div id='m_status'> asd </div>";
/* 		var table = "<table class='gr_table' id='m_status'>" + 
						"<tr>" + 
							"<Td class='label'>${opstatus}</td>" + 
							"<td>asd</td>" +
						"</tr>"
					"</table>"; */
	
//		$("#container").append(table);
				
		$("#m_status").css({
			"position" : "absolute",
			"left" : getElSize(730) + marginWidth,
			"top" : getElSize(1260) + marginHeight,
			"width" : getElSize(1300),
			"z-index" : 9999

		});
		
		$("#m_status td").css({
			"width" : getElSize(1110)
		});
		
		
		
		var table = "<table class='gr_table' id='tmp'>" + 
				"<tr>" + 
					"<Td class='label'>SAMPLE</td>" + 
				"</tr>" + 
				"<tr>" + 
					"<Td class='icon'></td>" +
				"</tr>" +
			"</table>";
		
		$("#container").append(table);
		
		$("#tmp").css({
			"left" : getElSize(500) + marginWidth,
			"top" : getElSize(1180) + marginHeight,	
		});
		
		$("#tmp").css({
			"width" : getElSize(190),
			"height" : getElSize(950)
		});

		
		$(".gr_table").css({
			"position" : "absolute",
			"border-collapse" : "collapse",
			"z-index" : 8
		});
		
		
		
		
		$(".gr_table .label").css({
			"background-color" : "#373737",
			"text-align" : "center",
			"height" : getElSize(70),
			"border" : getElSize(3) + "px solid #535556"
		});
		
		$(".gr_table .icon").css({
			//"height" : getElSize(530),
			"height" : getElSize(730),
			"border" : getElSize(3) + "px solid #535556"

		});
		
		$("#lfa_rr .icon,#yp_frt .icon,  #ta_frt .icon, #jc_rr .icon, #hr_pu .icon").css({
			"height" : getElSize(730),

		});
		
		$("#tq_frt .icon").css({
			"height" : getElSize(1700),

		});
		
		$("#yp_rr, #yp_rr2, #yp_rr3, #yp_rr4, #yp_rr5").css({
			"height" : getElSize(980),
		})
		
		$("#fs_rr .icon").css({
			"height" : getElSize(790),

		});
		
		$(".gr_table td").css({
			"color" : "white",
			"font-size" : getElSize(30),
		});
		
		
	};
	
	//테이블 그리기
	function redrawPieChart(){
		var url = ctxPath + "/svg/getMachineInfo2.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function (data){
				var json = data.machineList;
				console.log(json)
				noconn = [];
				inCycleMachine = 0;
				waitMachine = 0;
				alarmMachine = 0;
				powerOffMachine = 0;
				
				$(json).each(function(key, data){
					if(data.notUse==1){
					//	powerOffMachine--;
					};
					
					if(data.lastChartStatus=="IN-CYCLE"){
						inCycleMachine++;
					}else if(data.lastChartStatus=="WAIT"){
						waitMachine++;
					}else if(data.lastChartStatus=="ALARM"){
						alarmMachine++;
					}else if(data.lastChartStatus=="NO-CONNECTION" && data.notUse!=1){
						powerOffMachine++;
						noconn.push(replaceAll(replaceAll(decodeURI(data.name), "%23","#"),"%2F","/"))
					};
				});
				
				if(waitMachine==0 && alarmMachine==0){
					borderColor = "green";
				}else if(alarmMachine==0){
					borderColor = "yellow";
				}else if(alarmMachine!=0){
					borderColor = "red";
				};
				
				$(".status_span").remove();
				var inCycleSpan = "<span id='inCycleSpan' class='status_span'>" + addZero(String(inCycleMachine)) + "</span>";
				$("#container").append(inCycleSpan);
				
				var waitSpan = "<span id='waitSpan' class='status_span'>" + addZero(String(waitMachine)) + "</span>";
				$("#container").append(waitSpan);
				
				var alarmSpan = "<span id='alarmSpan' class='status_span'>" + addZero(String(alarmMachine)) + "</span>";
				$("#container").append(alarmSpan);
				
				var noConnSpan = "<span id='noConnSpan' class='status_span'>" + addZero(String(powerOffMachine)) + "</span>";
				$("#container").append(noConnSpan);
				
				$(".total").remove();
				var totalSpan = "<div id='totalSpan' class='total'><div id='total_title'>Total</div><div>" + (inCycleMachine + waitMachine + alarmMachine + powerOffMachine) + "<div></div>";
				$("#container").append(totalSpan);
				
				$("#inCycleSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(100),
					"z-index" : 10,
					"color" : "#A3D800",
					"top" : getElSize(1460) + marginHeight,
					"left" : getElSize(1205) + marginWidth
				});
				
				$("#waitSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(100),
					"z-index" : 10,
					"color" : "#FF9100",
					"top" : getElSize(1700) + marginHeight,
					"left" : getElSize(1450) + marginWidth
				});
				
				$("#alarmSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(100),
					"z-index" : 10,
					"color" : "#EC1C24",
					"top" : getElSize(1920) + marginHeight,
					"left" : getElSize(1205) + marginWidth
				});
				
				$("#noConnSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(100),
					"z-index" : 10,
					"color" : "#CFD1D2",
					"top" : getElSize(1700) + marginHeight,
					"left" : getElSize(950) + marginWidth
				});
				
				$("#totalSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(70),
					"z-index" : 10,
					"color" : "#ffffff",
					"top" : getElSize(1680) + marginHeight,
					"left" : getElSize(1200) + marginWidth,
					"text-align" : "center"
				});
				
				$("#total_title").css({
					"font-size" : getElSize(45),
				});
			}
		});
	};
	
	function getCreateImg(){
		marker = draw.image(imgPath + data.pic + ".png").size(data.w, data.h);
	}
	
	function getTable(){
		var url = ctxPath + "/svg/getMachineInfo.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function (data){
				var json = data.machineList;
				
				$(json).each(function(idx,data){
					data.name=decode(data.name)
				})
				jsonBox = data.machineList;
				
				beforejsonBox=JSON.parse(JSON.stringify(jsonBox))
				
				console.log("?")
				
				setTimeout(function() { drawBox() }, 2000)

				
				
				
				
				
				
				
				kendodata = new kendo.data.DataSource({
					data: jsonBox,
					batch: true,
					schema: {
						model: {
							id: "id",
							fields: {
								dvcId: { editable: false },
								name: { editable: false },
								x: {type : "number"}, 
								y: {type : "number"},
							}
						}
					}
				});
					
				kendotable.setDataSource(kendodata);
				
				$("#m_status thead tr th").css({
					"font-size":getElSize(40),
				    "text-align": "center"
				});
				
				$("#m_status tbody tr td").css("font-size",getElSize(40));
				
				/* dataSource: {
				    data: jsonBox,
				    schema: {
				      model: {
				        fields: {
				        	dvcId: { type: "number", validation: { required: true }, editable : false },
				        	name: {  validation: { required: true }, editable : false }
				        }
				      }
				    }
				}, */
				
			}
		})
	}

	function drawBox(){
		//img.src="../images/DashBoard/m1-NO-CONNECTION.svg"
		
		$(jsonBox).each(function(idx,data){
			var img=new Image();
			img.src="../images/DashBoard/m1-"+data.lastChartStatus+"-if.svg"
/* 			ctx.fillText(data.dvcId,getElSize(data.x),getElSize(data.y),63.416666666666664,47.5625);
			ctx.font = "30px Comic Sans MS";
			ctx.fillStyle = "red";
 *///			ctx.textAlign = "center";
			
/* 			if(idx==0){
				console.log(getElSize(data.x)-$("#svg").offset().left + marginWidth)
				console.log(" svg :"+ $("#svg").offset().left + " , margin :" + marginWidth + " h :" + getElSize(data.h*0.8)  + " y :" + getElSize(data.w*0.8))
			}
 */	

 			if(/* data.dvcId==27 || data.dvcId==86 || data.dvcId==99 || */ data.dvcId==88 || data.dvcId==87 || data.dvcId==85 || data.dvcId==84 || data.dvcId==83 || data.dvcId==76 || data.dvcId==77 || data.dvcId==78){
 				ctx.font = getElSize(35)+"px Comic Sans MS";
 				ctx.fillStyle = "black";
 				ctx.drawImage(img,getElSize(data.x)+getElSize(52)-$("#home")[0].clientWidth,getElSize(data.y)-$("#svg_td").offset().top+marginHeight,getElSize(data.h*0.68),getElSize(data.w*0.46))
 				ctx.fillText(data.dvcId,getElSize(data.x)+getElSize(77)-$("#home")[0].clientWidth,getElSize(data.y)+getElSize(70)-$("#svg_td").offset().top+marginHeight,getElSize(data.h*0.68),getElSize(data.w*0.46));
			}else{
 				ctx.font = getElSize(35)+"px Comic Sans MS";
 				ctx.fillStyle = "black";
				asd = ctx.drawImage(img,getElSize(data.x)+getElSize(30)-$("#home")[0].clientWidth,getElSize(data.y)-$("#svg_td").offset().top +marginHeight,getElSize(data.h*0.66),getElSize(data.w*0.66))
 				ctx.fillText(data.dvcId,getElSize(data.x)+getElSize(55)-$("#home")[0].clientWidth,getElSize(data.y)+getElSize(70)-$("#svg_td").offset().top+marginHeight,getElSize(data.h*0.66),getElSize(data.w*0.66));
			}
//			ctx.drawImage(img,getElSize(data.x),getElSize(data.y),getElSize(data.h),getElSize(data.w))
		})
	}
	
	var chkIdx;
	var chkClick;
	// handle mousedown events
	function myDown(e) {
		console.log("click")
	    // tell the browser we're handling this mouse event
	    e.preventDefault();
	    e.stopPropagation();
	
/*  	    console.log(e.clientX + " , " + e.clientY)
	    console.log(offsetX + " , " + offsetY)  */
	    // get the current mouse position
	    var mx = parseInt(e.clientX - offsetX);
	    var my = parseInt(e.clientY - offsetY);
		
/* 
	    setTimeout(function() {
	    	$('#testC').css("background","white")
    	}, 500)
		
		setTimeout(function() {
	   		 $('#testC').css("background","black")
		}, 1000)
        		
		setTimeout(function() {
			$('#testC').css("background","white")
		}, 1500)

   		setTimeout(function() {
	   		 $('#testC').css("background","black")
   		}, 2000) */
        		

	    // test each rect to see if mouse is inside
	    dragok = false;

	    /* console.log(mx + " , " + my)
	    console.log("x :" + getElSize(jsonBox[0].x) + " , w :" + getElSize(jsonBox[0].w))
	    console.log("y :" + getElSize(jsonBox[0].y) + " , h :" + getElSize(jsonBox[0].h))
	    console.log(mx + " >: " + (Number(getElSize(jsonBox[0].x))+ Number(getElSize(30))))
	    console.log(mx + " <: " + (Number(getElSize(jsonBox[0].x)) + Number(getElSize(jsonBox[0].w*0.66))+ Number(getElSize(30))))
	    console.log(my + " >: " + (Number(getElSize(jsonBox[0].y))+ Number(getElSize(70))))
	    console.log(my + " <: " + (Number(getElSize(jsonBox[0].y)) + Number(getElSize(jsonBox[0].h*0.66))+ Number(getElSize(70)))) */
	    
	    for (var i = 0; i < jsonBox.length; i++) {
		    
	        if (mx > getElSize(jsonBox[i].x) + getElSize(30) && mx < getElSize(jsonBox[i].x) + getElSize(jsonBox[i].w*0.66) + getElSize(30) && my > getElSize(jsonBox[i].y) && my < getElSize(jsonBox[i].y) + getElSize(jsonBox[i].h*0.66)) {
	        	console.log(jsonBox[i])
	            // if yes, set that rects isDragging=true
	            chkClick = true;
	        	var grid = $("#m_status").data("kendoGrid");
	        	grid.select("tr:eq("+i+")");
	            chkClick = false;
	        	chkIdx=i
//	            dragok = true;
//	        	jsonBox[chkIdx].isDragging = true;
	        }
	        
	    }
	    
	    // 일부로 바깥으로 뺌 => 같은위치 박스가 2개있으면 두개다 움직이므로 위에있는것 하나만 움직이기 위해서
	    if(chkIdx!=undefined || chkIdx != null){
	        dragok = true;
	    	jsonBox[chkIdx].isDragging = true;
	    }

	    // save the current mouse position
	    startX = mx;
	    startY = my;
	}


	// handle mouseup events
	function myUp(e) {  
	    // tell the browser we're handling this mouse event
	    e.preventDefault();
	    e.stopPropagation();

	    // clear all the dragging flags
	    dragok = false;
	    for (var i = 0; i < jsonBox.length; i++) {
	    	jsonBox[i].isDragging = false;
	    }
	    drawBox();
	    if(chkIdx!=null){
	    	chkClick = true;
	        var grid = $("#m_status").data("kendoGrid");
	        grid.select("tr:eq("+chkIdx+")");
	        chkClick = false;
	    }
	    chkIdx=null
	}

	// handle mouse moves
	function myMove(e) {
	    // if we're dragging anything...
	    if (dragok) {

	        // tell the browser we're handling this mouse event
	        e.preventDefault();
	        e.stopPropagation();

	        // get the current mouse position
	        var mx = parseInt(e.clientX - offsetX);
	        var my = parseInt(e.clientY - offsetY);

	        // calculate the distance the mouse has moved
	        // since the last mousemove
	        function getElSize(n){
	    		return contentWidth/(targetWidth/n);
	    	};

	    	var dx = mx - startX;
	    		dx = Number((targetWidth * (dx/contentWidth)).toFixed(0))
	        var dy = my - startY;
	    		dy = Number((targetHeight * (dy/contentHeight)).toFixed(0))
	    		
	    //    console.log(mx + " , " + my + " , " + dx + " , " + dy + " , " )

	        // move each rect that isDragging 
	        // by the distance the mouse has moved
	        // since the last mousemove
	        for (var i = 0; i < jsonBox.length; i++) {
	            if (jsonBox[i].isDragging) {
	            	jsonBox[i].x += dx;
	            	jsonBox[i].y += dy;
	            }
	        }

	        // redraw the scene with the new rect positions
	        
	        ctx.clearRect(dx, dy, 7500, 7500);
	        drawBox();
	        chkClick = true;
	        $('#m_status').data('kendoGrid').dataSource.read();
	        $("#m_status").data("kendoGrid").select("tr:eq("+chkIdx+")");
	        chkClick = false;
/* 	        var grid = $("#m_status").data("kendoGrid");
	        grid.select("tr:eq("+chkIdx+")"); */
/* 	        $('#m_status').data('kendoGrid').dataSource.read();
        	var grid = $("#m_status").data("kendoGrid");
        	grid.select("tr:eq("+chkIdx+")"); */

	        // reset the starting mouse position for the next mousemove
	        startX = mx;
	        startY = my;

	    }
	}
	
	function exit(){
		location.href=ctxPath + "/chart/main.do";
	}
	function save(){
		var updateDate=[]
		//바뀐값 for문
		for(i=0,len=jsonBox.length; i<len; i++){
			//기존 바뀌기전 데이터 for문
			for(j=0, length=beforejsonBox.length; j<length; j++){
				//dvcId , name 이 같을때
				if(jsonBox[i].dvcId==beforejsonBox[j].dvcId && jsonBox[i].name == beforejsonBox[j].name){
					//데이터 변했는지 확인하기
					if(jsonBox[i].x!=beforejsonBox[j].x || jsonBox[i].y!=beforejsonBox[j].y){
						updateDate.push(jsonBox[i])
					}
				}
			}
		}
		
		if(updateDate.length==0){
			alert("변경사항이 없습니다.111")
			return;
		}
		console.log("================")
		console.log(updateDate)
			
		var url = ctxPath + "/chart/saveDvcMove.do";
		
		var obj = new Object();
		obj.val = updateDate;
		
		var param = "shopId=" + shopId +
					"&val=" + JSON.stringify(obj);
		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function (data){
				if(data=="success"){
					$.hideLoading()
					alert("저장이 완료되었습니다.");
				}else{
					$.hideLoading()
					alert("저장에 실패하였습니다.");
				}
			}
		})
		
		
	}
	
	</script>
</head>
<body>

	<div id="svg"></div>
	<div id="time"></div>
	<div id="title_right"></div>
	<div id="editDvc">
		<input type="button" value="저장" id="editSave" onclick="save()">　 <input type="button" value="나가기 " id="editExit" onclick="exit()">
	</div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/order_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
				<div id="testC" style="position: absolute;">　</div>
				<canvas id="canvas"></canvas>
				<!-- 
				
				이곳에 필요한 DOM 을 작성합니다.
				실질적 화면에 표시되는 부분.
				
				 -->
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	<div id='m_status'></div>
	<div id="intro_back"></div>
	<span id="intro"></span>
	
</body>
</html>	
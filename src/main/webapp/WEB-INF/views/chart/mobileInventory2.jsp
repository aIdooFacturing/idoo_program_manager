<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="/WEB-INF/views/include/unomiclib.jsp"%>
<%@ page import="com.unomic.dulink.common.domain.CommonCode"%>
<%@ page import="com.unomic.dulink.chart.domain.*"%>
<%@ page session = "true" %>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>

<script src="${ctxPath }/js/jquery.js"></script>


<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>Insert title here</title>
</head>
<style>
body{
	height: 100%;
	width: 100%;
	position: absolute;
	margin: 0%;
	padding: 0%;
	background: black;
	color: white;
	
}
.space{
	width: 100%;
	height: 4%;
	display: table;
	float: left;
}
.mDiv{
	width: 100%;
	height: 28%;
	display: table;
	float: left;
	font-size: 400%;
}
.mDiv label{
	display: table-cell;
	vertical-align: middle;
	width: 100%;
	height: 100%;
	text-align: center;
	background: #8041D9;
}
.mBtn {
	cursor: pointer;
}
</style>
<script>
	//입고 관리
	function mInMove(){
		location.href="${ctxPath}/chart/mobileInMove.do"
	}
	//불출 관리
	function mOutMove(){
		location.href="${ctxPath}/chart/mobileOutMove.do"
	}
	//반입 관리	
	function mReInMove(){
		location.href="${ctxPath}/chart/mobileReInMove.do"
	}
</script>
<body>
	<div style="height: 100%; width: 17%; float: left;">
		　
	</div>
	<div style="height: 100%; width: 66%; float: left;">
		<div id="content" style="position: absolute; height: 100%;width: 66%; display: table;">
			<div class="space">　</div>
			<div class="mDiv">
				<label onclick="mInMove()" class="mBtn"> 입고 관리  </label>
			</div>
			<div class="space" >　</div>
			<div class="mDiv">
				<label onclick="mOutMove()" class="mBtn"> 불출 관리  </label>
			</div>
			<div class="space">　</div>
			<div class="mDiv">
				<label onclick="mReInMove()" class="mBtn"> 반입 관리  </label>
			</div>
			<div class="space">　</div>
		</div>
	</div>
	<div style="height: 100%; width: 17%; float: left;">
		　
	</div>
	
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<%-- <script type="text/javascript" src="${ctxPath }/js/chart/svg_controller.js"></script> --%>
<script type="text/javascript" src="${ctxPath }/js/chart/SVG_.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/SVG_draggable.js"></script>
 
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}
#ui-datepicker-div{
	z-index:999;
}
#sDate{
	z-index:999;
	position: relative; z-index: 100000;
}
</style> 
<script type="text/javascript">
	var loopFlag = null;
	var session = window.localStorage.getItem("auto_flag");
	if(session==null) window.localStorage.setItem("auto_flag", false);
	
	var flag = false;
	function stopLoop(){
		var flag = window.localStorage.getItem("auto_flag");
		
		if(flag=="true"){
			flag = "false"
		}else{
			flag = "true"
		}
		
		window.localStorage.setItem("auto_flag", flag);
		
		if(flag=="false"){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		alert("페이지 자동 이동 : " + flag);
	};
	
	var canvas;
	var ctx;
	
	var handle = 0;
	var incycleColor = "#A3D800";
	var waitColor = "#FF9100";
	var alarmColor = "#EC1C24";
	var noConnColor = "#D7D7D7";
	
	var startHour, startMinute;
	
	function getStartTime(){
		var url = ctxPath + "/chart/getStartTime.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				startHour = Number(data.split("-")[0])
				startMinute = data.split("-")[1] * 10;
				
				createMachine();
			}, error : function(e1,e2,e3){ 
				console.log(e1,e2,e3)
			}
		});	
	};
	
	$(function(){
		$("#resetBtn").prop('checked', true) ;
		$("#sDate").val(getToday().substr(0,10))
		time()
		getStartTime();
		createNav("monitor_nav",1);
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
	
		canvas = document.getElementById("canvas");
		ctx = canvas.getContext("2d");
		canvas.width = contentWidth;
		canvas.height = contentHeight;
		$("#canvas").css({
			"z-index" : -7,
			"position" : "absolute",
			"top" : marginHeight,
			"left" : marginWidth
		});
		
		drawGroupDiv();
		//document.oncontextmenu = function() {stopLoop()}; 
		setDivPos();
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		if(session=="false"){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		chkBanner();
	});
	
	var isTodayN = false;
	var nd;
	function createMachine(btnNd){
		if(isTodayN==false){
			$( "#sDate" ).datepicker( "option", "disabled", true );
		}else{
			$( "#sDate" ).datepicker( "option", "disabled", false );
		}
		clearInterval(dateInterval);
		
		var url = ctxPath + "/chart/getMachineInfo.do";
		var date = new Date();
		var year = date.getFullYear();
		
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var hour = date.getHours();
		
		var minute = date.getMinutes();

		
		var target_time_n = (startHour - 12) * 60 + startMinute;
		var target_time_d = startHour * 60 + startMinute;
		var current_time = hour * 60 + minute;
		
		var isToday = true;
		
		if(getToday().substr(0,10) != today){
			isToday = false;
		}
		var worker;
		var prdctRatio;
		
		
		var day = moment(new Date(moment().year(), moment().month(), moment().date(), 8, 30, 0));
    	var night = moment(new Date(moment().year(), moment().month(), moment().date(), 20, 30, 0));
    	
    	if(btnNd==1){
    		nd=btnNd;
    		$("#nightWorkBtn").css({
				"background" : "#004080"
			})
			$("#dayWorkBtn").css({
				"background" : "linear-gradient(#007FFE, #004080)"
			})
    	}else if(btnNd==2){
    		nd=btnNd;
    		$("#nightWorkBtn").css({
				"background" : "linear-gradient(#007FFE, #004080)"
			})
			$("#dayWorkBtn").css({
				"background" : "#004080"
			})
    	}
    	
    	if(isTodayN==false){
    		if(moment().isAfter(day)){
        		if(moment().isAfter(night)){
        			nd=1;
        			$('input:radio[name="nd"][value="1"]').prop('checked', true);
        			$("#nightWorkBtn").css({
        				"background" : "#004080"
        			})
        			$("#dayWorkBtn").css({
        				"background" : "linear-gradient(#007FFE, #004080)"
        			})
        			$("input#sDate").val(moment().format("YYYY-MM-DD"))
        		}else{
        			nd=2;
        			$("#nightWorkBtn").css({
        				"background" : "linear-gradient(#007FFE, #004080)"
        			})
        			$("#dayWorkBtn").css({
        				"background" : "#004080"
        			})
        			$('input:radio[name="nd"][value="2"]').prop('checked', true);
        			$("input#sDate").val(moment().format("YYYY-MM-DD"))
        		}
        	}else{
        		nd=1;
        		$('input:radio[name="nd"][value="1"]').prop('checked', true);
        		$("input#sDate").val(moment().subtract('day', 1).format("YYYY-MM-DD"))
        	}
    	}else{
    		
    	}
    	
		var today = $("#sDate").val();
		var param = "shopId=" + shopId + 
					"&startDateTime=" + today + 
					"&nd=" + nd;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function (data){
				inCycleMachine = 0;
				waitMachine = 0;
				alarmMachine = 0;
				powerOffMachine = 0;
				
				var json = data.machineList;
				
				$(".box").remove();
				

				var machineStatus;
				var cntCyl;
				
				maxTime = new Date(json[0].workDate.substr(0,4), json[0].workDate.substr(5,2), json[0].workDate.substr(8,2), json[0].workDate.substr(11,2),json[0].workDate.substr(14,2), json[0].workDate.substr(17,2))
				$(json).each(function(idx, data){
					var time = data.workDate;
					var uDate = new Date(time.substr(0,4), time.substr(5,2), time.substr(8,2), time.substr(11,2),time.substr(14,2), time.substr(17,2))
					
					if(maxTime<uDate) maxTime = uDate
					
					
					if(moment().format("YYYY-MM-DD")==$("#sDate").val()){
						
						if(nd==2){
							if(hour>=8 && hour<=20){
								cntCyl =  Math.round(data.tgCyl * (hour - (startHour - 12)) /12)
							}else{
								cntCyl =  data.tgCyl
							}
						}else{
							if(hour<=8){
								cntCyl =  Math.round(data.tgCyl * ((hour+24) - startHour)/12)
							}else if(hour>=20){
								cntCyl =  Math.round(data.tgCyl * (hour - startHour)/12)
							}else{
								cntCyl =  data.tgCyl
							}
						} 
					}else{
						if(nd==2){
							cntCyl =  data.tgCyl
						}else{
							cntCyl =  data.tgCyl
						}
					}
				   //현시간 기준 목표 수량 (주간인 경우 : 주간목표 수량 X (현재시간 – 주간시작시간) / 12 )
				   /*if(nd==2){
						cntCyl =  data.tgCyl
					}else{
						cntCyl =  data.tgCyl
					} */
					
					worker = decode(data.worker);
					
					prdctRatio = Math.round(data.cntCyl / cntCyl * 100)
	
					if(cntCyl == 0 || data.cntCyl == 0){
						prdctRatio = 0;
					}
					
					var prdctRatiofontColor = "black";
					var prdctRatioColor;
					if(prdctRatio>=100){
						prdctRatioColor = "#004110";	
						prdctRatiofontColor = "white";
					}else if(prdctRatio>=80){
						prdctRatioColor = "green";
					}else if(prdctRatio>=60){
						prdctRatioColor = "yellow";
					}else if(prdctRatio==0 &&  data.cntCyl==0){
						prdctRatioColor = "rgba(255, 0, 0, 0);";
					}else{
						prdctRatioColor = "red";
					}
					
					if(data.lastChartStatus=="IN-CYCLE"){
						inCycleMachine++;
						machineStatus = incycleColor;
					}else if(data.lastChartStatus=="WAIT"){
						waitMachine++;
						machineStatus = waitColor;
					}else if(data.lastChartStatus=="ALARM"){
						alarmMachine++;
						machineStatus = alarmColor;
					}else if(data.lastChartStatus=="NO-CONNECTION" && data.notUse!=1){
						powerOffMachine++;
						machineStatus = noConnColor;
					};
					
					var box = document.createElement("div");
					box.setAttribute("class", "box");
					box.setAttribute("id", "box" + data.dvcId);
					box.setAttribute("boxId", data.dvcId);	
					
					//수정 박스사이즈 수정
					
					if(data.dvcId==88 || data.dvcId==83 || data.dvcId==84 || data.dvcId==85 || data.dvcId==87 || data.dvcId==27 || data.dvcId==86 || data.dvcId==99){
						var boxWidth = getElSize(140);
						var boxHeight = getElSize(100);
					}else{
						var boxWidth = getElSize(100);
						var boxHeight = getElSize(140);
					}

					box.style.cssText = "position : absolute;" + 
										"width:" + boxWidth + ";" + 
										"z-index : 99;" + 
										"font-size : " + getElSize(20) + "px;" + 
										"height : " + boxHeight + ";" +
										"top : " + (getElSize(data.y) - getElSize(100)) + "px;" + 
										"left : " + (getElSize(data.x) + getElSize(30)) + "px;";
					
					
	
					var cntCylText = document.createTextNode(decode(worker));
					var cntCyl = document.createTextNode(" (" + cntCyl + ")");
					var br = document.createElement("br");
					//var cntCylText = document.createTextNode(data.dvcId + " (" + cntCyl + ")");
					var cntCylSpan = document.createElement("span");
					cntCylSpan.style.cssText = "display : table-cell;" +
											   "white-space : nowrap;" + 
											   "border : 1px solid black;" + 
											   "border-bottom : 0px;"+
											   "vertical-align:middle";
					
					cntCylSpan.append(cntCylText);
					cntCylSpan.appendChild(br);
					cntCylSpan.append(cntCyl);
					
					
					var statusBox = document.createElement("div");
					statusBox.setAttribute("id", "m" + data.dvcId);
					statusBox.style.cssText = "width : " + boxWidth + ";" + 
												"height : " + boxHeight/2 + ";" +
												"font-size: " + getElSize(30) +"px;" +  
												"font-weight : bolder;" + 
												"display : table;" +
												"color : black;" + 
												"text-align : center;" +
												"background-color : " + machineStatus;
					
					statusBox.append(cntCylSpan)
					$(box).append(statusBox)
					
					
					var prdctText = document.createTextNode(data.cntCyl);
					var prdctSpan = document.createElement("span");
					prdctSpan.style.cssText = "display : table-cell;" +
												"vertical-align:middle";
					
					prdctSpan.append(prdctRatio + "% ")
					prdctSpan.append("(" + data.cntCyl + ")")
					//prdctSpan.append(document.createElement("br"))
					
					
					
					var prdctBox = document.createElement("div");
					prdctBox.setAttribute("id", "m" + data.dvcId);
					prdctBox.style.cssText = "width : " + boxWidth + ";" +
												"font-size:" + getElSize(30) +"px;" +
												"height : " + boxHeight/2 + ";" + 
												"display : table;" +
												"font-weight:bolder;" + 
												"color : " + prdctRatiofontColor + ";" +
												"text-align : center;" + 
												"overflow : visible;" + 
												"background-color : " + prdctRatioColor;
					
					prdctBox.append(prdctSpan);
					$(box).append(prdctBox);
					
					
					$("#svg_td").append(box);
					
					$(box).dblclick(function(){
						window.localStorage.setItem("dvcId", data.dvcId);
						window.localStorage.setItem("name", decode(data.name));
						
						location.href=ctxPath + "/chart/singleChartStatus.do?fromDashBoard=true";
					});
					
					/* $(box).draggable({
						start : function(){
							console.log("start")
						},
						drag : function(){
							
						},
						stop : function(){
							var id = $(box).attr("boxId");
							var x = setElSize($(box).offset().left - $("#table").offset().left);
							var y = setElSize($(box).offset().top) 
							
							
							setCirclePos(id, x, y);
						}
					}) */
				});
				
				maxTime = timeFormatter(maxTime)
				
				$(".status_span").remove();
				$(".total").remove();
				var inCycleSpan = "<span id='inCycleSpan' class='status_span'>" + addZero(String(inCycleMachine)) + "</span>";
				$("#container").append(inCycleSpan);
				
				var waitSpan = "<span id='waitSpan' class='status_span'>" + addZero(String(waitMachine)) + "</span>";
				$("#container").append(waitSpan);
				
				var alarmSpan = "<span id='alarmSpan' class='status_span'>" + addZero(String(alarmMachine)) + "</span>";
				$("#container").append(alarmSpan);
				
				var noConnSpan = "<span id='noConnSpan' class='status_span'>" + addZero(String(powerOffMachine)) + "</span>";
				$("#container").append(noConnSpan);
				
				var totalSpan = "<div id='totalSpan' class='total'><div id='total_title'>Total</div><div>" + (inCycleMachine + waitMachine + alarmMachine + powerOffMachine) + "<div></div>";
				$("#container").append(totalSpan);
				
				$("#inCycleSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(100),
					"z-index" : 10,
					"color" : "#A3D800",
					"top" : getElSize(1360) + marginHeight,
					"left" : getElSize(1205) + marginWidth
				});
				
				$("#waitSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(100),
					"z-index" : 10,
					"color" : "#FF9100",
					"top" : getElSize(1640) + marginHeight,
					"left" : getElSize(1470) + marginWidth
				});
				
				$("#alarmSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(100),
					"z-index" : 10,
					"color" : "#EC1C24",
					"top" : getElSize(1920) + marginHeight,
					"left" : getElSize(1205) + marginWidth
				});
				
				$("#noConnSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(100),
					"z-index" : 10,
					"color" : "#CFD1D2",
					"top" : getElSize(1640) + marginHeight,
					"left" : getElSize(940) + marginWidth
				});
				
				$("#totalSpan").css({
					"position" : "absolute",
					"font-size" : getElSize(70),
					"z-index" : 10,
					"color" : "#ffffff",
					"top" : getElSize(1640) + marginHeight,
					"left" : getElSize(1200) + marginWidth,
					"text-align" : "center"
				});
				
				$("#total_title").css({
					"font-size" : getElSize(45),
				});
				
				$(".radioBoxDay").css({
					"left": getElSize(1200),
				    "top": getElSize(140),
				    "position": "absolute",
				    "padding" : getElSize(20)
				})
				
				$(".radioBoxNight").css({
					"left": getElSize(1500),
				    "top": getElSize(140),
				    "position": "absolute",
				    "padding" : getElSize(20)
				})
				
				$(".resetBtn").css({
					"right": getElSize(15),
				    "top": getElSize(100),
				    "position": "absolute",
				    "height" : getElSize(130),
				    "border-radius" : getElSize(30),
				    "border" : "1px solid black",
				    "background" : "yellowgreen",
				    "font-weight" : "bolder",
				    "margin" : getElSize(30),
				    "font-size" : getElSize(60),
				    "display" : "inline"
				}) 
				
				$("#sDate").css({
					"z-index" : "999"
				})
				
				$("#ui-datepicker-div").css({
					"z-index" : "999"
				})
				
				dateInterval = setTimeout(createMachine, 5000);
			}
		});
	};
	
	
	function setCirclePos(id, x, y){
		var url = ctxPath + "/chart/setCirclePos.do";
		var param = "id=" + id + 
					"&x=" + x + 
					"&y=" + y;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				
			}
		});
	}
	
	function timeFormatter(time){
		var year = time.getFullYear();
		var month = addZero(String(time.getMonth()));
		var day = addZero(String(time.getDate()));
		var hour = time.getHours();
		var minute = addZero(String(time.getMinutes()));
		var second = addZero(String(time.getSeconds()));
		
		var ty;
		if(hour>12){
			ty = "오후"
			hour-=12;
		}else{
			ty = "오전"
		}
		
		return today = year + "-" + month + "-" + day + " " + ty + " " + hour + " : " + minute + " : " + second
		
	}
	
	function drawGroupDiv(){
		//LFA RR
		var table = "<table class='gr_table' id='lfa_rr'>" + 
						"<tr>" + 
							"<Td class='label'>TA RR / FRT<br>JC RR</td>" +
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" +
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#lfa_rr").css({
			"left" : getElSize(500) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#lfa_rr").css({
			"width" : getElSize(190)
		});
		
		//YP FRT (cnc)
		var table = "<table class='gr_table' id='yp_frt'>" + 
						"<tr>" + 
							//"<Td class='label'>YP FRT (CNC)<br>${kr}, ${na}</td>" +
							"<Td class='label'>TA FRT <br>OS FRT 15 / 17</td>" +
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#yp_frt").css({
			"left" : getElSize(730) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#yp_frt td").css({
			"width" : getElSize(250)
		});
	
		//UM FRT (CNC)
		var table = "<table class='gr_table' id='um_frt'>" + 
						"<tr>" + 
							"<Td class='label'>OS FRT 16 / UM EU /<br>TM RR 16</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#um_frt").css({
			"left" : getElSize(1030) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#um_frt td").css({
			"width" : getElSize(400)
		});
		
		//TA RR
		var table = "<table class='gr_table' id='ta_Rr'>" + 
						"<tr>" + 
							"<Td class='label'>LFA RR /<br>HR FRT</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#ta_Rr").css({
			"left" : getElSize(1490) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#ta_Rr td").css({
			"width" : getElSize(250)
		});
		
		//TA FRT
		var table = "<table class='gr_table' id='ta_frt'>" + 
						"<tr>" + 
							"<Td class='label'>YP FRT /<br>HR FRT</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#ta_frt").css({
			"left" : getElSize(1800) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#ta_frt td").css({
			"width" : getElSize(250)
		});
		
		
		//JC RR
		var table = "<table class='gr_table' id='jc_rr'>" + 
						"<tr>" + 
							"<Td class='label'>UM FRT /<br>TQ FRT</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#jc_rr").css({
			"left" : getElSize(2100) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#jc_rr td").css({
			"width" : getElSize(250)
		});
		
		//
		var table = "<table class='gr_table' id='jc_rr2'>" + 
						"<tr>" + 
							"<Td class='label'>OS RR 자동</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#jc_rr2").css({
			"left" : getElSize(2400) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#jc_rr2 td").css({
			"width" : getElSize(250)
		});
		
		
		//HR PU/FRT
		var table = "<table class='gr_table' id='hr_pu'>" + 
						"<tr>" + 
							"<Td class='label'>OS RR</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#hr_pu").css({
			"left" : getElSize(2700) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#hr_pu td").css({
			"width" : getElSize(300)
		});
		
		
		//HR PU/FRT
		var table = "<table class='gr_table' id='hr_pu2'>" + 
						"<tr>" + 
							"<Td class='label'>TM FRT 17</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#hr_pu2").css({
			"left" : getElSize(3040) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#hr_pu2 td").css({
			"width" : getElSize(250)
		});
		
		//HR PU/FRT
		var table = "<table class='gr_table' id='hr_pu3'>" + 
						"<tr>" + 
							"<Td class='label'>HR FRT</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#hr_pu3").css({
			"left" : getElSize(3340) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#hr_pu3 td").css({
			"width" : getElSize(250)
		});
		
		
		//TQ FRT
		var table = "<table class='gr_table' id='tq_frt'>" + 
						"<tr>" + 
							"<Td class='label'>TQ FRT /<br>TQ PE</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#tq_frt").css({
			"left" : getElSize(3640) + marginWidth,
			"top" : getElSize(380) + marginHeight,	
		});
		
		$("#tq_frt td").css({
			"width" : getElSize(190)
		});
		
		
		
		
		
		
		var table = "<table class='gr_table' id='yp_rr'>" + 
							"<tr>" + 
								"<Td class='label'>EPB 16 / EPB 15</td>" + 
							"</tr>" + 
							"<tr>" + 
								"<Td class='icon'></td>" + 
							"</tr>" +
						"</table>";
					
					$("#container").append(table);
					
					$("#yp_rr").css({
						"left" : getElSize(2330) + marginWidth,
						"top" : getElSize(1180) + marginHeight,	
					});
					
					$("#yp_rr td").css({
						"width" : getElSize(470),
						"height" : getElSize(250)
					});

		var table = "<table class='gr_table' id='yp_rr2'>" + 
					"<tr>" + 
						"<Td class='label'>YP RR NA/<br>UM RR KR</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";
			
			$("#container").append(table);
			
			$("#yp_rr2").css({
				"left" : getElSize(2850) + marginWidth,
				"top" : getElSize(1180) + marginHeight,	
			});
			
			$("#yp_rr2 td").css({
				"width" : getElSize(150)
			});
			
			
			var table = "<table class='gr_table' id='yp_rr3'>" + 
					"<tr>" + 
						"<Td class='label'>LFA RR</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";
			
			$("#container").append(table);
			
			$("#yp_rr3").css({
				"left" : getElSize(3040) + marginWidth,
				"top" : getElSize(1180) + marginHeight,	
			});
			
			$("#yp_rr3 td").css({
				"width" : getElSize(250)
			});
		
			
			var table = "<table class='gr_table' id='yp_rr4'>" + 
					"<tr>" + 
						"<Td class='label'>YP RR / YP FRT</td>" + 
					"</tr>" + 
					"<tr>" + 
						"<Td class='icon'></td>" + 
					"</tr>" +
				"</table>";
			
			$("#container").append(table);
			
			$("#yp_rr4").css({
				"left" : getElSize(3340) + marginWidth,
				"top" : getElSize(1180) + marginHeight,	
			});
			
			$("#yp_rr4 td").css({
				"width" : getElSize(250)
			});
			
			
			//UM FRT 추가
			var table = "<table class='gr_table' id='yp_rr5'>" + 
			"<tr>" + 
				"<Td class='label'>UM FRT /<br>JS FRT</td>" + 
			"</tr>" + 
			"<tr>" + 
				"<Td class='icon'></td>" + 
			"</tr>" +
			"</table>";
	
			$("#container").append(table);
			
			$("#yp_rr5").css({
				"left" : getElSize(3640) + marginWidth,
				"top" : getElSize(1180) + marginHeight,	
			});
			
			$("#yp_rr5 td").css({
				"width" : getElSize(190)
			});
			
		//UM FRT (MCT)
		/* var table = "<table class='gr_table' id='um_frt_m'>" + 
						"<tr>" + 
							"<Td class='label'>UM FRT (MCT)</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#um_frt_m").css({
			"left" : getElSize(3610) + marginWidth,
			"top" : getElSize(1400) + marginHeight,	
		});
		
		$("#um_frt_m td").css({
			"width" : getElSize(190)
		});
		 */
		
		 /* 
		//YP RR 
		var table = "<table class='gr_table' id='yp_rr'>" + 
						"<tr>" + 
							"<Td class='label'>YP RR<br>${kr}</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#yp_rr").css({
			"left" : getElSize(3200) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#yp_rr td").css({
			"width" : getElSize(174)
		});
		
		//YP FRT 
		var table = "<table class='gr_table' id='yp_frt_m'>" + 
						"<tr>" + 
							"<Td class='label'>YP FRT<br>(MCT)</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#yp_frt_m").css({
			"left" : getElSize(3378) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#yp_frt_m td").css({
			"width" : getElSize(174)
		});
		
		//UM RR
		var table = "<table class='gr_table' id='um_rr'>" + 
						"<tr>" + 
							"<Td class='label'>UM RR<Br>${up}</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#um_rr").css({
			"left" : getElSize(2790) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#um_rr td").css({
			"width" : getElSize(174)
		});
		
		//LFA RR
		var table = "<table class='gr_table' id='lfa_rr_m'>" + 
						"<tr>" + 
							"<Td class='label'>LFA RR<Br>(MCT)</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#lfa_rr_m").css({
			"left" : getElSize(2968) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#lfa_rr_m td").css({
			"width" : getElSize(174)
		});
		
		//YP RR
		var table = "<table class='gr_table' id='yp_rr_a'>" + 
						"<tr>" + 
							"<Td class='label'>YP RR<br>${na}</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#yp_rr_a").css({
			"left" : getElSize(2390) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#yp_rr_a td").css({
			"width" : getElSize(174)
		});
		
		//UM_RR
		var table = "<table class='gr_table' id='um_rr_n'>" + 
						"<tr>" + 
							"<Td class='label'>UM_RR<br>${kr}</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	 	
		$("#container").append(table);
				
		$("#um_rr_n").css({
			"left" : getElSize(2568) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#um_rr_n td").css({
			"width" : getElSize(174)
		});
		
		//FS RR
		var table = "<table class='gr_table' id='fs_rr'>" + 
						"<tr>" + 
							"<Td class='label'>FS RR</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#fs_rr").css({
			"left" : getElSize(1980) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#fs_rr td").css({
			"width" : getElSize(350)
		}); */
		
		//machine status
		var table = "<table class='gr_table' id='m_status'>" + 
						"<tr>" + 
							"<Td class='label'>${opstatus}</td>" + 
					"</table>";
	
		$("#container").append(table);
				
		$("#m_status").css({
			"left" : getElSize(700) + marginWidth,
			"top" : getElSize(1180) + marginHeight,	
		});
		
		$("#m_status td").css({
			"width" : getElSize(1110)
		});
		
		
		
		var table = "<table class='gr_table' id='tmp'>" + 
				"<tr>" + 
					"<Td class='label'>SAMPLE</td>" + 
				"</tr>" + 
				"<tr>" + 
					"<Td class='icon'></td>" +
				"</tr>" +
			"</table>";
		
		$("#container").append(table);
		
		$("#tmp").css({
			"left" : getElSize(500) + marginWidth,
			"top" : getElSize(1180) + marginHeight,	
		});
		
		$("#tmp").css({
			"width" : getElSize(190),
			"height" : getElSize(950)
		});

		
		$(".gr_table").css({
			"position" : "absolute",
			"border-collapse" : "collapse",
			"z-index" : 8
		});
		
		
		
		
		$(".gr_table .label").css({
			"background-color" : "#373737",
			"text-align" : "center",
			"height" : getElSize(70),
			"border" : getElSize(3) + "px solid #535556"
		});
		
		$(".gr_table .icon").css({
			//"height" : getElSize(530),
			"height" : getElSize(750),
			"border" : getElSize(3) + "px solid #535556"

		});
		
		$("#lfa_rr .icon,#yp_frt .icon,  #ta_frt .icon, #jc_rr .icon, #hr_pu .icon").css({
			"height" : getElSize(730),

		});
		
		$("#tq_frt .icon").css({
			"height" : getElSize(1700),

		});
		
		$("#yp_rr, #yp_rr2, #yp_rr3, #yp_rr4, #yp_rr5").css({
			"height" : getElSize(980),

		})
		
		$("#fs_rr .icon").css({
			"height" : getElSize(790),

		});
		
		$(".gr_table td").css({
			"color" : "white",
			"font-size" : getElSize(30),
		});
		
		
		
	};
	
	function drawGroupDiv2(){
		//LFA RR
		var table = "<table class='gr_table' id='lfa_rr'>" + 
						"<tr>" + 
							"<Td class='label'>ATN PK</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" +
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#lfa_rr").css({
			"left" : getElSize(550) + marginWidth,
			"top" : getElSize(400) + marginHeight,	
		});
		
		$("#lfa_rr").css({
			"width" : getElSize(190)
		});
		
		//YP FRT (cnc)
		var table = "<table class='gr_table' id='yp_frt'>" + 
						"<tr>" + 
							"<Td class='label'>TM NRN (CNC)<br>${kr}, ${na}</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#yp_frt").css({
			"left" : getElSize(850) + marginWidth,
			"top" : getElSize(400) + marginHeight,	
		});
		
		$("#yp_frt td").css({
			"width" : getElSize(350)
		});
	
		//UM FRT (CNC)
		var table = "<table class='gr_table' id='um_frt'>" + 
						"<tr>" + 
							"<Td class='label'>VG ONJ (CNC)</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#um_frt").css({
			"left" : getElSize(1310) + marginWidth,
			"top" : getElSize(400) + marginHeight,	
		});
		
		$("#um_frt td").css({
			"width" : getElSize(350)
		});
		
		//TA RR
		var table = "<table class='gr_table' id='ta_Rr'>" + 
						"<tr>" + 
							"<Td class='label'>TM MM</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#ta_Rr").css({
			"left" : getElSize(1770) + marginWidth,
			"top" : getElSize(400) + marginHeight,	
		});
		
		$("#ta_Rr td").css({
			"width" : getElSize(350)
		});
		
		//TA FRT
		var table = "<table class='gr_table' id='ta_frt'>" + 
						"<tr>" + 
							"<Td class='label'>OR TVM</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#ta_frt").css({
			"left" : getElSize(2230) + marginWidth,
			"top" : getElSize(400) + marginHeight,	
		});
		
		$("#ta_frt td").css({
			"width" : getElSize(350)
		});
		
		
		//JC RR
		var table = "<table class='gr_table' id='jc_rr'>" + 
						"<tr>" + 
							"<Td class='label'>VN SS</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#jc_rr").css({
			"left" : getElSize(2690) + marginWidth,
			"top" : getElSize(400) + marginHeight,	
		});
		
		$("#jc_rr td").css({
			"width" : getElSize(350)
		});
		
		
		//HR PU/FRT
		var table = "<table class='gr_table' id='hr_pu'>" + 
						"<tr>" + 
							"<Td class='label'>IK REN/LQN</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#hr_pu").css({
			"left" : getElSize(3150) + marginWidth,
			"top" : getElSize(400) + marginHeight,	
		});
		
		$("#hr_pu td").css({
			"width" : getElSize(350)
		});
		
		
		//TQ FRT
		var table = "<table class='gr_table' id='tq_frt'>" + 
						"<tr>" + 
							"<Td class='label'>JN ERN</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#tq_frt").css({
			"left" : getElSize(3610) + marginWidth,
			"top" : getElSize(400) + marginHeight,	
		});
		
		$("#tq_frt td").css({
			"width" : getElSize(190)
		});
		
		
		//YP RR 
		var table = "<table class='gr_table' id='yp_rr'>" + 
						"<tr>" + 
							"<Td class='label'>UT LM<br>${kr}</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#yp_rr").css({
			"left" : getElSize(3150) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#yp_rr td").css({
			"width" : getElSize(174)
		});
		
		//YP FRT 
		var table = "<table class='gr_table' id='yp_frt_m'>" + 
						"<tr>" + 
							"<Td class='label'>XZ RET<br>(MCT)</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#yp_frt_m").css({
			"left" : getElSize(3328) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#yp_frt_m td").css({
			"width" : getElSize(174)
		});
		
		//UM RR
		var table = "<table class='gr_table' id='um_rr'>" + 
						"<tr>" + 
							"<Td class='label'>PK WE<Br>${up}</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#um_rr").css({
			"left" : getElSize(2690) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#um_rr td").css({
			"width" : getElSize(174)
		});
		
		//LFA RR
		var table = "<table class='gr_table' id='lfa_rr_m'>" + 
						"<tr>" + 
							"<Td class='label'>KMN EQ<Br>(MCT)</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#lfa_rr_m").css({
			"left" : getElSize(2868) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#lfa_rr_m td").css({
			"width" : getElSize(174)
		});
		
		//YP RR
		var table = "<table class='gr_table' id='yp_rr_a'>" + 
						"<tr>" + 
							"<Td class='label'>RQ NN<br>${na}</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#yp_rr_a").css({
			"left" : getElSize(2230) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#yp_rr_a td").css({
			"width" : getElSize(174)
		});
		
		//UM_RR
		var table = "<table class='gr_table' id='um_rr_n'>" + 
						"<tr>" + 
							"<Td class='label'>DS MP<br>${kr}</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	 	
		$("#container").append(table);
				
		$("#um_rr_n").css({
			"left" : getElSize(2408) + marginWidth,
			"top" : getElSize(1270) + marginHeight,	
		});
		
		$("#um_rr_n td").css({
			"width" : getElSize(174)
		});
		
		//FS RR
		var table = "<table class='gr_table' id='fs_rr'>" + 
						"<tr>" + 
							"<Td class='label'>TY BB</td>" + 
						"</tr>" + 
						"<tr>" + 
							"<Td class='icon'></td>" + 
						"</tr>" +
					"</table>";
	
		$("#container").append(table);
				
		$("#fs_rr").css({
			"left" : getElSize(1770) + marginWidth,
			"top" : getElSize(1140) + marginHeight,	
		});
		
		$("#fs_rr td").css({
			"width" : getElSize(350)
		});
		
		//machine status
		var table = "<table class='gr_table' id='m_status'>" + 
						"<tr>" + 
							"<Td class='label'>${opstatus}</td>" + 
					"</table>";
	
		$("#container").append(table);
				
		$("#m_status").css({
			"left" : getElSize(550) + marginWidth,
			"top" : getElSize(1140) + marginHeight,	
		});
		
		$("#m_status td").css({
			"width" : getElSize(1110)
		});
		
		$(".gr_table").css({
			"position" : "absolute",
			"border-collapse" : "collapse",
			"z-index" : 8
		});
		
		$(".gr_table .label").css({
			"background-color" : "#373737",
			"text-align" : "center",
			"height" : getElSize(90),
			"border" : getElSize(3) + "px solid #535556"
		});
		
		$(".gr_table .icon").css({
			"height" : getElSize(530),
			"border" : getElSize(3) + "px solid #535556"

		});
		
		$("#ta_frt .icon, #jc_rr .icon, #hr_pu .icon").css({
			"height" : getElSize(660),

		});
		
		$("#tq_frt .icon").css({
			"height" : getElSize(1660),

		});
		
		$("#yp_rr .icon, #yp_frt_m .icon, #um_rr .icon, #yp_rr_a .icon, #lfa_rr_m .icon, #um_rr_n .icon").css({
			"height" : getElSize(790),

		});
		
		$("#fs_rr .icon").css({
			"height" : getElSize(920),

		});
		
		$(".gr_table td").css({
			"color" : "white",
			"font-size" : getElSize(30),
		});
		
		
	};
	
	function startPageLoop(){
		loopFlag = setInterval(function(){
			location.href=ctxPath + "/chart/singleChartStatus.do";
			
		},1000*5);
	};
	
	var isToday = true;
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setDivPos(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			//"background-color" : "black",
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width(),
		})
		
		$("img").css({
			"display" : "inline"	
		});
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$(".nav_span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$("#status_chart").css({
			"width" : getElSize(800),
			"position" : "absolute",
			"z-index" : 9,
			"top" : getElSize(1300) + marginHeight,
		});
		
		$("#status_chart").css({
			"left" : $("#m_status").offset().left + ($("#m_status").width()/2) - ($("#status_chart").width()/2),
		});
		
		$("#svg").css({
			"width" : $("#table").width() - $("#svg_td").offset().left + marginWidth ,
			"height" : originHeight,
			"left" : $("#svg_td").offset().left,
			"position" :"absolute",
			"z-index" : -9
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$(".sDateBox").css({
			"position" : "absolute",
			"top" : getElSize(130),
			"left" : getElSize(600)
		})
		
		$("#sDate").css({
			"font-size" : getElSize(50) + "px",
			"width" : getElSize(300),
		}).val(getToday().substr(0,10)).change(editTargetDate);
		
		
		$("#legend_table td").css({
			"color" : "white",
			"font-size" : getElSize(30),
			"text-align" : "center"
		});
		
		$("#legend_table tr td:nth(0)").css({
			"border" : getElSize(3) + "px solid white",
		})
		
		$("#legend_table").css({
			"border" : getElSize(3) + "px solid white",
			"position" : "absolute",
			"width" : getElSize(350) + "px",
			"z-index" : 99999,
			"left" : getElSize(1600) + marginWidth + "px",
			"bottom" : getElSize(10) + "px"
		});
		
		$(".legendDiv").css({
			"width" : getElSize(45),
			"height" : getElSize(45)
		});
		
		$(".Selectlabel").css({
			"font-size": getElSize(80)
		})
		
		$("#dayWorkBtn").css({
		    "width": getElSize(350),
		    "height": getElSize(125),
		    "position": "absolute",
		    "top": getElSize(-5),
		    "border-radius": getElSize(13),
		    "font-size": getElSize(80),
		    "font-weight": "bolder",
		    "color" : "white",
		    "border" : "0px"
		})
		
		$("#nightWorkBtn").css({
		    "width": getElSize(350),
		    "height": getElSize(125),
		    "position": "absolute",
		    "top": getElSize(-5),
		    "left": getElSize(85),
		    "border-radius": getElSize(13),
		    "font-size": getElSize(80),
		    "font-weight": "bolder",
		    "color" : "white",
		    "border" : "0px"
		})
		
		$("input[type=checkbox]").css({
			"-webkit-transform": "scale(2)",
			"padding" :  getElSize(30),
			"width" : getElSize(100)
		})
		
	};
	
	function editTargetDate(){
		var now = new Date(); 
		var todayAtMidn = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		
		var selectedDate = $("#sDate").val();
		var s_year = selectedDate.substr(0,4);
		var s_month = selectedDate.substr(5,2);
		var s_day = selectedDate.substr(8,2);

		var specificDate = new Date(s_month + "/" + s_day + "/" + s_year);
		
		if(todayAtMidn.getTime()<specificDate.getTime()){
			alert("오늘 이후의 날짜는 선택할 수 없습니다.");
			$("#sDate").val(getToday().substr(0,10));
			return;
		};
		
		createMachine()
		
		dateTimer = setTimeout(function(){
			/* $("#sDate").val(getToday().substr(0,10)); */
			createMachine();
			clearInterval(dateTimer)
		}, 1000 * 60);
	};

	var dateTimer = null;
	
	var maxTime = "";
	
	function time(){
		$("#time").html("현재시간 : " + getToday() + ", 데이터수신시간 : " + maxTime)
		
		handle = requestAnimationFrame(time)
		
		 
		var hour = new Date().getHours();
		var minute = new Date().getMinutes();
		
	};
	
	
	var dateInterval = null;
	
	$(function(){
		
		$.datepicker.setDefaults({
		    dateFormat: 'yy-mm-dd',
		    prevText: '이전 달',
		    nextText: '다음 달',
		    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		    dayNames: ['일', '월', '화', '수', '목', '금', '토'],
		    dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
		    dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		    showMonthAfterYear: true,
		    yearSuffix: '년'
		});
		
		$("#sDate").datepicker({
			maxDate:0,
			zIndexOffset: 1040,
			onSelect: function (e) {
				var todayDate = moment().format("YYYY-MM-DD");
				if(moment(e).isAfter(todayDate)){
					alert("오늘 이후의 날짜 입니다.")
					$("#sDate").val(moment().format("YYYY-MM-DD"));
				}else{
					createMachine()
				}
			}
		})
		
		
		$('input[type=radio][name=nd]').change(function() {
			createMachine()
	    });
		
		 $("#resetBtn").change(function(){
			 if($("#resetBtn").is(":checked")){
				 isTodayN=false;
				 createMachine()
	        }else{
	        	isTodayN=true;
	        	 createMachine()
	        }
		 })
	})
	
	
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<!-- 현시간 기준 실적 수량(100% 이상 진녹색, 90% 이상 녹색, 70% 이상 노란색,  그외 빨간색) -->
	
	<div>
		<table id="legend_table" style="border-collapse: collapse;">
			<tr>
				<Td colspan="2">작업자 (목표량)</Td>
			</tr>
			<tr>
				<td>
					달성율 (생산량)
				</td>
				<td>
					
				</td>
			</tr>
			<Tr>
				<td>
					100% 이상
				</td>
				<Td>
					<div class='legendDiv' style="background-color:#004110"> </div>
				</Td>
			</Tr>
			<Tr>
				<td>
					80% 이상
				</td>
				<Td>
					<div class='legendDiv' style="background-color:green"> </div>
				</Td>
			</Tr>
			<Tr>
				<td>
					60% 이상
				</td>
				<Td>
					<div class='legendDiv' style="background-color:yellow"> </div>
				</Td>
			</Tr>
			<Tr>
				<td >
					그 외
				</td>
				<Td>
					<div class='legendDiv' style="background-color:red"> </div>
				</Td>
			</Tr>
		</table>
	</div>	
	
	<div id="svg"></div>
	<div id="time"></div>
	<div id="title_right"></div>
	<img alt="" src="${ctxPath }/images/status_chart.png" id="status_chart">
	
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" style="display: none">
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right' style="display: none">
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/monitor_left.png" class='menu_left'  style="display: none">
				</td>
				<td style="disply:inline">
					<div class="sDateBox">
						<label class="Selectlabel" for="sDate"><spring:message code="date_"></spring:message> : </label><input id="sDate" type="text" readonly>
					</div>
					<div class="radioBoxDay">
						<button id="dayWorkBtn" onclick="createMachine(2)"><spring:message code="day"></spring:message></button>
					</div> 
					<div class="radioBoxNight">
						<button id="nightWorkBtn" onclick="createMachine(1)"><spring:message code="night"></spring:message></button>
					</div>
					<div class="resetBtn">
						<input type="checkbox" id="resetBtn"><label class="Selectlabel" for="resetBtn"><spring:message code="view_current_status"></spring:message></label>
					</div>
					
					<img alt="" src="${ctxPath }/images/blue_right.png" class='menu_right' style="display: none">
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'  ></span>
					<img alt="" src="${ctxPath }/images/selected_blue.png" class='menu_left' style="display: none">
				</Td>
				<td rowspan="10" id="svg_td">
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span  class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span  class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
		</table>
	 </div>
	
  	<canvas id="canvas"></canvas>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
		

	<%-- <div id="title_main" class="title"><spring:message code="layout"></spring:message></div> --%>
</body>
</html>	
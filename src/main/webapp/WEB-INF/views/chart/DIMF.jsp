<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>DIMF</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
</script>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/solid-gauge.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/exporting.js"></script>

<script type="text/javascript" src="${ctxPath }/js/chart/highcharts	2.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/canvas.css">
<script type="text/javascript" src="${ctxPath }/js/chart/Chart.js" ></script>
<script type="text/javascript" src="${ctxPath }/js/canvas.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/d3.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/SVG_.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/SVG_draggable.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/svg_controller2.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/canvas_chart.js"></script>

<style>
*{
	font-family:'MalgunGothic';
}

.chartTitle{
	position : absolute;
	top:-100;
	left:20%;
	right:0;
	margin:auto;
	font-size : 50;
}

#chart{
	overflow: hidden;
}

#svg{
	background: url("../images/DashBoard/Background.png");
	background-size : 100% 100%;
}

#canvas{
	background: url("../images/DashBoard/Road.png");
	background-size : 100% 100%;
	z-index: 99999;
}

.title{
	top : 50px;	
	position: absolute;
	z-index: 99;
}

#title_main{
	left: 1300px;
	width: 1000px;
}
#title_left{
	left : 50px;
	width: 300px;
}

#title_right{
	right: 50px;
	width: 300px;
}

#pieChart1{
	position: absolute;
	left : 800px;
	z-index: 99;
	top: 1480px;
	height: 700px;
	width: 750px;
}

#pieChart2{
	position: absolute;
	left : 300px;
	z-index: 99;
	top: 1480px; 
	height: 700px;
	width: 750px;
}

#tableDiv{
	left: 20px;
	width: 550px; 
	position: absolute; 
	z-index: 999;
	top: 400px;
}

#table{
	border: 1px solid white;
	border-collapse: collapse;
	z-index: 999;
	background-color: black;
}

.tr{
	font-size: 30px;
}

.td{
	padding: 10px;
	border: 1px solid white; 
}

#go123{
	position: absolute;
	z-index: 999;
	width: 600;
	bottom : 50px;
	right: 0px;
}
#textBackground{
	background-color: white;
	width: 780px;
	height: 320px;
	z-index: 1;
	position: absolute;
	border-radius : 10px;
	opacity : 1;
	top: 1720;
	left: 2570;
}
#mainText{
	position: absolute;
	z-index: 2;
	font-size: 70px;
	font-weight: bolder;
	text-align: right;
	top: 1730;
	left: 2610;
}
#title_1{
	top : 50px;
	position: absolute;
	z-index: 9999;
}
#title_2{
	right : 50px;
	position : absolute;
	top : 50px;
}
</style> 
<script type="text/javascript">
	$(function(){
		setDivPos();
		
		
		$("#title_main").click(function(){
			location.href = "${ctxPath}/chart/multiVision.do";
		});
	});
	
	function setDivPos(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#Legend").css({
			"bottom" : 50,
			"left" : width/2 - $("#Legend").width()/2	
		});
		
		$("#title_1").css({
			"left" : width/2 - $("#title_1").width()/2
		})
	};
	
	window.addEventListener("keydown", function (event) {
		if(event.keyCode=="32"){
			drawRow();
		};	
	}, true);
	
	var rowNum = 0;
	
	function drawRow(){
		var rows = $(
							"<tr style='color:white; text-align:center' class='tr'>" + 
								"<td width='84px' style='padding:5px' class='td'>" + tableRow[rowNum][0] + "</td>" + 
								"<td width='154px'style='padding:5px' class='td'>" + tableRow[rowNum][1] + "</td>" +
								"<td width='200px'style='padding:5px' class='td'>" + tableRow[rowNum][2] + "</td>" +
								"<td width='110px'style='padding:5px' class='td'>" + tableRow[rowNum][3] + "</td>" +
							"</tr>"); 
		
	
		rows.hide();
	   $('tr:last-child').after(rows);
	  	rows.fadeIn("slow");
		//$("#table").append(rows);
		rowNum++;
	};
	
	var tableRow = [
	                		["1", "5FPM", "04:25", "완료"],
	                		["2", "HF7P", "05:07", "완료"],
	                		["3", "HFMS#1", "06:10", "중단"],
	                		["4", "HFMS#2", "00:01", "중단"],
	                		["5", "D3710", "02:10", "완료"],
	                ];
</script>
</head>


<body>
	<Center><div id="title_1" style="color: white; font-size: 130px; font-weight: bolder;">Shop Operation Status</div></Center>
	<div id="title_2" style="color: white; font-size: 45px; font-weight: bolder;">Doosan Infracore<br>	 Machine Tools</div> 
	<div id="Legend" style="position: absolute; font-size: 50px;">
		<span style="background-color: GREEN; color: green; border: 1px solid black;">범례</span> In-Cycle  
		<span style="background-color: yellow; color: yellow; border: 1px solid black;">범례</span> Wait 
		<span style="background-color: red; color: red; border: 1px solid black;">범례</span> Alarm  
		<span style="background-color: rgb(206,206,206); color: rgb(206,206,206); border: 1px solid black;">범례</span> Power Off  
	</div>

	<!-- <div id="pieChart1"  ></div> -->
	<div id="pieChart2" ></div>
	
	<%-- <img src="${ctxPath }/images/DashBoard/go123.gif" id="go123"> --%>

	<div id="tableDiv" >
		<!-- <center><font style="font-size: 40px;color: white;font-weight: bold;">야간 무인 정지장비</font></center> -->
		<br><br>
		<!-- <table style="width: 100%; text-align: center; color: white;" id="table">
			<tr style="font-size: 30px; font-weight: bold;" class='tr'>
				<td class='td'>No</td>
				<td class='td'>설비명</td>
				<td class='td'>정지시간</td>
				<td class='td'>상태</td>
			</tr>
		</table> -->
	</div>

	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" class="title">
	<%-- <img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right" class="title"> --%>	

	<div id="svg"></div>

	<font id="date"></font>
	<font id="time"></font>
	
	<canvas id="canvas" ></canvas>
</body>
</html>
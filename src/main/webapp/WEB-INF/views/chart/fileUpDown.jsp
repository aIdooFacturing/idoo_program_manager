<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />

<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">


<!-- ######################################## 웹소켓 ~ MQTT 부분 넣기 ######################################## -->		
		<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
		<link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
   	
   		<link rel="stylesheet" href="${ctxPath }/css/filetree.css">
<!-- ######################################## 웹소켓 ~ MQTT 부분 넣기 ######################################## -->



<title>Dash Board</title>
<script type="text/javascript">

	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	
	
	var select_dvcId = "";
	var select_nane = "";
	
	var count = 1;
	
</script>




<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>


<!-- ######################################## 웹소켓 ~ MQTT 부분 넣기 ######################################## -->

		<!-- 4 include the jQuery library -->
		<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
				  crossorigin="anonymous"">
		</script>
		
		<script type="text/javascript" src="${ctxPath }/js/filetree.js"></script>
		
		<script language="javascript" type="text/javascript"> 

		
		// 웹소켓 서버 생성하기 
		var wsUri = "ws://192.168.0.7:8080/DULink/echo";
		var websocket;
		
		if(websocket!==undefined && websocket.readyState!==WebSocket.CLOSED){
//	        alert("websocket already opened.");
	    } else {
	        //#1# 웹소켓 생성 
	    	websocket = new WebSocket(wsUri);
//	        alert("websocket create")
	    }
		
		//#2# 웹소켓 오픈		
	   	websocket.onopen = function(evt){onOpen(evt)};	

	   	//#3# 웹소켓 메세지 보냄
	   	websocket.onmessage = function(evt){onMessage(evt)};
	   	
	   	//#4# 웹소켓 에러
	   	websocket.onerror = function(evt){onError(evt)};
	   	
	   	//#5# 웹소켓 종료
	   	websocket.onclose = function(evt){onClose(evt)};
		
	   	
	/*    	$("#select_dvcId").click(function(){
	   		alert("selectBOX 클릭 !!");;
	   		var value_dvcId = $("#select_dvcId option:selected").val();
	   		if(value_dvcId == "MCM25")
	   			alert("MCM25 클릭 !!");
	   		
	   	}); */
	   	
	 /*   	var value_dvcId = $("#select_dvcId option:selected").val();
	   	$(value_dvcId).click(function(){
	   		alert("selectBOX list 클릭 !!");
	   	}); */
	   		
	   	
		
	   	
	   	
	   	function onOpen(evt) {
	   		// 페이지 접속 (웹소켓 연결/생성 과 동시에 MQTT Subscribe 시작!

	   		
	//        var dvcId = "dvcId=101";
	        
	        /***** 1. 접속시 데이터베이스 연동으로 dvcId & name 값 긁어와서 select box 에서 보여줌 *****/
	        get_dvcId_name_FromDB();
	        
	   		
	        console.log("Program List get get ~~~");
	   		
	        
	   	};
		
		
		
	   	function onMessage(evt) {
	   		console.log("test: " + evt.data);
	   		
	   		/* cmd 분석해서 종류별로 파싱하기
	         * 
	         */
	            
//			alert("MQTT로 넘어온 값? : " + evt.data);
//			$('#dataArea').html(evt.data);
					
			var obj = JSON.parse(evt.data);
			var cmd = obj.cmd;
			console.log("cmd 종류 1 ? " + obj.cmd);
			console.log("cmd 종류 2 ? " + cmd);
//			alert("cmd 종류? " + obj.cmd);
				
//			obj.topic = 'DMT01/browser';
//			$('#dataArea').html(obj.cmd);
//			alert(obj.topic);
				
			switch(cmd){
				case "getPgmList"  :
					callAjax_showList(evt.data);
					break;
					
				case "downloadPgm" :
					callAjax_download(evt.data);
					break;
					
				case "pushPgm"   :
					callAjax_upload(evt.data);
					break;
			}
	   	};
	   	
	   	function onError(evt) {
//			messageArea.value += "!!!!! ERROR !!!!!";
//			alert("!!! ERROR !!!" + evt.data);
	    };
		
	   	function onClose(evt) {
//	    	messageArea.value += "..... CLOSE .....";
//			alert("CLOSE..." + evt.data);
	    };
		
		
	   	
	   	function callAjax_showList(text){		

	   		$('.items').empty();
//	   		alert("다시 그릴거야 ~~~~~~~");
	   		
	   		$.ajax({
	   		 	// 에러났을때 처리부분 	
	   		 	error : function(){			
//	   		 		alert('통신실패!!');
	   		 	},
	   		 					
	   		 	// 성공했을떄 처리부분 
	   		 	// 데이터 핸들링 하는부 부분 				
	   		 	success : function(data){
//	   		 	alert("통신데이터 값 : " + data);
//	   		 	 	alert("넘어온 값? : " + text);
//	   		 	console.log(data);		
//	   			console.log(text);
//	   		 	$('#dataArea').html(data);
//	   		 	$('#dataArea').html(text);
	   		 					
					/* 1) text 에 MQTT subscribe 한 내용이 들어있다 */
	   		 		var obj = JSON.parse(text);
//	   		 		alert(obj.cmd);
	   		 						
//	   		 		obj.topic = '101';
//	   		 	$('#dataArea').html(obj.cmd);
//	   		 		alert(obj.topic);
	   		 						
//	   		 		alert(obj.data);
	   		 												
//	   		 	$("#get_cmd").html(text["cmd"]);

//	   			$("#get_cmd").html(obj.cmd);
//	   			$(".main-tree").append(obj.cmd);
	   							
					
					/* 2) MQTT 내용 중 "name" 데이터들을 중복없이 리스트로 저장한다 */
					var list_name = [];
	   				console.log("before list_name[] : " + list_name);
	   					
	   					

	   		 		$.each(obj.data, function(key, value){
//	   		 		console.log(value);
//	   		 		$("#get_names").append("<li>" + value.name + "</li>\n");
	   					var name = "";
	   					name = value.name;
	   									
	   					console.log("name : " + name);
	   
	   					if($.inArray(name, list_name) == -1) {
	   						list_name.push(name);
	   					}
	   					console.log("after list_name[] : " + list_name);
	   					console.log("size of list_name[] : " + list_name.length);
	   				});
	   					

	   					
						/***** 프로그램 리스트 중복 없이 <li> 요소로 추가하는 함수                   		  *****/
						/***** subscribe 하는동안 계속해서 긁어오는 문제 때문 ... (이전의 sub 죽임!... 해결 ?) *****/
	   				$.each(list_name, function(index, name){
	   					if(document.getElementById(name)){
	   						console.log("이미 있음.. : " + name);
	   					}else{ testIMG(name);
//	   						$(".tree").append("<li id="+name+" "+ "onclick='click_tree_item(\""+name+"\")' class=\"tree-item\">" + name + "</li>\n");	 
	   					}
	   					console.log("<"+index+"> 번째 name : " + name);	
	   				});
					}
	   		});
	   	};
		
	   	
	   	
	   	
		function testIMG(dvcName){
			var name = dvcName;			
// 			var name = "O0002"
 			
 			console.log("1. before count : " + count);
 			
 			var item = "item" + count;
			
 			var tag = "#" + item;
 			
//			$(".shwoProgramList").append('<img class="image_file" id="O0003" src="${ctxPath}/images/icons/img_file.svg">');

			
//	   		$(".shwoProgramList").append('<img class="image_file" id="O0001" src="${ctxPath}/images/icons/img_file.svg">');

			$(".items").append("<div id="+item+">");

			$(tag).append("<img class=\"image_file\" id="+name+" " + "onclick='click_tree_item(\""+name+"\")' src=\"${ctxPath}/images/icons/img_file.svg\" style=\"display:block;\">");
			$(tag).append("<label class=\"name_file\" onclick='click_tree_item(\""+name+"\")'>"+name+"</label>");
			
//			$(".image_file").append("<")
  		
  			$(tag).css({
  				"text-align" : "center",
  				"vertical-align" : "middle"
  			});
  		
	   		$(".image_file").css({
	   			"position" : "relative",
	   			"margin-top" : getElSize(40),
	   			"margin-left" : getElSize(95),
	   			"margin-right" : getElSize(80),
				"width" : getElSize(350),
				"height" : getElSize(350),
				/*
				"margin-left" : getElSize(100),
				"margin-bottom" : getElSize(50),
				*/
				"display" : "block",
		//		"display" : "inline-block", 
/*				"margin-left" : "auto",
				"margin-right" : "auto", */
				
				"float" : "left"
			});
			
			$(".name_file").css({
				"margin-top" : getElSize(400),
				"margin-left" : -getElSize(360),
				"margin-bottom" : getElSize(40),
				"position" : "static",
				"float" : "left",
//				"display" : "inline-block",
				"font-size" : getElSize(80),
				"float" : "left"
			});
			
//			$("#item").appendTo(".showProgramList");
			
/* 	   		$(".image_file #text").css({
				"color" : "black"
			});
	   		
	   		$(".showProgramList").prepend("<label value="+name+" " + "control="+name+"\">"); 
 */	  
 
/* 	   		$(".item").append("<img class=\"image_file\" id="+name+" " + "src=\"${ctxPath}/images/icons/img_file.svg\">");
	   		$(".item").append("<span class=\"caption\">" + name + "</span>");
	   		
	   		$(".item").css({
	   			"vertical-align" : "top",
	   			"display" : "inline-block",
	   			"text-align" : "center",
	   			"width" : getElSize(120)
	   		});
	   		
	   		$(".image_file").css({
	   			"width" : getElSize(100),
	   			"height" : getElSize(100),
	   			"background-color" : "red"
	   		});
	   		
	   		$(".caption").css({
	   			"display" : "block"
	   		}); */
	   		
	   		count++;
			console.log("2. after count : " + count);
		}
		   	
	   	
	   	
	   	
	   	function click_tree_item(clickValue){
			console.log("@");
//			alert(clickValue)
			console.log(clickValue);
			
			var dvcid = 101;
	
//			var dvcid = select_dvcId;
			
			
			var isDown = confirm("선택하신 프로그램 파일 ["+clickValue+"] 을 다운로드 하시겠습니까 ?");
			if(isDown == true) {
				alert ("해당 파일을 다운로드 합니다..");
				callAjax_downProgram(dvcid, clickValue);
			}else {
				alert ("다운로드를 취소합니다.");
			}
								
			// 첫번째 인자 (dvcId) 일단 고정 ....
			// var dvcId = 1
//	 		Pgm_download(1, click_id)
		};
	   	
	   	function callAjax_downProgram(v1, v2){
				var dvcId = v1;
			
			var name = v2;
				
			console.log(dvcId);
			console.log(name);
			
	/* 			$.get('/DULink/chart/controll_downloadPgm.do', function(data){
				alert("data is .. " + data);
				result = data;
				console.log(result);
			});
	*/
			
			var allData = "dvcId="+dvcId+"&name="+name;
			
			$.ajax({
				url : "controll_downloadPgm.do",
				type : 'GET',
				data : allData,
				
				success : function(data){
//					alert("다운로드 get ajax 완료 !");
					
//					alert("download is .. " + data);
	  				result = data;
	  				console.log("download result : " + result);
	  				
//					window.opener.location.reload();
//					self.close();
				},
				error : function(data){
//					alert("error 발생 ~~~~~~");
//					self.close();
				}
			});
		};
	   	
	   	function callAjax_download (text){
	   		console.log("다운로드 시도 결과로 넘어온 MQTT 값 : " + text);
	   		
	   		var obj = JSON.parse(text);
//		 	alert(obj.cmd);
		 										
		 	$.each(obj.data, function(key, value){
//		 		console.log(value);
//		 		$("#get_names").append("<li>" + value.name + "</li>\n");
				var url = "";
				url = value.url;
									
				console.log("url : " + url);
	     	
				window.open(url);
			});
	   	};
	   	
	   	function addFile(ty){
//			let url = ""
			var file, name, fileType;
			var dvcid = "";
//			dvcid = select_dvcId;
			dvcid = "101";			
			
			if(ty=="browser"){
//				url = "www.digitaltwincloud.com/aIdoo/browser/uploadPgm"
				file = $("input[id=browser_file]")[0].files[0];
				console.log("file : " + file);
				name = $("input[id=browser_file]")[0].files[0].name;
				console.log("name : " + name);
				fileType = $("#fileType").val();
				console.log("fileType : " + fileType);
			}else {
				alert("전송이 불가능합니다 !!!");
			}
			
			var formData = new FormData();
			formData.append('file', file);
			formData.append('dvcId', 101);
//			formData.append('dvcId', dvcid);
			formData.append('name', name);
			formData.append('fileType', fileType);
			 
			console.log("formData Length : " + formData.length);

			
			console.log(file);
			console.log(name);
			console.log(fileType);
			console.log("formdata : " + formData ) ;
			
			$.ajax({
			   url: "controll_uploadPgm.do",
			   processData: false,
			   contentType: false,
			   data: formData,
			   type: 'POST',
			   
			   success: function(result){
				   
//				   alert("result : " + result);
				   
				   if(result=="success" + "\n"){
//					   alert("전송 완료");
//					   alert("upload is .. " + result);
					   console.log("upload result : " + result);
				   }else {
//					   alert("전송 오류 ..");
				   }
			   	
			   }, error: function(result){
			       alert("전송 실패");
			   }
			});
			
		};
	   	
	   	
	   	function get_dvcId_name_FromDB(){
	   		
//	   		alert("DB 연동 ajax");
	   		
	   		var resultDB;
	   		
	   		$.ajax({
				   url: "get_Program_DVCID_NAME.do",
//				   dataType : 'json',
	/*  			   processData: false,
				   contentType: false,
				   data: formData,
				   type: 'POST',
	*/ 			   
				   success: function(result){
//					   alert("result : " + result);

//					console.log(result)
					   resultDB = result;
//					   $('#dataArea').html(resultDB);
					   				   
					   var obj = JSON.parse(resultDB);
//					   alert(obj.programList);
					 
					   $.each(obj.programList, function(key, value){
						   var dvcId = "";
						   var name = "";
						   dvcId = value.dvcId;
						   name = value.name;
				       	
						   console.log("dvcId : " + dvcId + " & name : " + name);

//						   $("#select_dvcId").append("<option value="+dvcId+" "+ "onclick='click_select_item(\""+dvcId+"\")'>"+ name + "</option>\n");
						  $("#select_dvcId").append("<option value="+dvcId+">" + name + "</option>\n"); 
						
					   });
					   
//					   alert("dvcId & name is All in SELECT BOX !!!");
					
				   }, error: function(result){
				       alert("연결 실패");
				   }
				});
	   	};
	   	
	   	
	   	
	   	
	   	
	   	function click_select_item(){
	   		var selectBox = document.getElementById("select_dvcId");
	   		
	   		// select element 에서 선택된 option의 value 저장
	   		select_dvcId = selectBox.options[selectBox.selectedIndex].value;
	   		console.log("선택된 select box 의 dvcId 값 ? " + select_dvcId);
	   		
	   		// select element 에서 선택된 option의 text가 저장
	   		select_name = selectBox.options[selectBox.selectedIndex].text;
	   		console.log("선택된 select box 의 name 값 ? " + select_name);
	   		
	   		
	   	};
	   	
	   	
	   	
	   	// 검색 버튼 눌렀을 때 ...!
	   	function getDVCData(){
	   		var dvcid = "101";
//	   		var dvcid = select_dvcId;
		console.log("1. dvcId = " + dvcid);
	   	
	   		var result = "";
	   		
	   		
			/***** select box 에서 선택한 기계의 dvcId 값을 토픽으로 삼아 subscribe를 진행한다. *****/
	        
//			var allData = "dvcId="+select_dvcId;
			var allData = "dvcId="+dvcid;
			console.log("2. allData = " + allData);
			
			$.ajax({
				url : "controll_getPgmList.do",
				type : 'GET',
				data : allData,
				
				success : function(data){
//					alert("다운로드 get ajax 완료 !");
					
//					alert("getList is .. " + data);
	  				result = data;
	  				console.log("getList result : " + result);
	  				
//					window.opener.location.reload();
//					self.close();
				},
				error : function(data){
//					alert("error 발생 ~~~~~~");
//					self.close();
				}
			});
			
				if(result=="success" + "\n"){
					
					console.log("결과가 석세스 엔터 인가 ?")
					
				}else if(result=="success"){
					console.log("결과가 석세스 인가 ?")
				}
	   		
	   		
	   		
			var allData = "dvcId="+select_dvcId+"&name="+select_name;
			
			$.ajax({
				url : "get_selected_DVCID.do",
				type : 'GET',
				data : allData,
				
				success : function(data){
//					alert("다운로드 get ajax 완료 !");
					
//					alert("toss is .. " + data);
	  				result = data;
	  				console.log("toss result : " + result);
	  				
//					window.opener.location.reload();
//					self.close();
				},
				error : function(data){
//					alert("error 발생 ~~~~~~");
//					self.close();
				}
			});
			
	   		
	   	};
	   	
	   	
	   	
	   	
	
	   	
	   	
	   	
	   	function createTree(){
				$('filetree').jstree();
				
				$('#filetree').on("changed.jstree", function(e, data){
					console.log(data.selected);
				});
				
				$('button').on('click', function() {
					$('#filetree').jstree(true).select_node('main-tree');
					$('#filetree').jstree('select_node', 'main-tree');
					$.jstree.reference('#filetree').select_node('main-tree');
				});
		};
	   	
	   	
	   	
	   	
	   	function callAjax_upload(text){			
	   		$.ajax({
	   		 	// 에러났을때 처리부분 	
	   		 	error : function(){			
	   		 		alert('통신실패!!');
	   		 	},
	   		 					
	   		 	success : function(data){
//	    		 	alert("통신데이터 값 : " + data);
//	    		 	alert("넘어온 값? : " + text);
//	    		 	console.log(data);		
//	    			console.log(text);
//	    		 	$('#dataArea').html(data);
//	    		 	$('#dataArea').html(text);
	    		 						
	    		 	var obj = JSON.parse(text);
//	    		 	alert(obj.cmd);
	    		 						
//	    		 	obj.topic = 'DMT01/browser';
//	    		 	$('#dataArea').html(obj.cmd);
//	    		 	alert(obj.topic);
	    		 						
//	    		 	alert(obj.data);
	    		 												
//	    		 	$("#get_cmd").html(text["cmd"]);



//	    			$("#get_cmd").html(obj.cmd);
//	    			$(".main-tree").append(obj.cmd);
	    								
	    								
	    		 	$.each(obj.data, function(key, value){
		//    		 	console.log(value);
		//    		 	$("#get_names").append("<li>" + value.name + "</li>\n");
		    			var rtn = "";
		    			rtn = value.rtn;
		    									
		    			console.log("rtn : " + rtn);
		    			
		    			if(rtn == "success")
//		    				alert("서버로 프로그램 전송이 완료되었습니다. !");
		    				alert("프로그램 전송이 완료되었습니다. !");
	    			});
	    		 	
	    		 	getDVCData();
			 	}
	   		});
	   	};
		
	   	
<!-- ######################################## 웹소켓 ~ MQTT 부분 넣기 ######################################## -->	

</script>


<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
html{
	overflow : hidden;
}
 .k-calendar .k-link {
    color: black !important;
}
.k-calendar .k-header .k-link {
    color: black;
    background: #D5D5D5;
}
.k-calendar .k-footer .k-link {
    color: black;
    background: #D5D5D5;
}
/*요일 */
.k-calendar th {
  background:#D5D5D5;
  color : black; 
}

/* 이전 다음달 날짜 */
.k-calendar, .k-calendar td.k-other-month{
	background : #EAEAEA;
}

#grid .k-grid-header th.k-header{
	background-color: black;
	color:white;
}

#grid .k-grid-header {
	border-color : black;
}

.k-grid tbody > .alarmExp{
	background : red !important;
}

.filebox label { 
	display: inline-block; 
	padding: .5em .75em; 
	color: #999; 
	font-size: inherit; 
	line-height: normal; 
	vertical-align: middle; 
	background-color: #fdfdfd; 
	cursor: pointer; 
	border: 1px solid #ebebeb; 
	border-bottom-color: #e2e2e2; 
	border-radius: .25em; 
} 

	
.filebox input[type="file"] { 	
	/* 파일 필드 숨기기 */ 
	position: absolute; 
	width: 1px; 
	height: 1px; 
	padding: 0; 
	margin: -1px; 
	overflow: hidden; 
	clip:rect(0,0,0,0); 
	border: 0; 
}

/* .filebox input[type="file"] { 
	position: absolute; 
	width: 1px; 
	height: 1px; 
	padding: 0; 
	margin: -1px; 
	overflow: hidden; 
	clip:rect(0,0,0,0); 
	border: 0; 
} 

.filebox label { 
	display: inline-block; 
	padding: .5em .75em; 
	color: #999; font-size: 
	inherit; 
	line-height: normal; 
	vertical-align: middle; 
	background-color: #fdfdfd; 
	cursor: pointer; 
	border: 1px solid #ebebeb; 
	border-bottom-color: #e2e2e2; 
	border-radius: .25em; 
} 
*/

/* named upload */ 
/*
.filebox .upload-name { 
	display: inline-block; 
	padding: .5em .75em; // label의 패딩값과 일치 
	font-size: inherit; 
	font-family: inherit; 
	line-height: normal; 
	vertical-align: middle; 
	background-color: #f5f5f5; 
	border: 1px solid #ebebeb; 
	border-bottom-color: #e2e2e2; 
	border-radius: .25em; 
	-webkit-appearance: none; // 네이티브 외형 감추기 
	-moz-appearance: none; 
	appearance: none; 
} */





		
</style> 
<script type="text/javascript">


const loadPage = () =>{
	createMenuTree("maintenance", "Alarm_Manager")	
}

	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	
/*	function getDvcList(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		
		var url =  ctxPath + "/chart/getDvcNameList.do";
		var param = "shopId=" + shopId + 
					"&line=ALL" + 
					"&sDate=" + today + 
					"&eDate=" + today + " 23:59:59" + 
					"&fromDashboard=" + fromDashboard;
		
		dvcArray = [];
		
		$.ajax({
			url : url,
			data :param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var list = "<option value='all'>${total}</option>";
				
				$(json).each(function(idx, data){
					dvcArray.push(data.id)
					list += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";					
				});

				$("#group").html(list);
				
				getAlarmData();
			}
		});	
	};
*/	
	$(function(){
		setDate();
//		getDvcList();
		//getGroup();
//		createNav("kpi_nav", 2);
		
//		setEl();
//		time();


		setEl2();
		
		
		
		var fileTarget = $('.filebox .upload-hidden');
		fileTarget.on('change', function(){		//값이 변경되면
			if(window.FileReader){	// mordern browser
				var filename = $(this)[0].files[0].name;
			}
			else{	// old IE
				var filename = $(this).val().split('/').pop().split('\\').pop();	//파일명만 추출 
			}
		
			//추출한 파일명 삽입
			$(this).siblings('.upload-name').val(filename);
		})
		
		
		
		
		
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		/* window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		 */
		
//		chkBanner();
		
/*		grid = $("#grid").kendoGrid({
			height:getElSize(1500),
			editable:true,
			dataBound : function(e){
				$("#grid").css("top", getElSize(50));
				$("#grid tbody td").css("font-size",getElSize(40));
				$("#grid tbody td").css("text-align","center");
				$("#grid tbody td").css("color","#000000");
				$("#grid tbody td").css("border-color","#1c1c20");
				$("#grid thead th").css("font-size",getElSize(40));
				$("#grid thead th").css("text-align","center");
				$("#grid thead th").css("background","#2c2c36");
				$("#grid tbody th").css("border-color","#1c1c20");

				
				var items = e.sender.items();
				items.each(function (index) {
					var dataItem = grid.dataItem(this);
				
				})
				
				$('#grid tbody tr').each(function(){
				if($(this).text().indexOf("alarmException")==-1){
				}else{
					$(this).addClass('alarmExp')
				}
				});
				
				$(".k-grid tbody > .alarmExp").css("background","red")
			},
			columns: [{
                field: "name",
                title: "${device}",
                width: getElSize(300),
 //               locked: true,
//				lockable: false,
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },{
                title: "${alarm} ${start_time}",
                field: "startDateTime",
                width: getElSize(440),
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },{
                title: "${alarm} ${end_time}",
                field: "endDateTime",
                width: getElSize(440),
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },{
                title: "${fixed_time}",
                field: "delayTimeSec",
                width: getElSize(230),
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },{
                title: "${alarm}${content}",
                field: "alarm",
                width: getElSize(1000),
 //               locked: true,
//				lockable: false,
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            }
            
            /* {
				field:"line"
//				,locked: true
//				,lockable: false
				,width: getElSize(1)
			}, 
			*/
/*            
           ]
			}).data("kendoGrid");
		
		$("#grid").css("width",getElSize(3710))
		$("#grid").css("height",getElSize(1580))
		$("#grid").css("margin-left",getElSize(50))
		$("#grid").css("border-color", "#222327")
		
		
//		$("#grid").css("width",getElSize(3340))

*/
	});


	
	
	function setEl2(){

		var width = window.innerWidth;
		var height = window.innerHeight;

		/*
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		*/
		
		$("#container").css({
			"width" : contentWidth - getElSize(30)
		})
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"height" : $("#container").height() + getElSize(5),
			"top" : getElSize(360) + marginHeight,
			"padding-top" : getElSize(30),
			"background" : "#222327"
		});
		
		$("#select").css({
			"width" : getElSize(300) + "px"
		});
		
	
		$("#contentTable").css("margin-top", getElSize(30));
	
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
			"height" : getElSize(88),
			"font-size" : getElSize(47)
		})
		
		$("#searchBtn").css({
			"cursor" : "pointer",
			"width" : getElSize(300),
			"height" : getElSize(88),
			"margin-right" : getElSize(600),
			"font-size" : getElSize(47),
			"background" : "#909090",
			"border-color" : "#222327"
		});
		
		$("#sendBtn").css({
			"cursor" : "pointer",
			"width" : getElSize(300),
			"height" : getElSize(88),
			"font-size" : getElSize(47),
			"background" : "#909090",
			"border-color" : "#222327"
		});
		
		$("#fileType").css({
		});
		
		
		
		$(".showProgramList").css({
			"width" : contentWidth - getElSize(100),
			"text-algin" : "center",
			"height" : contentHeight - getElSize(400),
			"margin-left" : getElSize(50),
			"margin-right" : getElSize(50),
			"left" : getElSize(50),
			"top" : getElSize(100),
			"bottom" : getElSize(100),
			"right" : getElSize(50),
		});
		
		
		
/*		
 		$(".filebox label").css({
			"display" : "inline-block",
			"color" : "#999",
			"font-size" : "inherit",
			"line-height" : "normal",
			"vertical-align" : "middle",
			"background-color" : "#fdfdfd",
			"cursor" : "pointer",
			"broder" : "1px solid #ebebeb",
			"border-bottom-color" : "#e2e2e2",
			"border-radius" : ".25em"
		});
		
		$(".filebox input[type='file']").css({
			"position : "
		});
*/		
	
	/* 	 
		$("#browser_file").css({
			"cursor" : "pointer",
			"width" : getElSize(300),
			"height" : getElSize(88),
			"font-size" : getElSize(47),
			"background" : "#909090",
			"border-color" : "#222327"
		});
		 */
		
		$("select").css("font-size",getElSize(47));
		$("select").css("height",getElSize(150));
		$("select").css("width",getElSize(200));
		$("select").css("border-color","#222327");
		
		$("select").css({
            "border": "1px black #999",
            "width" : getElSize(600),
            "height" : getElSize(70),
            "font-family": "inherit",
            "background": "url(${ctxPath}/images/FL/default/btn_drop_menu_default.svg) no-repeat 95% 50%",
            "background-color" : "black",
            "z-index" : "999",
            "border-radius": "0px",
            "-webkit-appearance": "none",
            "-moz-appearance": "none",
            "appearance":"none",
            "background-size" : getElSize(60),
            "color" : "white",
            "border" : "none"
        })
        
        $("select option").css({
            "background-color" : "rgba(0,0,0,5)",
            "color" : "white",
            "border" : "1",
            "transition-delay" : 0.05,
            "border-bottom" : getElSize(10) + "px solid red",
            "text-align-last": "center",
/*             "border-top-color": "rgb(215, 0, 0)",
            "border-right-color": "rgb(215, 0, 0)",
            "border-left-color": "rgb(215, 0, 0)", */
            "padding" : getElSize(15) + "px",
            "font-size": getElSize(52) + "px",
            "transition" : "1s"
        })
		
		$("input").css("height",getElSize(70))
		$("input").css("font-size",getElSize(47));

		$(".text1").css({
			"font-size" : getElSize(50),
			"padding" : getElSize(14),
			"margin-left" : getElSize(30)
		});
		
/* 		$(".k-rid-header tr").css({
			"width" : getElSize(70),
			"border-color" : "#000000"
		});
		
		$(".ui-datepicker-div").css({
            "font-size" : getElSize(100) + "px",
            "color" : "#000000",
            "border-color" : "black",
            "width" : getElSize(600) + "px"
        });
	 */	
		
		
/*		
 		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
	
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
			
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#contentDiv").css({
			"overflow" : "auto",
			"width" : $(".menu_right").width()
		});
		
		$("#insertForm").css({
			"width" : getElSize(3000),
			"position" : "absolute",
			"z-index" : 999,
		});
		
		$("#insertForm").css({
			"left" : $(".menu_right").offset().left + ($(".menu_right").width()/2) - ($("#insertForm").width()/2) - marginWidth,
			"top" : getElSize(400)
		});
		
		$("#insertForm table td").css({
			"font-size" : getElSize(70),
			"padding" : getElSize(15),
			"background-color" : "#323232"
		});
		
		$("#insertForm button, #insertForm select, #insertForm input").css({
			"font-size" : getElSize(60),
			"margin" : getElSize(15)
		});
		
		$(".table_title").css({
			"background-color" : "#222222",
			"color" : "white"
		});
		
		$("#contentTable td").css({
			"color" : "white",
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		$(".btC").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(275)//600
		});
		
		$(".text1").css({
			"font-size" : getElSize(60),
			"background" : "linear-gradient( to bottom, #00A04B, #005026 )",
			"padding" : getElSize(14),
			
		});
*/		
		
		
		$(".pushExc").css({
			"width" : getElSize(50),
			"height" : getElSize(50)
		}); 
		
		
	};	

	
	
	function getGroup(){
		var url = "${ctxPath}/chart/getGroup.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "<option value='all'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.group + "'>" + data.group + "</option>"; 
				});
				
				$("#group").html(option);
				
				getAlarmData();
			}
		});
	};
	
	var className = "";
	var classFlag = true;
	var dataSource;
	
	
	
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			
				<td rowspan="10" id="svg_td" style="vertical-align: top; position:relative; background:#26282c; ">
				
					<table id="contentTable">
						<Tr>
							<td>
								<label class='text1'><spring:message code="device"></spring:message></label>
	<!--  								<select id="group"></select>	-->							
										<select name="select_dvcId" id="select_dvcId" onchange="click_select_item()">
								        	<!-- 정적 리스트없이, DB에서 긁어온 dvcId & name 값으로 select list 생성 -->
								        	<option value=""> 전 체 </option>
								        </select> 
								<%-- <button> <spring:message code="excel"></spring:message></button> --%>
<!--  							<label id="pushExc" class="text1"><input type="checkbox" id="pushExc" class="pushExc">push ${Excluded_included }</label>
-->	
<!-- 								<button id="search" onclick="getAlarmData()" style="cursor: pointer;"><i class="fa fa-search" aria-hidden="true"</i>    검색 </button>   -->
								
									<button id="searchBtn" onclick="getDVCData()" style="cursor: pointer; border-radius:2%;">   검색 </button>
									
<!-- 								<button id="alarmException" class='btC' onclick="alarmException()" style="cursor: pointer;">알람예외</button> -->
<!--								<button id="saverow" class='btC' onclick="saveRow()" style="cursor: pointer;">${save }</button>   -->
							
							
							
							<!-- ######################################## 웹소켓 ~ MQTT 부분 넣기 ######################################## -->
				
												<!-- 0. get DB selectBox ........ 여기서 선택한 장비이 토픽값으로 적용 (?) -->
<!-- 								        		
												<select name="select_dvcId" id="select_dvcId" onchange="click_select_item()">
								        			<!-- 정적 리스트없이, DB에서 긁어온 dvcId & name 값으로 select list 생성 -->
<!-- 								        			<option value="">기계이름</option>
								        		</select> 
								        		
								        		<!-- 2.   getList print part & print DVCID & print TOPIC-->
<!-- 								        		<div id="dataArea" style="font-size:1px; color:white; background-color:black; padding:2px;"></div>	        --> 		
								        		
												
												
												<!-- 1.   browser upload part -->
								       			<!-- 1-1. 파일 선택 -->
								       		
<!-- 								       		</br></br></br></br>
								       		
								       		<div class="filebox">
								       			<input class="upload-name" value="파일선택" disabled="disabled">
								       			
								       			<label for="ex_file">업로드</label>
								       			<input type="file" id="browser_file" class="upload-hidden">
								       		</div>
								       		
								       		</br></br></br></br>
 -->								       		
								       		
  								       			<input type="file" id="browser_file">			 
 								       			       
								       			<!-- 1-2. 파일 종류 선택 -->	
								       			<select id="fileType">
									       			<option value="PGM">NC 프로그램</option>
									       			<option value="TOFS">툴 오프세트</option>
									       			<option value="PRM">파라미터</option>
									       			<option value="PTCH">피치 오차 보수 데이터</option>
									       			<option value="MAC">사용자 매크로 변수</option>
									       			<option value="ZOFS">제로 오프세트 데이터</option>
									       			<option value="RTR">로타리 테이블 자동장치</option>
								       			</select>	
								       			<!-- 1-3. 파일 전송 -->
								        		<button id=sendBtn onclick="addFile('browser')">전송</button><br>
								        
								        
								        		<br/> 
								        		
								        		<!-- 이미지 그리기 테스트 버튼 -->
<!-- 								        		<button id=testIMG onclick="testIMG()"> 이미지 출력 테스트</button>	
 -->
								        		<div class="showProgramList" style="overflow:scroll; height:400px;">
								        			<div class="items">
								        			
								        			</div>
								        		</div>
								        		
								        		
								        		<br/> <br/>
	<!-- 							        	
								        		<!-- 3.   print Program List to filetree 
												<div class="filetree">       
								        			<ul class="main-tree">
								        				<li class="tree-title">Program List</li>
								 						<ul class="tree">
								 							<li class="tree-title">topic...</li>
								 						</ul>
								        			</ul>
												</div>
	 -->
	 							
								<!-- ######################################## 웹소켓 ~ MQTT 부분 넣기 ######################################## -->
							
							
							
							
							
							</td>
							
							
						</Tr>
					</table>
  				
  				

						
					<!-- <div id="wrapper">
						<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" border="1">
						</table>
					</div> -->
					
				</td>
		
			
		</table>
	 </div>
	
</body>
</html>	
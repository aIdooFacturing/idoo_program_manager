<!-- 
최소한의 화면으로 구성된
시작화면 Template
 -->
 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}

.k-grid-header-wrap.k-auto-scrollable table thead tr th{
	text-align: center;
	vertical-align: middle;
}

.k-header.k-grid-toolbar {
	background: linear-gradient( to top, black, gray);
	color: white;
	text-align: center;
}
</style> 
<script type="text/javascript">
	$(function(){
		createNav("inven_nav", 8);
		
		gridTable();
		
		getTable();
		setEl();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		
	});
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function readOnly(container, options){
		container.removeClass("k-edit-cell");
		container.text(options.model.get(options.field));
	}
	
	function gridTable(){
		kendotable = $("#grid").kendoGrid({
			height:getElSize(1680)
			,editable:true
			/* ,filterable: {
			      mode: "row"
			} */
			,toolbar: [{name: "kdate", template : "<input type='text' id='kdate' readOnly='readOnly' style='text-align:center;'>"}, { name: "custom", text: "저장"}]
			,cellClose:  function(e) {
				
				e.model.set("iniohdCnt",e.model.iniohdCnt);
				e.model.set("ohdCnt",Number(e.model.iniohdCnt) + Number(e.model.rcvCnt) - Number(e.model.issCnt) - Number(e.model.notiCnt));

				e.model.set("iniohdCntP",e.model.iniohdCntP);
				e.model.set("ohdCntP",Number(e.model.iniohdCntP) + Number(e.model.rcvCntP) - Number(e.model.issCntP) - Number(e.model.notiCntP));
				
			}
			,columns:[{
					field: "group"
					,title :"Gp"
					,width: getElSize(100)
				},{
					field: "prdNo"
					,title :"소재품번"
					,width: getElSize(320)
				},{
				width: getElSize(20)
				, attributes: {
					style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
				}, headerAttributes: {
					style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
				}
				},{
				title: "소재창고",
				columns: [{
					field: "iniohdCnt"
					, title: "기초 재고"
					, width: getElSize(151)
					, attributes: {
						class: "editable-cell" ,
						style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
					}

				}, {
					field: "rcvCnt"
					, title: "입고"
					, width: getElSize(151)
//					, template : '<input type="text" class="k-input k-textbox" name="rcvCnt" value="#=rcvCnt#" data-bind="value:rcvCnt">'
				},{
					field: "issCnt"
					, title: "불출"
					, width: getElSize(151)
				},{
					field: "notiCnt"
						, title: "불량"
						, width: getElSize(151)
				},{
					field: "ohdCnt"
					, title: "창고 재고"
					, width: getElSize(151)
					, attributes: {
						class: "editable-cell" ,
						style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
					}
				}]
			},{
				width: getElSize(81)
				, attributes: {
					style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
				}, headerAttributes: {
					style: "text-align: center; background-color:white; color:black; font-size:" + getElSize(38)
				}
			},{
				title: "공정창고",
				columns: [{
					field: "iniohdCntP"
					, title: "기초 재고"
					, width: getElSize(151)
					, attributes: {
						class: "editable-cell" ,
						style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
					}
				}, {
					field: "rcvCntP"
					, title: "입고"
					, width: getElSize(151)
				},{
					field: "issCntP"
					, title: "불출"
					, width: getElSize(151)
				},{
					field: "notiCntP"
						, title: "불량"
						, width: getElSize(151)
				},{
					field: "ohdCntP"
//					, template: "<div class='#=ohdCnt#'>#=iniohdCnt+rcvCnt-notiCnt-issCnt#"
					, title: "창고 재고"
					, width: getElSize(151)
					, attributes: {
						class: "editable-cell" ,
						style: "text-align: center; background-color:yellow; color:black !important; font-size:" + getElSize(38)
					}
				}]
			}]
		}).data("kendoGrid");
		
		//저장 버튼클릭시
		$(".k-grid-custom").click(function () {
			$.showLoading();

			var itemlist = kendodata.data();
			var savelist = [];
			
			$(itemlist).each(function(idx,data){
				
				var arr = {};
				arr.prdNo = data.prdNo;
				arr.proj = "0000";
				arr.iniohdCnt = data.iniohdCnt;
				arr.rcvCnt = data.rcvCnt;
				arr.issCnt = data.issCnt;
				arr.notiCnt = data.notiCnt;
				arr.ohdCnt = data.ohdCnt;
				arr.date = data.date;
				savelist.push(arr);
				
				var arr = {};
				arr.prdNo = data.prdNo;
				arr.proj = "0005";
				arr.iniohdCnt = data.iniohdCntP;
				arr.rcvCnt = data.rcvCntP;
				arr.issCnt = data.issCntP;
				arr.notiCnt = data.notiCntP;
				arr.ohdCnt = data.ohdCntP;
				arr.date = data.date;
				
				savelist.push(arr);
			})
			
			var url = "${ctxPath}/chart/stockLastUpSave.do";
			var obj = new Object();
			obj.val = savelist;
	
			var param = "val=" + JSON.stringify(obj);
			
			console.log(savelist);
			console.log(param);
			$.ajax({
				url: url,
				data: param,
				type: "post",
				success: function (data) {
					
					if(data=="success"){
						$.hideLoading();
						getTable()
						alert("저장완료 되었습니다.")
					}
				}
			})
		})
	}
	
	function getTable(){

		$.showLoading(); 
		classFlag = true;
		var tablelist=[];
		var url = "${ctxPath}/chart/getStockUpList.do";
		
		
		
		var param = "deliveryNo=" + $("#deliveryNo").val()+
 					"&sDate=" + moment().subtract(6,"month").format("YYYY-MM-DD") +
					"&eDate=" + moment().format("YYYY-MM-DD") ;  
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				console.log("완성")
				console.log(data.dataList)
				
				var json = data.dataList;
				$("#kdate").val(json[0].date)
				$(json).each(function(idx,data){
					if(idx<=3){
						data.group="A"
					}else if(idx<=3 && idx <=6){
						data.group="B"
					}else if(idx<=6 && idx<=8){
						data.group="C"
					}else{
						data.group="D"
					}
				})
				
				kendodata = new kendo.data.DataSource({
						data: json,
						batch: true,
						editable: false,
						schema: {
							model: {
								id: "id",
								fields: {
									prdNo: { editable: false },
									item: { editable: false },
									
									iniohdCnt: { editable: true ,type:"number"},
									rcvCnt: { editable: false },
									issCnt: { editable: false },
									notiCnt: { editable: false },
									ohdCnt: { editable: true ,type:"number"},


									iniohdCntP: { editable: true ,type:"number"},
									rcvCntP: { editable: false },
									issCntP: { editable: false },
									notiCntP: { editable: false },
									ohdCntP: { editable: true ,type:"number"},

								}
							}
						}
					});
					
		
					kendotable.setDataSource(kendodata);

				
				
				
				
				$.hideLoading(); 
			}
		})
		
	}
	
	
	</script>
</head>
<body>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/order_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
				<div id="grid">
					
				</div>
				<!-- 
				
				이곳에 필요한 DOM 을 작성합니다.
				실질적 화면에 표시되는 부분.
				
				 -->
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back" style="text-align: "></div>
	<span id="intro"></span>
</body>
</html>	
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
</script>
<link rel="stylesheet" href="http://github.hubspot.com/odometer/themes/odometer-theme-train-station.css" />
<script src="http://github.hubspot.com/odometer/odometer.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.2.min.js"></script>

<link rel="stylesheet" href="${ctxPath }/css/canvas.css">
<script type="text/javascript" src="${ctxPath }/js/chart/Chart.js" ></script>
<script type="text/javascript" src="${ctxPath }/js/canvas.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/d3.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/SVG_.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/SVG_draggable.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/svg_controller2.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/canvas_chart.js"></script>

<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/solid-gauge.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/exporting.js"></script>

<script type="text/javascript" src="${ctxPath }/js/chart/highchart.js"></script>


<style>
.chartTitle{
	position : absolute;
	top:-100;
	left:20%;
	right:0;
	margin:auto;
	font-size : 50;
}

#chart{
	overflow: hidden;
}

#svg{
	background: url("../images/DashBoard/Background.png");
	background-size : 100% 100%;
}

#canvas{
	background: url("../images/DashBoard/Road.png");
	background-size : 100% 100%;
	z-index: 99999;
}

.title{
	top : 50px;
	position: absolute;
	z-index: 99;
}

#title_main{
	left: 1300px;
	width: 1000px;
}
#title_left{
	left : 50px;
	width: 300px;
}

#title_right{
	right: 50px;
	width: 300px;
}

#pieChart1{
	position: absolute;
	left : 900px;
	z-index: 99;
	top: 1480px;
	height: 700px;
	width: 650px;
}

#pieChart2{
	position: absolute;
	left : 200px;
	z-index: 99;
	top: 1480px; 
	height: 700px;
	width: 650px;
}

#tableDiv{
	left: 20px;
	width: 550px; 
	position: absolute; 
	z-index: 999;
	top: 400px;
}

#table{
	border: 1px solid white;
	border-collapse: collapse;
	z-index: 999;
	background-color: black;
}

.tr{
	font-size: 30px;
}

.td{
	padding: 10px;
	border: 1px solid white; 
}

#go123{
	position: absolute;
	z-index: 999;
	width: 600;
	bottom : 50px;
	right: 0px;
}
#odometerDiv{
	position: absolute;
	z-index: 99999999;
}
.odometer{
	font-size: 30px;
	z-index: 99999999;
}
</style> 
<script type="text/javascript">
	$(function(){
		setDivPos();
		
	});
	
	function setDivPos(){
		
	};
	
	window.addEventListener("keydown", function (event) {
		if(event.keyCode=="32"){
			drawRow();
		};	
	}, true);
	
	var rowNum = 0;
	
	function drawRow(){
		var rows = $(
							"<tr style='color:white; text-align:center' class='tr'>" + 
								"<td width='84px' style='padding:5px' class='td'>" + tableRow[rowNum][0] + "</td>" + 
								"<td width='154px'style='padding:5px' class='td'>" + tableRow[rowNum][1] + "</td>" +
								"<td width='200px'style='padding:5px' class='td'>" + tableRow[rowNum][2] + "</td>" +
								"<td width='110px'style='padding:5px' class='td'>" + tableRow[rowNum][3] + "</td>" +
							"</tr>"); 
		
	
		rows.hide();
	   $('tr:last-child').after(rows);
	  	rows.fadeIn("slow");
		//$("#table").append(rows);
		rowNum++;
	};
	
	var tableRow = [
	                		["1", "5FPM", "04:25", "완료"],
	                		["2", "HF7P", "05:07", "완료"],
	                		["3", "HFMS#1", "06:10", "중단"],
	                		["4", "HFMS#2", "00:01", "중단"],
	                		["5", "D3710", "02:10", "완료"],
	                ];
</script>
</head>


<body>
	<div id="pieChart1"  ></div>
	<div id="pieChart2" ></div>
	<%-- <img src="${ctxPath }/images/DashBoard/go123.gif" id="go123"> --%>
	<div id="tableDiv" >
		<center><font style="font-size: 40px;color: white;font-weight: bold;">야간 무인 정지장비</font></center>
		<br><br>
		<table style="width: 100%; text-align: center; color: white;" id="table">
			<tr style="font-size: 30px; font-weight: bold;" class='tr'>
				<td class='td'>No</td>
				<td class='td'>설비명</td>
				<td class='td'>정지시간</td>
				<td class='td'>상태</td>
			</tr>
		</table>
	</div>

	<%-- <img src="${ctxPath }/images/DashBoard/title_main.svg" id="title_main" class="title">
	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" class="title">
	<img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right" class="title"> --%>	

	<div id="svg"></div>
	<img alt="" src="${ctxPath }/images/DashBoard/Alarm.svg" id="alarm_line" >

	<font id="date"></font>
	<font id="time"></font>
	
	<canvas id="canvas" ></canvas>
</body>
</html>
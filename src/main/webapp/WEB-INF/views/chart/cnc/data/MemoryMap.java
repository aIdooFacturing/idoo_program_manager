package com.unomic.cnc.data;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.LoggerFactory;

import com.unomic.cnc.CNCUtils;

import common.Logger;

public class MemoryMap
{
	private static final boolean DEBUG = true;
	private final String CLASSNAME = this.getClass().getSimpleName();
	
	private static final int MAX_NUM_OF_AXIS		= 8;
	private static final int MAX_NUM_OF_COORDINATE	= 7;
	private static final int SIZE_OF_ALARM_LIST		= 16;
	private static final int MAX_NUM_OF_SPINDLE		= 6;
	
	private static final int MAX_NUM_OF_TIME        = 4;
	private static final int MAX_G_GROUP			= 32;
	
	private static final int MTES_MAX_TITLE_CHAR	= 50;
	private static final int INDEX_OF_ABSOLUTE		= 0;
	private static final int INDEX_OF_RELATIVE		= 1;
	private static final int INDEX_OF_MACHINE		= 2;
	private static final int INDEX_OF_DIST_TO_GO	= 3;
	
	private static final int AXES_NAME_SIZE         = 8;
	private static final int AXES_POS_SIZE          = 28;
	private static final int SPINDLE_NAME_SIZE      = 8;
	private static final int AUX_CODE_TITLE_SIZE    = 64;
	private static final int NC_STATUS_SIZE         = 16;
	private static final int NC_MODE_SIZE           = 16;
	private static final int PROG_NAME_CHAR_SIZE    = 26;
	private static final int MAIN_PROG_NAME_CHAR_SIZE = 72;
	private static final int PROG_CHAR_SIZE         = 120;

	private static final int ALARM_NUMBER_CHAR_SIZE = 16;
	private static final int ALARM_MESSAGE_CHAR_SIZE= 512;
	private static final int VERSION_CHAR_SIZE      = 40;
	
	private int [] axis_order;
	private byte[][][] axes_name;
	private byte[][] axis_coordinate_system_title;
	private byte[][][] axes;
	private Alarm[] alarm;
	private byte[][] spindle_name;
	private double[] spindle_actual_speed;
	private double[] spindle_target_speed;
	private float[] spindle_load;
	private float actual_feed;
	private float target_feed;
	private int feed_unit;
	private byte[][] time_title_en;
	private long[] time_min;
	private long[] time_msec;
	private short[] G_modal;
	private short[] Aux_code;
	private byte[] Aux_code_title;
	private long S_code;
	private long F_code;
	private String nc_status;
	private String nc_mode;
	private byte[][] part_title;
	private long[] part_num;
	private byte[] prog_name;
	private byte[][] program;
	private long CaretLineNo;
	private int feed_override;
	private byte[] main_prog_name;
	
	private String[] G_modals = new String[MAX_G_GROUP];
	
	private ByteBuffer mByteBuffer;
	
	public MemoryMap()
	{
		// allocate
		mByteBuffer = ByteBuffer.allocate(1024*14);
		
		axis_order = new int[MAX_NUM_OF_AXIS];
		
		// axes_name[0] : Absolute type
		// axes_name[1] : Increment type
		axes_name = new byte[2][MAX_NUM_OF_AXIS][AXES_NAME_SIZE];
		
		axis_coordinate_system_title = new byte[MAX_NUM_OF_COORDINATE][MTES_MAX_TITLE_CHAR];
		axes = new byte[MAX_NUM_OF_COORDINATE][MAX_NUM_OF_AXIS][AXES_POS_SIZE];      // 13.4.23 [10] -> [12]
		alarm = new Alarm[SIZE_OF_ALARM_LIST];
		spindle_name = new byte[MAX_NUM_OF_SPINDLE][SPINDLE_NAME_SIZE];
		spindle_actual_speed = new double[MAX_NUM_OF_SPINDLE];
		spindle_target_speed = new double[MAX_NUM_OF_SPINDLE];
		spindle_load = new float[MAX_NUM_OF_SPINDLE];
		time_title_en = new byte[6][MTES_MAX_TITLE_CHAR];
		time_min = new long[6];
		time_msec = new long[6];
		G_modal = new short[MAX_G_GROUP];	
		Aux_code = new short[32];
		Aux_code_title = new byte[AUX_CODE_TITLE_SIZE];
		part_title = new byte[4][MTES_MAX_TITLE_CHAR];
		part_num = new long[4];
		prog_name = new byte[PROG_NAME_CHAR_SIZE];
		program = new byte[17][PROG_CHAR_SIZE];
		main_prog_name = new byte[MAIN_PROG_NAME_CHAR_SIZE];
	}
	
	public void reload(byte[] buf, int length)
	{
//		CNCUtils.Logi(CLASSNAME, ">>>>>>>>>>>>>>>>>>>> MemoryMap <<<<<<<<<<<<<<<<<<<<");
//		CNCUtils.Logi(CLASSNAME, "DEBUG: " + DEBUG);

		mByteBuffer.put(buf).rewind();
		mByteBuffer.rewind();
		
		// buff�� endian�� LITTLE_ENDIAN���� ����Ѵ�
		mByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
		
		// INT32 axis_order[MAX_NUM_OF_AXIS]
		for(int i=0; i < axis_order.length; i++)
		{
			axis_order[i] = mByteBuffer.getInt();
//			if(DEBUG) CNCUtils.Logi(CLASSNAME, "axis_order["+i+"] " + axis_order[i]);
		}
		
		// char axes_name[2][MAX_NUM_OF_AXIS][3]
		for(int i=0; i < axes_name.length; i++)
		{
			for(int j=0; j < axes_name[i].length; j++)
			{
				axes_name[i][j] = getByte(AXES_NAME_SIZE);
	            for (int k=0;axes_name[i][j].length>k;k++)
	            {
	                //System.out.println("[" + i + "][" + j + "][" + k + "] : " + (char)axes_name[i][j][k]);
	            }
//				if(DEBUG) CNCUtils.Logi(CLASSNAME, "axes_name["+i+"]["+j+"] " + CNCUtils.unicodeEncoding(axes_name[i][j]));
			}
		}
		
		// char axis_cordinate_system_title[MAX_NUM_OF_COORDINATE][MTES_MAX_TITLE_CHAR]
		for(int i=0; i<axis_coordinate_system_title.length; i++)
		{
			axis_coordinate_system_title[i] = getByte(MTES_MAX_TITLE_CHAR);
//			if(DEBUG) CNCUtils.Logi(CLASSNAME, "axis_coordinate_system_title["+i+"] " + CNCUtils.unicodeEncoding(axis_coordinate_system_title[i]));
		}
		
		// char axes[MAX_NUM_OF_COORDINATE][MAX_NUM_OF_AXIS][10];
		for (int i=0; i < axes.length; i++)
		{
			for (int j=0; j < axes[i].length; j++)
			{
				axes[i][j] = getByte(AXES_POS_SIZE);
//				if(DEBUG) CNCUtils.Logi(CLASSNAME, "axes["+i+"]["+j+"] " + CNCUtils.unicodeEncoding(axes[i][j]));
			}
		}
		
		// ALARM alarm[SIZE_OF_ALARM_LIST]; // Use structure type.
		for (int i=0; i < alarm.length; i++)
		{
			int alarm_type;
			byte[] alarm_number;
			byte[] alarm_message;
			byte mtes_action;
			
			alarm_type = unsigned16(mByteBuffer.getShort());
			alarm_number = getByte(ALARM_NUMBER_CHAR_SIZE);
			alarm_message = getByte(ALARM_MESSAGE_CHAR_SIZE);
			mtes_action = (byte) mByteBuffer.getChar();
			
			alarm[i] = new Alarm(alarm_type, alarm_number, alarm_message, mtes_action);
			//if(DEBUG) CNCUtils.Logi(CLASSNAME, "reload -> alarm["+i+"] alarm_type: " + alarm[i].alarm_type + ", alarm_number: " + CNCUtils.unicodeEncoding(alarm_number) + ", alarm_message: " + CNCUtils.unicodeEncoding(alarm_message));
		    
			for (int k=0;alarm_number.length>k;k++)
		    {
		        //System.out.println("[" + k + "] : " + (char)alarm_number[k]);
		    }
			//System.out.println("alarm["+i+"] alarm_type: " + alarm[i].alarm_type + ", alarm_number: " + CNCUtils.unicodeEncoding(alarm_number) + ", alarm_message: " + CNCUtils.unicodeEncoding(alarm_message));
		}
		
		// dummy bytes passes
		//mByteBuffer.position(mByteBuffer.position() + 1);
		
		// char spindle_name[MAX_NUM_OF_SPINDLE][3];
		for (int i=0; i < spindle_name.length; i++)
		{
			spindle_name[i] = getByte(SPINDLE_NAME_SIZE);
//			for (int k=0;spindle_name[i].length>k;k++)
//            {
//                System.out.println("[" + i + "][" + k + "] : " + spindle_name[i][k]);
//            }
//			if(DEBUG) CNCUtils.Logi(CLASSNAME, "spindle_name["+i+"] " + CNCUtils.unicodeEncoding(spindle_name[i]));
		}
		
		// dummy bytes passes
		mByteBuffer.position(mByteBuffer.position() + 2);
		
		// unsigned INT32 spindle_actual_speed[MAX_NUM_OF_SPINDLE];
		for (int i=0; i < spindle_actual_speed.length; i++)
		{
			spindle_actual_speed[i] = mByteBuffer.getDouble();
//			if(DEBUG) CNCUtils.Logi(CLASSNAME, "spindle_actual_speed["+i+"] " + spindle_actual_speed[i]);
		}
		
		// unsigned INT32 spindle_target_speed[MAX_NUM_OF_SPINDLE];
		for (int i=0; i < spindle_target_speed.length; i++)
		{
			spindle_target_speed[i] = mByteBuffer.getDouble();
//			if(DEBUG) CNCUtils.Logi(CLASSNAME, "spindle_target_speed["+i+"] " + spindle_target_speed[i]);
		}
		
		// unsigned float [MAX_NUM_OF_SPINDLE];
		for (int i=0; i < spindle_load.length; i++)
		{
			spindle_load[i] = mByteBuffer.getFloat();
			//if(DEBUG) CNCUtils.Logi(CLASSNAME, "spindle_load["+i+"] " + spindle_load[i]);
		}
		
		// float actual_feed;
		actual_feed = mByteBuffer.getFloat();
//		if(DEBUG) CNCUtils.Logi(CLASSNAME, "actual_feed: " + actual_feed);

		// float target_feed;
		target_feed = mByteBuffer.getFloat();
//		if(DEBUG) CNCUtils.Logi(CLASSNAME, "target_feed: " + target_feed);
		
		// EnFeedType feed_unit;
		feed_unit = unsigned16(mByteBuffer.getShort());
//		if(DEBUG) CNCUtils.Logi(CLASSNAME, "feed_unit: " + feed_unit);
		
		// char time_title_en[6][MTES_MAX_TITLE_CHAR]
		for(int i=0; i<time_title_en.length; i++)
		{
			time_title_en[i] = getByte(MTES_MAX_TITLE_CHAR);
			//if(DEBUG) CNCUtils.Logi(CLASSNAME, "time_title_en["+i+"] " + CNCUtils.unicodeEncoding(time_title_en[i]));
		}
		
		// dummy bytes passes
		mByteBuffer.position(mByteBuffer.position() + 2);
        
		// unsigned INT32 time_min[4]
		for (int i=0; i < time_min.length; i++)
		{
			time_min[i] = unsigned32(mByteBuffer.getInt());
//			if(DEBUG) CNCUtils.Logi(CLASSNAME, "time_min["+i+"] " + time_min[i]);
		}
		
		// unsigned INT32 time_msec[4]
		for (int i=0; i < time_msec.length; i++)
		{
			time_msec[i] = unsigned32(mByteBuffer.getInt());
//			if(DEBUG) CNCUtils.Logi(CLASSNAME, "time_msec["+i+"] " + time_msec[i]);
		}
								
		// unsigned INT16 G_modal[MAX_G_GROUP];
		for (int i=0; i < G_modal.length; i++)
		{
			G_modal[i] = mByteBuffer.getShort();
//			if(DEBUG) CNCUtils.Logi(CLASSNAME, "G_modal["+i+"] " + G_modal[i]);
		}
		
		// unsigned INT16 Aux_code[MAX_G_GROUP];
		for (int i=0; i < Aux_code.length; i++)
		{
			short sh = mByteBuffer.getShort();
			Aux_code[i] = sh;
//			if(DEBUG) CNCUtils.Logi(CLASSNAME, "Aux_code["+i+"] " + Aux_code[i]);
		}
		
		// char Aux_code_title[32]
		Aux_code_title = getByte(AUX_CODE_TITLE_SIZE);
//		if(DEBUG) CNCUtils.Logi(CLASSNAME, "Aux_code_title: " + CNCUtils.unicodeEncoding(Aux_code_title));
		
		// unsigned INT16 S_code;
		S_code = unsigned32(mByteBuffer.getInt());
//		if(DEBUG) CNCUtils.Logi(CLASSNAME, "S_code: " + S_code);
		
		// unsigned INT16 F_code;
		F_code = unsigned32(mByteBuffer.getInt());
//		if(DEBUG) CNCUtils.Logi(CLASSNAME, "F_code: " + F_code);
		
		// nc_status;
		nc_status = CNCUtils.unicodeEncoding(getByte(NC_STATUS_SIZE));
		//System.out.println("nc_status : ["+nc_status+"]");
		//if(DEBUG) CNCUtils.Logi(CLASSNAME, "nc_status: " + nc_status);
		
		// nc_mode;
		nc_mode = CNCUtils.unicodeEncoding(getByte(NC_MODE_SIZE));
		//if(DEBUG) CNCUtils.Logi(CLASSNAME, "nc_mode: " + nc_mode);
		
		// char part_title[4][MTES_MAX_TITLE_CHAR]
		for(int i=0; i<part_title.length; ++i)
		{
			part_title[i] = getByte(MTES_MAX_TITLE_CHAR);
			//if(DEBUG) CNCUtils.Logi(CLASSNAME, "part_title["+i+"] " + CNCUtils.unicodeEncoding(part_title[i]));
		}
		
		// UINT32 part_num[4]
		for(int i=0; i<part_num.length; ++i)
		{
			part_num[i] = unsigned32(mByteBuffer.getInt());
			//if(DEBUG) CNCUtils.Logi(CLASSNAME, "part_num["+i+"] " + part_num[i]);
		}
		
		// char prorg_name[12]
		prog_name = getByte(PROG_NAME_CHAR_SIZE);
		//if(DEBUG) CNCUtils.Logi(CLASSNAME, "prog_name: [" + CNCUtils.unicodeEncoding(prog_name) + "]");
		
		//System.out.println("prog_name : [" + CNCUtils.unicodeEncoding(prog_name) + "]");
		// char program_view[17][60];
		for(int i=0; i < program.length; i++)
		{
			program[i] = getByte(PROG_CHAR_SIZE);
			//if(DEBUG) CNCUtils.Logi(CLASSNAME, "program["+i+"] " + CNCUtils.unicodeEncoding(program[i]));
			//System.out.println("program["+i+"] : [" + CNCUtils.unicodeEncoding(program[i]) + "]");
			/*
			System.out.println("-------------------------------- " + i);
			for(int k=0; k<program[i].length; ++k)
			{
				System.out.println("k: " + program[i][k]);
			}
			System.out.println("--------------------------------");
			*/
		}
		
//		System.out.println("0 : " +String.format("%x",mByteBuffer.get()));
//		System.out.println("1 : " +String.format("%x",mByteBuffer.get()));
//		System.out.println("2 : " +String.format("%x",mByteBuffer.get()));
//		System.out.println("3 : " +String.format("%x",mByteBuffer.get()));
//		System.out.println("4 : " +String.format("%x",mByteBuffer.get()));
//        System.out.println("5 : " +String.format("%x",mByteBuffer.get()));
//        System.out.println("6 : " +String.format("%x",mByteBuffer.get()));
//        System.out.println("7 : " +String.format("%x",mByteBuffer.get()));
//        System.out.println("8 : " +String.format("%x",mByteBuffer.get()));
        
		// dummy bytes passes
		mByteBuffer.position(mByteBuffer.position() + 6);
		
		// INT64 CaretLineNo;
		CaretLineNo = mByteBuffer.getLong();
		feed_override = mByteBuffer.getInt();
		//System.out.println("CaretLineNo : " + CaretLineNo);
		
		//if(DEBUG) CNCUtils.Logi(CLASSNAME, "CaretLineNo: " + CaretLineNo);
		//if(DEBUG) CNCUtils.Logi(CLASSNAME, "feed_override: " + feed_override);
		
		main_prog_name = getByte(MAIN_PROG_NAME_CHAR_SIZE);
		//if(DEBUG) CNCUtils.Logi(CLASSNAME, "main_prog_name: [" + CNCUtils.unicodeEncoding(main_prog_name) + "]");
		
		mByteBuffer.rewind();
		
//		CNCUtils.Logi(CLASSNAME, ">>>>>>>>>>>>>>>>>>>> MemoryMap End <<<<<<<<<<<<<<<<<<<<");
	}
	
	/**
	 * byte[] to string
	 * @param b
	 * @return
	 */
	public String byteToString(byte [] b)
	{
		try
		{
			return new String(b, "EUC-KR").trim();
		} 
		catch (UnsupportedEncodingException e)
		{
		}
		return "";
	}
	
	/**
	 * char[] �� size��ŭ buffer���� �о� �����Ѵ�.
	 * @param length
	 * @return
	 */
	public byte[] getByte(int length)
	{
		byte[] temp = new byte[length];
		mByteBuffer.get(temp);
		return temp;
	}
	
	/*
	 * ########################################
	 * Sean function's
	 * ########################################
	 */ 
	
	public float arr2float (byte[] arr, int start) 
	{
        int i = 0;
        int len = 4;
        int cnt = 0;
        byte[] tmp = new byte[len];

        for (i = start; i < (start + len); i++) 
        {
              tmp[cnt] = arr[i];
              cnt++;
        }

        int accum = 0;
        i = 0;
        for ( int shiftBy = 0; shiftBy < 32; shiftBy += 8 ) 
        {
              accum |= ( (long)( tmp[i] & 0xff ) ) << shiftBy;
              i++;
        }
        return Float.intBitsToFloat(accum);
	}
	
	public float int2float (int bits)
	{
		int s = ((bits >> 31) == 0) ? 1 : -1;
		int e = ((bits >> 23) & 0xff);
		int m = (e == 0) ?
	                 (bits & 0x7fffff) << 1 :
	                 (bits & 0x7fffff) | 0x800000;

		float f = (float)(s * m * Math.pow(2, e-150));
		return f;
	}
	
	
	public String getAbsoluteAxis()
	{
		String makeString = "";
		
		int maxDataIndex = 0;
		
		for(int i=0; i<axis_order.length;i++, maxDataIndex++)
		{
			//Log.i(TAG,"axis order : "+axis_order[i]);
			if(axis_order[i] == 0)
			{
				break;
			}
		}
			
		//Log.d(TAG, "ArrayList index : " + maxDataIndex);
		ArrayList<Axis> axesList = new ArrayList<Axis>(maxDataIndex);
		for(int i=0;i < maxDataIndex; i++ )
		{
			int index = axis_order[i];
			Axis axis = new Axis(CNCUtils.unicodeEncoding(axes_name[0][i]),CNCUtils.unicodeEncoding(axes[INDEX_OF_ABSOLUTE][i]));
			axesList.add(index-1, axis);
		}
		
		for(Axis axis : axesList){
			makeString += axis.toString();
		}
		
		return makeString;
	}
	
	public ArrayList<Axis> getAbsoluteAxisArray()
	{
		int maxDataIndex = 0;
		
		for(int i=0; i<axis_order.length;i++, maxDataIndex++)
		{
			//Log.i(TAG,"axis order : "+axis_order[i]);
			if(axis_order[i] == 0)
			{
				break;
			}
		}
			
		ArrayList<Axis> axesList = new ArrayList<Axis>(maxDataIndex);
		for(int i=0;i < maxDataIndex; i++ )
		{
			int index = axis_order[i];
			Axis axis = new Axis(CNCUtils.unicodeEncoding(axes_name[0][i]),CNCUtils.unicodeEncoding(axes[INDEX_OF_ABSOLUTE][i]));
			axesList.add(index-1, axis);
		}
		
		return axesList;
	}

	public String getRelativeAxis()
	{
		String makeString = "";
		
		int maxDataIndex = 0;
		
		for(int i=0; i<axis_order.length;i++, maxDataIndex++)
			if(axis_order[i] == 0)
				break;

		//Log.d(TAG, "ArrayList index : " + maxDataIndex);
		ArrayList<Axis> axesList = new ArrayList<Axis>(maxDataIndex);
		for(int i=0;i < maxDataIndex; i++ )
		{
			int index = axis_order[i];
			Axis axis = new Axis(CNCUtils.unicodeEncoding(axes_name[1][i]),CNCUtils.unicodeEncoding(axes[INDEX_OF_RELATIVE][i]));
			axesList.add(index-1, axis);			
		}
		
		for(Axis axis : axesList)
		{
			makeString += axis.toString();
		}
		return makeString;
	}

	public ArrayList<Axis> getRelativeAxisArray()
	{
		int maxDataIndex = 0;
		
		for(int i=0; i<axis_order.length;i++, maxDataIndex++)
			if(axis_order[i] == 0)
				break;

		//Log.d(TAG, "ArrayList index : " + maxDataIndex);
		ArrayList<Axis> axesList = new ArrayList<Axis>(maxDataIndex);
		for(int i=0;i < maxDataIndex; i++ )
		{
			int index = axis_order[i];
			Axis axis = new Axis(CNCUtils.unicodeEncoding(axes_name[1][i]),CNCUtils.unicodeEncoding(axes[INDEX_OF_RELATIVE][i]));
			axesList.add(index-1, axis);			
		}
		return axesList;
	}

	public String getMachine()
	{
		String makeString = "";
		
		int maxDataIndex = 0;
		
		for(int i=0; i<axis_order.length;i++, maxDataIndex++)
			if(axis_order[i] == 0)
				break;

		//Log.d(TAG, "ArrayList index : " + maxDataIndex);
		ArrayList<Axis> axesList = new ArrayList<Axis>(maxDataIndex);
		for(int i=0;i < maxDataIndex; i++ )
		{
			int index = axis_order[i];
			Axis axis = new Axis(CNCUtils.unicodeEncoding(axes_name[0][i]),CNCUtils.unicodeEncoding(axes[INDEX_OF_MACHINE][i]));
			axesList.add(index-1, axis);			
		}
		
		for(Axis axis : axesList)
		{
			makeString += axis.toString();
		}
		return makeString;
	}

	public ArrayList<Axis> getMachineArray()
	{
		int maxDataIndex = 0;
		
		for(int i=0; i<axis_order.length;i++, maxDataIndex++)
			if(axis_order[i] == 0)
				break;

		//Log.d(TAG, "ArrayList index : " + maxDataIndex);
		ArrayList<Axis> axesList = new ArrayList<Axis>(maxDataIndex);
		for(int i=0;i < maxDataIndex; i++ )
		{
			int index = axis_order[i];
			Axis axis = new Axis(CNCUtils.unicodeEncoding(axes_name[0][i]),CNCUtils.unicodeEncoding(axes[INDEX_OF_MACHINE][i]));
			axesList.add(index-1, axis);			
		}
		
		return axesList;
	}

	public String getDistToGo()
	{
		String makeString = "";
		int maxDataIndex = 0;
		
		for(int i=0; i<axis_order.length;i++, maxDataIndex++)
			if(axis_order[i] == 0)
				break;

		//Log.d(TAG, "ArrayList index : " + maxDataIndex);
		ArrayList<Axis> axesList = new ArrayList<Axis>(maxDataIndex);
		for(int i=0;i < maxDataIndex; i++ )
		{
			int index = axis_order[i];
			Axis axis = new Axis(CNCUtils.unicodeEncoding(axes_name[0][i]),CNCUtils.unicodeEncoding(axes[INDEX_OF_DIST_TO_GO][i]));
			axesList.add(index-1, axis);			
		}
		
		for(Axis axis : axesList)
		{
			makeString += axis.toString();
		}
		return makeString;
	}

	public ArrayList<Axis> getDistToGoArray()
	{
		int maxDataIndex = 0;
		
		for(int i=0; i<axis_order.length;i++, maxDataIndex++)
			if(axis_order[i] == 0)
				break;

		//Log.d(TAG, "ArrayList index : " + maxDataIndex);
		ArrayList<Axis> axesList = new ArrayList<Axis>(maxDataIndex);
		for(int i=0;i < maxDataIndex; i++ )
		{
			int index = axis_order[i];
			Axis axis = new Axis(CNCUtils.unicodeEncoding(axes_name[0][i]),CNCUtils.unicodeEncoding(axes[INDEX_OF_DIST_TO_GO][i]));
			axesList.add(index-1, axis);			
		}
		return axesList;
	}

	public String getRunTimeMainHHMMSS()
	{
		String makeString = "";
		
		makeString += "" + time_min[0]/60 + "H\n" + (time_min[0]%60) + "M\n" + time_msec[0]/1000 + "S";
		return makeString; 
	}
	
	public String getRunTimeMainHH()
	{
		String makeString = "";
		
		makeString = "" + time_min[0]/60 + "H";
		return makeString; 
	}
	public String getRunTimeMainMM()
	{
		String makeString = "";
		
		makeString = "" + (time_min[0]%60) + "M";
		return makeString; 
	}
	public String getRunTimeMainSS()
	{
		String makeString = "";
		
		makeString = "" + time_msec[0]/1000 + "S";
		return makeString; 
	}
	
	public String getRunTimeHHMMSS()
	{
		String makeString = "";
		
		String m = ""+ (time_min[0]%60);
		if(m.length()==1)
		{
			m = "0"+m;
		}
		String s = ""+ ((time_msec[0]/1000)%60);
		if(s.length()==1)
		{
			s = "0"+s;
		}
		
		makeString += "" + time_min[0]/60 + "H " + m + "M " + s + "S";
		return makeString;
	}
	
	public String getCutTimeHHMMSS()
	{
		String makeString = "";
		
		String m = ""+ (time_min[1]%60);
		if(m.length()==1)
		{
			m = "0"+m;
		}
		String s = ""+ ((time_msec[1]/1000)%60);
		if(s.length()==1)
		{
			s = "0"+s;
		}
		
		makeString += "" + time_min[1]/60 + "H " + m + "M " + s + "S";
		return makeString;
	}
	
	public String getTimerTimeHHMMSS()
	{
		String makeString = "";
		
		String m = ""+ (time_min[2]%60);
		if(m.length()==1)
		{
			m = "0"+m;
		}
		String s = ""+ ((time_msec[2]/1000)%60);
		if(s.length()==1)
		{
			s = "0"+s;
		}
		
		makeString += "" + time_min[2]/60 + "H " + m + "M " + s + "S";
		return makeString;
	}
	
	public String getCycleTimeHHMMSS()
	{
		String makeString = "";
		
		String m = ""+ (time_min[3]%60);
		if(m.length()==1)
		{
			m = "0"+m;
		}
		String s = ""+ ((time_msec[3]/1000)%60);
		if(s.length()==1)
		{
			s = "0"+s;
		}
		
		makeString += "" + time_min[3]/60 + "H " + m + "M " + s + "S";
		return makeString;
	}
	
	public short[] getG_modal()
	{
		return G_modal;
	}
	
	public short[] getAux_code()
	{
		return Aux_code;
	}
	
	public long getS_code(){
		return S_code;
	}

	public String getNcStatus()
	{
		return nc_status;
	}
	public String getNcMode()
	{
		return nc_mode;
	}

	public String getProg_name()
	{
		return CNCUtils.unicodeEncoding(prog_name);
	}
	
	public String getMainProg_name(){
		return CNCUtils.unicodeEncoding(main_prog_name);
	};
		
	public byte[][] getProgram_view()
	{
		return program;
	}

	public double getCaretLineNo()
	{
		return CaretLineNo;
	};
	
	public static long unsigned32(int n) 
	{
		return n & 0xFFFFFFFFL;
	}
	public static int unsigned16(short n) 
	{
		return n & 0xFFFF;
	}
	
	public int getAlarmCount()
	{
		int alarmCount = 0;
		for (int i=0; i < alarm.length; i++)
		{
			if(!CNCUtils.unicodeEncoding(alarm[i].alarm_number).equals(""))
			{
				alarmCount++;
			}
		}
		return alarmCount;
	}
	
	public String getAlarmNumber()
	{
		String alarmNumber = "";
		for (int i=0; i < alarm.length; i++)
		{
	        if(CNCUtils.unicodeEncoding(alarm[i].alarm_number).equals(""))
	        {
	                // nothing
	        }
//	        else if(alarm[i].alarm_type != 0)
	        else 
			{
				alarmNumber = CNCUtils.unicodeEncoding(alarm[i].alarm_number);
				//System.out.println("["+i+"] alarmNumber : " + alarmNumber);
				break;
			}
		}
		return alarmNumber;
	}
	
	public ArrayList<Alarm> getAlarm()
	{
		ArrayList<Alarm> alarmList = new ArrayList<Alarm>(SIZE_OF_ALARM_LIST);
		int j = 0;
		for (int i=0; i < alarm.length; i++)
		{
			if(CNCUtils.unicodeEncoding(alarm[i].alarm_number).equals(""))
			{
				// nothing
			}else{
				Alarm alarml = new Alarm(alarm[i].alarm_type, alarm[i].alarm_number, alarm[i].alarm_message, alarm[i].mtes_action);
	            //if(DEBUG) CNCUtils.Logi(CLASSNAME, "memoryMemory -> alarm["+i+"] alarm_type: " + alarm[i].alarm_type + ", alarm_number: " + CNCUtils.unicodeEncoding(alarm[i].alarm_number) + ", alarm_message: " + CNCUtils.unicodeEncoding(alarm[i].alarm_message));				
				alarmList.add(j, alarml);			
				j++;
			}
		}
		return alarmList;
	}
	
	public String getAlarmArray()
	{
		String txtAlarm = "";
		for (int i=0; i < alarm.length; i++)
		{
			if(CNCUtils.unicodeEncoding(alarm[i].alarm_number).equals(""))
			{
				// nothing
			}else{
				txtAlarm += "" + CNCUtils.unicodeEncoding(alarm[i].alarm_number) + " : " +
				CNCUtils.unicodeEncoding(alarm[i].alarm_message) + ":" + alarm[i].alarm_type + "\n"; 
			}
		}
		return txtAlarm;
	}
	
	public String[] getG_modals()
	{
		for (int i=0; i < G_modal.length; i++)
		{
			String G_modala, G_modalf, G_modald;
			G_modala = "" + G_modal[i];
			
			if(G_modala.length() > 2)
			{
				G_modalf = G_modala.substring(0, 2);
				G_modald = G_modala.substring(2, 3);
				if(G_modald.equals("0"))
				{
					G_modals[i] = G_modalf;
				}
				else
				{
					G_modals[i] = G_modalf + "." + G_modald;
				}
				
			}
			else if(G_modala.length()==2)
			{
				if(G_modala.equals("-1"))
				{
					G_modals[i] = "";
				}
				else
				{
					G_modalf = "0" + G_modala.substring(0, 1);
					G_modald = G_modala.substring(1);
					if(G_modald.equals("0"))
					{
						G_modals[i] = G_modalf;
					}else{
						G_modals[i] = G_modalf + "." + G_modald;
					}
				}
				
			}else{
				if(G_modala.equals("0"))
				{
					G_modals[i] = "0" + G_modala;
				}else{
					G_modals[i] = "0." + G_modala;
				}
			}
		}
		return G_modals;
	}
	
	/** 
	 * 
	 * Jeff Function's
	 * 
	 */
	
	public String getAux_code_title()
	{
		return CNCUtils.unicodeEncoding(Aux_code_title);
	}
	
	public int getFeed_override(){
		return feed_override;
	};
	
	public String getActualFeed()
	{
//		String value = "" + (actual_feed + 0.5f);
		String value = "" + String.format("%.3f", actual_feed);
		
		if(feed_unit == 1)
		{
			value += " mm/rev";
		}
		else
		{
			value += " mm/min";
		}
		return value;
	}
	public String getTargetFeed()
	{
//		String value = "" + (target_feed + 0.5f);
		String value = "" + String.format("%.3f", target_feed);
		if(feed_unit == 1)
		{
			value += " mm/rev";
		}else{
			value += " mm/min";
		}
		return value;
	}
	
	public List<String> getSpindles()
	{
		List<String> spindle_names = new ArrayList<String>();
		for(int i=0; i<spindle_name.length; ++i)
		{
			String name = CNCUtils.unicodeEncoding(spindle_name[i]);
			if(name != null && name.length() != 0)
			{
				spindle_names.add(i, "" + CNCUtils.unicodeEncoding(spindle_name[i]) + " : ");
			}
		}
		return spindle_names;
	}
	
	public List<String> getSpindlesValues()
	{
		List<String> spindle_values = new ArrayList<String>();
		for(int i=0; i<spindle_name.length; ++i)
		{
			String name = CNCUtils.unicodeEncoding(spindle_name[i]);
			if(name != null && name.length() != 0)
			{		    
			    String spindle_spped = "";
		        if (spindle_actual_speed[i] % 1.0 == 0.0) {
		            spindle_spped = (int)spindle_actual_speed[i] + " rpm";    
		        }else{
		            spindle_spped = String.format("%.1f", spindle_actual_speed[i]);
		        }
				spindle_values.add(i, spindle_spped + " | " + String.format("%.0f", spindle_target_speed[i]) + " | " + String.format("%.1f", (spindle_load[i] + 0.05f)) + "%");
			}
		}
		return spindle_values;
	}
	
	public String getSpindle()
	{
		String spindle = "";
		for(int i=0; i<spindle_name.length; ++i)
		{
			String name = CNCUtils.unicodeEncoding(spindle_name[i]);
			if(name != null && name.length() != 0)
			{
				spindle += "" + CNCUtils.unicodeEncoding(spindle_name[i]) + " :\n";
			}
		}
		return spindle;
	}
	
	public String getSpindleValue()
	{
		String spindle = "";
		for(int i=0; i<spindle_name.length; ++i)
		{
			String name = CNCUtils.unicodeEncoding(spindle_name[i]);
			if(name != null && name.length() != 0)
			{
			    String spindle_spped = "";
                if (spindle_actual_speed[i] % 1.0 == 0.0) {
                    spindle_spped = (int)spindle_actual_speed[i] + " rpm";    
                } else {
                    spindle_spped = String.format("%.1f", spindle_actual_speed[i]);
                }
                spindle += spindle_spped + " / " + String.format("%.0f", spindle_target_speed[i]) + " / " + String.format("%.1f", (spindle_load[i] + 0.05f)) + "%\n";
//				spindle += String.format("%.1f", spindle_actual_speed[i]) + "rpm / " + String.format("%.0f", spindle_target_speed[i]) + " / " + String.format("%.1f", (spindle_load[i] + 0.05f)) + "%\n";
			}
		}
		return spindle;
	}
	
	public List<String> getPart_num()
	{
		List<String> arr = new ArrayList<String>();
		for(int i=0; i<part_title.length; ++i)
		{
			String temp = "" + part_num[i];
			if(temp == null || temp.equals(""))
			{
				break;
			}
			arr.add(i, temp);
		}
		return arr;
	}
	
	public List<String> getPart_title()
	{
		List<String> arr = new ArrayList<String>();
		for(int i=0; i<part_title.length; ++i)
		{
			String temp = CNCUtils.unicodeEncoding(part_title[i]);
			if(temp == null || temp.equals(""))
			{
				break;
			}
			arr.add(i, temp);
		}
		return arr;
	}
	
	public List<String> getTimeHHMMSS()
	{
		List<String> arr = new ArrayList<String>();
		for(int i=0; i<time_title_en.length; ++i)
		{
			String makeString = "";
			String m = ""+ (time_min[i]%60);
			if(m.length()==1)
			{
				m = "0"+m;
			}
			String s = ""+ ((time_msec[i]/1000)%60);
			if(s.length()==1)
			{
				s = "0"+s;
			}
			
			makeString += "" + time_min[i]/60 + "H " + m + "M " + s + "S";
			arr.add(makeString);
		}
		return arr;
	}
	
	public List<String> gettTime_title_en()
	{
		List<String> arr = new ArrayList<String>();
		for(int i=0; i<time_title_en.length; ++i)
		{
			if(time_title_en[i].length != 0)
			{
				String temp = CNCUtils.unicodeEncoding(time_title_en[i]);
				if(temp == null || temp.equals(""))
				{
					break;
				}
				arr.add(i, temp);
			}
		}
		
		if(arr.size() == 0){
			return null;
		}else{
			return arr;
		}
	}
	
	public List<String> getAxis_coordinate_system_title()
	{
		List<String> arr = new ArrayList<String>();
		for(int i=0; i<axis_coordinate_system_title.length; ++i)
		{
			String temp = CNCUtils.unicodeEncoding(axis_coordinate_system_title[i]);
			if(temp == null || temp.equals(""))
			{
				break;
			}
			arr.add(i, temp);
		}
		return arr;
	}
	
	public List<Axis> getAbsAxis(int position)
	{
		int maxDataIndex = 0;
		for(int i=0; i<axis_order.length;i++, maxDataIndex++)
		{
			if(axis_order[i] == 0)
			{
				break;
			}
		}

		List<Axis> axesList = new ArrayList<Axis>(maxDataIndex);
		for(int i=0;i < maxDataIndex; i++)
		{
			int index = axis_order[i];
			Axis axis = new Axis(CNCUtils.unicodeEncoding(axes_name[0][i]), CNCUtils.unicodeEncoding(axes[position][i]));
			axesList.add(index-1, axis);
		}
		return axesList;
	}

}

package com.unomic.cnc.communication.params;

import com.unomic.cnc.CNC;

public interface Params extends CNC
{
	public String resizeString();
	public int getMessageLength();
}

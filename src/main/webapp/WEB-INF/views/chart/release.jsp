<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	var handle = 0;
	
	function getPrdNo(){
		var url = "${ctxPath}/chart/getMatInfo.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + decode(data.prdNo) + "'>" + decode(data.prdNo) + "</option>"; 
				});
				
				$("#group").html(option);
				
				//getLotNoByPrdNo();
			}
		});
	};

	function getLotNoByPrdNo(){
		var url = "${ctxPath}/chart/getLotNoByPrdNo.do";
		var param = "prdNo=" + $("#group").val();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.lotNo + "'>" + data.lotNo + "</option>"; 
				});
				
				$("#lotNo").html(option);
				
				//getLotInfo();
			}
		});
	};
	
	var dvcIdArray = [];
	function getDvcIDs(){
		var url = "${ctxPath}/chart/getDvc.do";
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var prdNo = {"dvcId" : 0,
						"dvcName" : "선택"}
				dvcIdArray.push(prdNo)
					
				$(json).each(function(idx, data){
					var prdNo = {"dvcId" : data.dvcId,
								"dvcName" : data.dvcName,}
					dvcIdArray.push(prdNo)
				});
				
						
			}
		});
	};
	
	function dvcName(dvcId) {
		for (var i = 0; i < dvcIdArray.length; i++) {
			if (dvcIdArray[i].dvcId == dvcId) {
				return dvcIdArray[i].dvcName;
			}
	    }
	}
	var className = "";
	var classFlag = true;

	var prdNoList = [];
	
	function checkDuplPrdNo(prdNo){
		if(prdNoList.length==0) {
			prdNoList.push(prdNo);
			return true;
		}
		
		for(var i = 0; i < prdNoList.length; i++){
			if(prdNoList[i] == prdNo){
				return false;
			}
		}
		
		return true;
	};
	function barcode(barcode){
		$.showLoading();
		var url= "${ctxPath}/chart/barcodebulchul.do";
		var param = "barcode=" + barcode;
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			dataType: "text",
			type : "post",
			success : function(data){
				$.hideLoading();
			},error:function(request,status,error){
		        alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
				$.hideLoading();
			}

		})
				
	}
	
	var enter=false;
	function getLotInfo(){
		if($("#lotNo").val()==""){
			alert("소재로트를 입력해주세요");
			enter=false;
			return false
		}else if($("#lotNo").val().length>14){
			barcode($("#lotNo").val())
			return false
		}
		classFlag = true;
		var url = "${ctxPath}/chart/getLotInfo.do";
		
		/* var param = "deliveryNo=" + $("#deliveryNo").val() + 
					"&lotNo=" + $("#lotNo").val(); */
		
		var param = "lotNo=" + $("#lotNo").val();
					
		$.showLoading();
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				cntArray = [];
				$(".alarmTable").empty();
				var tr = "<thead>" + 
							"<Tr style='background-color:#222222'>" +
								"<Td style='width:7%'>" +
									"${lot_no}" +
								"</Td>" +
								"<Td >" +
									"${mat_prd_no}" +
								"</Td>" +
								"<Td>" +
									"${spec}" +
								"</Td>" +
								"<Td>" +
									"${income_cnt}" +
								"</Td>" +
								"<Td>" +
									"${pre_release_cnt}" +
								"</Td>" +
								"<Td>" +
									"${stock}" +
								"</Td>" +
								"<Td style='width:13%'>" +
									"${release_prd}" +
								"</Td>" +
								"<Td>" +
									"${release_cnt}" +
								"</Td>" +
								"<Td style='width:30%'>" +
									"${release_date}" +
								"</Td>" +
								"<Td>" +
									"${divide_lot}" +
								"</Td>" +  
							"</Tr></thead><tbody>";
							
				$(".alarmTable").html(tr)	
//				$(".alarmTable").empty();
				$(json).each(function(idx, data){
					var array = [data.stockCnt, "stock_" + data.id];
					cntArray.push(array);
					
					if(data.seq!="."){
						if(classFlag){
							className = "row2"
						}else{
							className = "row1"
						};
						classFlag = !classFlag;
						
						if(data.stockCnt==0) return;
						
						tr = "<tr class='contentTr " + className + " parent" + data.lotNo + "' >" ;
						tr += "<td id="+data.barcode+"><span>" + data.lotNo + "</span><input type='hidden' id='id" + data.id + "' value=" + data.id + "></td>";
						tr += "<td id='prdNo" + data.id + "'>" + decodeURIComponent(data.prdNo).replace(/\+/gi, " ") + "</td>"; 
						tr += "<td>" + decodeURIComponent(data.spec).replace(/\+/gi, " ") + "</td>";
						tr += "<td>" + data.rcvCnt + "</td>"; 
						tr += "<td id='preSend" + data.id + "'>" + data.sendCnt + "</td>"; 
						tr += "<td>" + data.stockCnt + "</td>"; 
						
						var prdNo=decodeURIComponent(data.prdNo).replace(/\+/gi, " ");
						var url="";
						var param="";
						var options;
						var chk;
						url="${ctxPath}/chart/getmstmatno.do";
						param=	"RWMATNO=" + prdNo;
						options+="<td><select>"
						$.ajax({
							url : url,
							data : param,
							type : "post",
							dataType : "json",
							async: false,
							success : function(data){
								var json = data.dataList;
								
								$(json).each(function(idx, data){
									if(options.indexOf(dvcName(data.dvcId))==-1){	//중복제거
										options += "<option value='" + data.dvcId + "' id='" + data.oprNo + "'>" + dvcName(data.dvcId) + "</option>";
										chk+=data.dvcId;
									} 
									
//									options += "<option value='" + data.dvcName + "'>" + data.dvcName + "</option>";
								});
								
								if(json.length==0){
									options += "<option value='rt/m#1'>장비 없음</option>";
								}
								
								options+="</select></td>";
/*del 수정할곳*/					tr +=options;
							}
						});			
						
						/* if (typeof chk == "undefined"){ //불충공정 값없을때
							tr += "<td><input type='text' id='sendCnt" + data.id + "' value='" + 0 + "' class='stock_" + data.id + "' size='5'> </td>";	
						}else{
							tr += "<td><input type='text' id='sendCnt" + data.id + "' value='" + data.stockCnt + "' class='stock_" + data.id + "' size='5'> </td>";							
						} */

						
						tr += "<td><input type='text' id='sendCnt" + data.id + "' value='" + data.stockCnt + "' class='stock_" + data.id + "' size='5'> </td>";
						
//						tr += "<td ><select id='oprNm" + data.id + "'></select></td>";
						tr += "<td><input type='date' id='date" + data.id + "' class='date' style='font-size : " + getElSize(40) + "'><input type='time' id='time" + data.id + "' class='time' style='font-size : " + getElSize(40) + "'></td>"; 
						tr += "<td><button onclick='addNewLot(this);' class='" + data.lotNo + "' style='padding : " + getElSize(10) + "px'>${divide}</button></td>"; 
						tr += "</tr>";	
						$(".alarmTable").append(tr).css({
							"font-size": getElSize(40),
						});
						
						getOprNm(decodeURIComponent(data.prdNo).replace(/\+/gi, " "), "oprNm" + data.id);
					}
				});
				
				tr = "</tbody>";
				$(".alarmTable").append(tr)
				
				
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(5) + "px solid black"
				});
				
				$("#wrapper").css({
					"height" :getElSize(1550),
					"overflow" : "hidden"
				});
				
				$("button, input, select").css({
					"font-size" : getElSize(40)
				})
				
				$("#wrapper").css({
				});
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$("#wrapper div:last").remove();
				//scrolify($('.alarmTable'), getElSize(1450));
				$("#wrapper div:last").css("overflow", "auto");
				
				$("body, html").css({
					"overflow" : "hidden"	
				})
				
				setToday();
				if(enter==true){
					saveUpdatedStock();
				}
				enter=false;
				$.hideLoading();
			},error:function(request,status,error){
		        alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
	       }

		});
	};
	
	var cntArray = [];
	function addNewLot(obj){
		var lotName = $(obj).attr("class"); 
		var parent = $(".parent" + lotName)[0];
		
		var className = $(obj).parent("td").parent("tr").children("td:nth(7)").children("input").attr("class");
		
		var lotNo = $(obj).parent("td").prev("td").prev("td").prev("td").prev("td").prev("td").prev("td").prev("td").prev("td").prev("td").html();
		var prdNo = $(obj).parent("td").prev("td").prev("td").prev("td").prev("td").prev("td").prev("td").prev("td").prev("td").html();
		var spec = $(obj).parent("td").prev("td").prev("td").prev("td").prev("td").prev("td").prev("td").prev("td").html();
		var rcvCnt = $(obj).parent("td").prev("td").prev("td").prev("td").prev("td").prev("td").prev("td").html();
		 
		 
		var preSend = "0";
		var remainCnt = $(obj).parent("td").prev("td").prev("td").prev("td").prev("td").html(); 
		var oprNm = $(obj).parent("td").prev("td").prev("td").prev("td").html();
		var sendCnt = "<input type='text' value=0 class='" + className + "' size='5'>";
		var date = "<input type='date'  class='date'><input type='time'  class='time'>";
		var dividLot = "<button onclick='delRow(this);' class='" + lotName + "' style='padding : " + getElSize(10) + "px'>${del}</button>";
		
		
		var tr = "<tr class='contentTr'>" + 
					"<td>" + lotNo + "</td>" +
					"<td>" + prdNo + "</td>" + 
					"<td>" + spec + "</td>" + 
					"<td>" + rcvCnt + "</td>" + 
					
					"<td></td>" + 
					"<td> </td>" + 
					"<td>" + oprNm + "</td>" + 
					"<td>" + sendCnt + "</td>" + 
					"<td>" + date + "</td>" + 
					"<td>" + dividLot + "</td>" + 
				"</tr>";
				
		$(parent).after(tr);
		setToday();
		
		$("button, input").css({
			"font-size" : getElSize(40)
		})
	};

	function delRow(el){
		$(el).parent("td").parent("Tr").remove();
	}
	
	function setToday(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		
		$(".date").val(year + "-" + month + "-" + day);
		$(".time").val(hour + ":" + minute);
	};

	
	$(function(){
		
		var target_input = $('#keyword'); // 포커스 인풋
	    var chk_short = true;
		$(document).bind("keydown keyup", function(e) {
	        var key = e.keyCode;
	        var tg = e.target;
	        if(tg.tagName == "INPUT" ||  tg.tagName == "TEXTAREA") return true;
	        
	        var specific = key >= 8 && key <= 46;
	        if(e.type == "keydown") {
	            if(specific) {
	                chk_short = false;
	                return true;
	            }
	            if(!specific && chk_short) {
	            	 $("#lotNo").focus().select();
	                //target_input.focus().select(); return false;
	            }
	            if(e.ctrlKey && e.keyCode == 86){
	            	 $("#lotNo").focus().select();
	            }
	        } else {
	            if(specific) {
	                chk_short = true;
	            }
	        }
	    });
		
		$("#lotNo").focus();
		//getPrdNo();
		createNav("inven_nav", 3);
		getDvcIDs();
		setEl();
		setDate();	
		time();
		
		setEvt();
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function setEvt(){

	};
	
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function goGraph(){
		location.href = "${ctxPath}/chart/jigGraph.do"
	};
	
	function getOprNm(prdNo, id){
		var url = "${ctxPath}/chart/getOprNmList.do";
		var rw = prdNo.indexOf("RW");

		if(rw!=-1){
			rw = "true";
		}else{
			rw = "false";
		}
		
		prdNo = prdNo.replace("유럽","UR");
		
		var param = "prdNo=" + prdNo + 
					"&rw=" + rw;
		
		var options="";
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			//async : false,
			success : function(data){
				var json = data.dataList;
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.val + "'>" + data.val + "</option>";
				});
				$("#" + id).html(options)
			}
		});
		
//		return options;
	};
	
	var valueArray = new Array();

	var valid = true;
	function saveUpdatedStock(){
		valid = true;
		 for(var i = 0; i < cntArray.length; i++){
			var stock =  cntArray[i][0];
			var sum = 0;
			$("." + cntArray[i][1]).each(function(idx, data){
				sum += Number($(data).val());		
			});
			if(stock<sum){
				valid = false;
				alert("${cnt_more_than_stock}");
				$("." + cntArray[i][1] + ":nth(0)").focus();
				return valid;
			}
		 };
		
		valueArray = [];
		var url = "${ctxPath}/chart/saveUpdatedStock.do";
		
		$(".contentTr").each(function(idx, data){
			var id = $(data).children("td:nth(0)").children("input").val();
			var sendCnt = Number($(data).children("td:nth(7)").children("input").val());
			var preSendCnt = Number($(data).children("td:nth(4)").html()) + sendCnt;
			if(sendCnt=="" || sendCnt==0){
				sendCnt = 0;
				preSendCnt= Number($(data).children("td:nth(4)").html());
				//return;
			}
			
			
			var inputCnt = $(data).children("td:nth(3)").html();
			var prdNo = $(data).children("td:nth(1)").html();
			var lotNo = $(data).children("td:nth(0)").children("span").html();
			var date = $(data).children("td:nth(8)").children("input[type='date']").val() + " " + $(data).children("td:nth(8)").children("input[type='time']").val() 
			var oprNm = $(data).children("td:nth(6)").children("select").val(); //장비 ,id값
			var oprNo = $(data).children("td:nth(6)").children("select").children("option:selected").attr("id"); //공정 번호값
			console.log(oprNo)
			var obj = new Object();
			
			obj.id = id;
			obj.preSendCnt = preSendCnt;
			obj.prdNo = prdNo;
			obj.lotNo = lotNo;
			obj.sendCnt = sendCnt;
			obj.date = date;
			obj.oprNm = oprNm;
			obj.inputCnt = inputCnt;
			obj.oprNo= oprNo;
			
			if(valid) valueArray.push(obj);
			if(valid==false){
				$.hideLoading();
				return valid;
			}
		});
		
		var obj = new Object();
		obj.val = valueArray;
		
		var param = JSON.stringify(obj);
		
		
		$.showLoading();
		if(valid==false){
			$.hideLoading();
			return;
		}
		
		console.log(param)
		$.ajax({
			url : url,
			data :"val=" + param,
			type : "post",
			dataType : "text",
			success : function(data){
				console.log(data)
				$.hideLoading()
				getLotInfo();
			}
		});   
	};
	
	
	var auto_save = true;
	
	function chkKeyCd(e){
		if(e.keyCode==13){
			enter=true;
			getLotInfo();
			$("#lotNo").select();
		}
	}
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/inven_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<Tr>
							<td> 
								<!-- 납품번호 
								<input type="text" id="deliveryNo" onkeyup="chkKeyCd(event)"> -->
								소재로트
								<input type="text" id="lotNo" onkeyup="chkKeyCd(event)">
								<%-- <spring:message code="prd_no"></spring:message>
								<select id="group"></select>
								<spring:message code="lot_no"  ></spring:message>&nbsp;<select id="lotNo"></select> --%>
								<img alt="" src="${ctxPath }/images/search.png" id="search" onclick="getLotInfo()">
<%-- 								<button onclick="saveUpdatedStock()"><spring:message code="save" ></spring:message></button> --%>
								<button onclick="saveUpdatedStock()">불출</button>
							</td>
						</Tr>		
						<tr>
							<td>
								<div id="wrapper" style="overflow:auto;">
									<table style="overflow:auto; width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" id="table2" border="1" >
									</table>
								</div>
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	
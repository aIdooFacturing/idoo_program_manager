<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1">
<title>Device List</title>

<link rel="stylesheet" href="${ctxPath}/css/jquery.mobile-1.4.5.min.css">
<%-- <link rel="stylesheet" href="${ctxPath}/css/jqm-demos.css"> --%>

<script src="${ctxPath}/machine_table/machine_table.js"></script>
<script src="${ctxPath}/machine_table/jquery.mobile-1.4.5.min.js"></script>

<script type="text/javascript">

$(function() {
	loadListMachine();
}
	
	function loadListMachine(){
		var url = "${ctxPath}/mobile/listMachine.do";
		
		$.ajax({
			url : url,
			data : param,
			dataType : "html",
			type : "post",
			success : function(data){
				$("#uiListMachine").html("");
				$("#uiListMachine").html(data);
			}
		});
	}
</script>
<style type="text/css">
	
	
</style>
</head>
<body>
	<div data-role="header" data-position="fixed">
    	<h1>Machine List</h1>
	</div>
	<ul id="uiListMachine" style='float:left;width:100%;'>
		
	</ul>

</body>
</html>
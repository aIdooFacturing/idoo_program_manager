<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="/WEB-INF/views/include/unomiclib.jsp"%>
<%@ include file="/WEB-INF/views/pop/lib.jsp"%>

<%@ page import="com.unomic.dulink.common.domain.CommonCode"%>
<%@ page import="com.unomic.dulink.chart.domain.*"%>
<%@ page session = "true" %>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<style>

</style>

<script>

	var evtMsg;
	var nm = '<%=(String)session.getAttribute("nm")%>';
		nm = decode(nm);
	var empCd = '<%=(String)session.getAttribute("empCd")%>';
	
	$(function(){
		
		//session 있는지 체크
		sessionChk();

		getTable();
		
		$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		msg();
		
		//focus TEXT 맞추기
		var chk_short = true;
		
		$(document).bind("keydown keyup", function(e) {
	        var key = e.keyCode;
	        var tg = e.target;
	        if(tg.tagName == "INPUT" ||  tg.tagName == "TEXTAREA") return true;
	        
	        var specific = key >= 8 && key <= 46;
	        if(e.type == "keydown") {
	            if(specific) {
	                chk_short = false;
	                return true;
	            }
	            if(!specific && chk_short) {
	            	 $("#empCd").focus().select();
	                //target_input.focus().select(); return false;
	            }
	            if(e.ctrlKey && e.keyCode == 86){
	            	 $("#empCd").focus().select();
	            }
	        } else {
	            if(specific) {
	                chk_short = true;
	            }
	        }
	    });
		
		//시간
		setInterval(function() {
			$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		}, 1000);
	})
	
	
	function sessionChk(){
		if(empCd==null || empCd=="null"){
			location.href='${ctxPath}/pop/popIndex.do';
		}
		
	}
		//enter key event
	function enterEvt(event) {
		if(event.keyCode == 13){

		}
	}
	//사원 바코드를 입력해주세요
	function alarmMsg(){
		clearTimeout(evtMsg)
		return evtMsg = setTimeout(function() {$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText'>"+ nm +"</marquee></span>")}, 10000)
	}
	function msg(){
		$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText'>"+ nm +" 님 반갑습니다.</marquee></span>")
	}
	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	//작업리스트 조회 후 장비 선택했을시
	function dvcSelect(e){
		console.log(e)
		console.log(e.closest("tr"))
		row = e.closest("tr");
		
		var dvcId = row.cells[1].getAttribute("class");
		var ty = row.cells[1].getAttribute("name");
		var name = encodeURIComponent(row.cells[1].innerHTML);
		location.href="${ctxPath}/pop/selectStartJob.do?nm="+nm+"&name="+name+"&empCd="+empCd+"&dvcId="+dvcId+"&ty="+ty;
		
/* 		$("#searchText").html('<div align="center" style="height: 10%"> 바코드 : <input type="text" id="empCd" onkeyup="enterEvt(event)" style="vertical-align: middle;"></div>')
		$("#jobList").empty()
		$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText' style='color:blue;'>소재 바코드를 입력해주세요.</marquee></span>") */

	}
	
	function getTable(){
		$.showLoading(); 
		
		var url = "${ctxPath}/pop/getStartJobList.do";
		
		
		
		var param = "empCd=" + empCd+
 					"&sDate=" + moment().subtract(6,"month").format("YYYY-MM-DD") +
					"&eDate=" + moment().format("YYYY-MM-DD") ;  
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				var table = "<table id='test' style='width:100%;text-align:center; font-size:200%; border-spacing: 0 20;'><tr><td colspan='4' style='text-align:right;'><input type='button' id='addDevice' value='장비추가' onclick='location.href=\"${ctxPath}/pop/addDevice.do?empCd="+ empCd +"\"' style='vertical-align: middle; margin-right:5%; font-size:200%; '></td></tr>"
					table += "<tr><th>작업자</th><th>장비</th><th>공정</th><th>선택</th></tr>"
//					table += '<tr><td colspan="6"><input type="button" value="asdas" style="vertical-align: middle; margin-left: 50%;"></td></tr>'								location.href='${ctxPath}/pop/popIndex.do'
				$(json).each(function(idx,data){
					data.name = decode(data.name);
					data.encode = data.name;
					nm = data.nm
					data.nm = decode(data.nm);
					if(data.ty=="0010"){
						data.tyNm = "R삭"
					}else if(data.ty=="0020"){
						data.tyNm = "MCT"
					}else if(data.ty=="0030"){
						data.tyNm = "CNC"
					}

					table += "<tr><td>" + data.nm + "</td>"
					table += "<td class=" + data.dvcId +" name="+ data.ty +">" + data.name + "</td>"
					table += "<td>" + data.tyNm + "</td>"
					table += "<td>" + "<input type='button' value='선택' style='font-size:100%;' onclick=dvcSelect(this)>" + "</td></tr>"
						
				});
					
				$("#jobList").empty()
				$("#jobList").append(table)
				
				clearTimeout(evtMsg)
				$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText' style='color:blue;'>[" + decode(nm) +"]님 작업할 장비를 선택해주세요.</marquee></span>")
				
				$.hideLoading(); 
			},error : function(request,status,error){
				if(request.responseText=="no"){
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:red;'>등록된 사원번호가 아닙니다.</marquee></span>")
					$("#jobList").empty()
					alarmMsg();
				}else{
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:blue;'>관리자에게 문의해주세요.</marquee></span>")
					$("#jobList").empty()
					alarmMsg();
					
				}
				$.hideLoading(); 
//				 alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
				   


			}
		});
		
	}
	
	
</script>

<body>
	<div id="header">
		<div id="leftH">
			<span></span>
		</div>
		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
			<span>부광정밀공업(주)</span>
		</div>
		<div id="rightH">
			<span id="time"></span>
		</div>
	</div>
	<div id="aside">
		<span>
			<marquee behavior=alternate scrollamount="20">
			</marquee>
		</span>
	</div>
	<div id="content">
		<div id="jobList">
		</div>
	</div>
</body>
</html>
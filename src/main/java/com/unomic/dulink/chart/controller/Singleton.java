package com.unomic.dulink.chart.controller;

import java.io.IOException;

import javax.websocket.Session;

public class Singleton {
	
	public static Session session_test;

	private static Singleton singletonWSS = new Singleton();
	private Singleton() {
		System.out.println("웹소켓 싱글톤 생성..");
	}
	public static Singleton getInstance() {
		return singletonWSS;
	}
	
	public static void socket_send(String msg) throws IOException {
		if(session_test.isOpen()) {
			session_test.getBasicRemote().sendText(msg);				
		} else {
			System.out.println("연결 안되었음");
		}
	}
}
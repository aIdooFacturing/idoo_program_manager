package com.unomic.dulink.chart.controller;

import javax.annotation.PostConstruct;

//MQTT
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class MQTT_subscribe {	
	
	public String MQTT_test_S = "";
	
//	private static String topic_is = "test/topic";
//	private static String topic_is = "test/+";
//	private static String topic_is = "101/browser";
	

	
//	private static String topic_is = "";
	private static String topic_is = "101/browser";

	
	
	private static String message_is = "";
	
	private static Boolean isSub = false;
	private static MqttClient previous_client;
	private static MqttClient suber;
	
	/* MQTT_SUB 생성자 호출 
	 * : subscribe 작업 시작 
	 */
	public MQTT_subscribe() throws MqttException {
//		final String MQTT_BROKER_IP = "tcp://192.168.0.138:1883";
		/* 
		 * MQTT 서버 : 192.168.0.21
		 * MQTT 포트 : 1883 (기본)
		 */
//		final String MQTT_BROKER_IP = "tcp://192.168.0.21:1883";
		final String MQTT_BROKER_IP = "tcp://106.240.234.114:1883";
		
	//		MqttClient suber = new MqttClient(
			suber = new MqttClient( 
	 			MQTT_BROKER_IP, //URI 
	 			MqttClient.generateClientId(), //ClientId 
	 			new MemoryPersistence());    
		
			if(!isSub) {
				System.out.println("첫 sub 이시네요 ..");
				previous_client = suber;
				isSub = true;
			}else {
				System.out.println("1) sub 경험이 있으시군요 !");
				previous_client.unsubscribe(topic_is);
				System.out.println("2) 이전의 client는 지워버릴게요..");
				previous_client = suber;
				System.out.println("3) 새로운 client 여 오라 ~!!");
			}
		
			
	 	suber.connect();    	
	 	System.out.println("test mqtt client : " + suber.toString());    
		
// 	
// 	// topic 이 와일드카드(+)를 포함하기 때문에 여기서 subscribe 함...
// 	suber.subscribe(topic_is, 2);
 	
// 	suber.subscribe(topic_is, 0);
//	
//	System.out.println("SUB 작업 시작해봅니다 ..");
	
	}


	//	@PostConstruct
	// 토큰 및 메세지 정보로 subscribe 하는 부분 
	public void init(String selected_topic) throws MqttException {	
//		suber.subscribe(topic_is, 0);
		
//		topic_is = selected_topic + "/browser";
		topic_is = "101/browser";
		
		suber.subscribe(topic_is, 0);
		System.out.println("topic : [" + topic_is + "] 으로 SUB 작업 시작합니다 ..");
		
		
		
		
     	
     suber.setCallback(new MqttCallback() {
      
	        /* Called when the client lost the connection to the broker. 
	        * 이 콜백은 MQTT 연결이 끊어지면 호출된다. 							 
		    */  
	        @Override
		    public void connectionLost(Throwable cause) { 
	        	System.out.println("Connection Lost ..!");
		    }
	        		
		    /* 이 콜백은 클라이언트가 게시한 메시지가 브로커에 의해 성공적으로 수신될 때 호출된다.
		     */
		    @Override
		    public void deliveryComplete(IMqttDeliveryToken token) {
		    	System.out.println("************* 111 *************");
		    	try {
		    		System.out.println("************* 222 *************");
					System.out.println("Pub complete" + token.getMessage());
					System.out.println("************* 333 *************");
				} catch (MqttException e) {
					System.out.println("************* 444 *************");
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
		        		    		
		    /* MQTT 브로커로부터 메시지를 받았을 때, 실제로 호출되는 콜백 함수
		     * 실제 비즈니스 로직을 구현하여 결과를 확인할 수 있는 부분 
		     * arg0 : topic 이름
		     * arg1 : message 내용									
		     */
		    @Override
		    public void messageArrived(String topic, MqttMessage msg) throws Exception {
		    
		    	try {
		    		System.out.println("현재 토픽이 무엇입니까 ? " + topic);
			    	
			    	if(topic.equals(topic_is)) {
			    		System.out.println("토픽이 일치합니다.. sub 작업을 수행합니다 ..");
			    		
			    		/* message_is : sub 한 내용을 담는 변수
				    	 * 내용 1) getPgmList => cmd , data [name]
				    	 * 내용 2) downloadPgm => cmd , data [url]
				    	 * 내용 3) uploadPgm => cmd , data [rtn]
				    	 */
				    	message_is = msg.toString();
				    	System.out.println("내용 : " + message_is);
				    	
				    	JSONParser parser = new JSONParser();
		        		Object obj = parser.parse(message_is);
		        		System.out.println("ok ???");
		        		System.out.println(obj);
/*		        		
		        		JSONArray arr_gPL = (JSONArray)parser.parse(message_is);
		        		JSONObject object = (JSONObject)arr_gPL.get(0);
		        		
		        		System.out.println((String)object.get("cmd"));

		        		JSONArray arr_gPL_data = (JSONArray)object.get("data");
		        		for(int i=0 ; i<arr_gPL_data.size(); i++) {
		        			JSONObject ObjectInArray = (JSONObject)arr_gPL_data.get(i);
		        			String name = (String)ObjectInArray.get("name");
		        			System.out.println("name: " + name);
		        		}
	        			
		        		
//		        			JSONObject object2 = (JSONObject)ja.get(1);
//		        			for(int i=0 ; i<object2.size(); i++) {
//		        				System.out.println(object2.get("name"));
//		        			}
*/		        		
		        		JSONObject jsonObj = (JSONObject)obj;
//		        		JSONObject dataObj = (JSONObject)jsonObj.get("cmd");
		        		System.out.println(jsonObj);
		        		System.out.println("test ???");
		        		
		        		
		        		/* cmd 의 내용에 따라서, 파싱 키 다르게 적용 
		        		 * cmd 1) getPgmList
		        		 * cmd 2) downloadPgm
		        		 * cmd 3) uploadPgm
		        		 */

		        		String cmd = (String)jsonObj.get("cmd");
		        		System.out.println("cmd : " + cmd);
		        		
		        		switch(cmd) {
		        			case "getPgmList" :
		        				JSONArray arr1 = (JSONArray)jsonObj.get("data");		        				
		                		for(int i=0 ; i<arr1.size(); i++) {
		                			JSONObject ObjectInArray = (JSONObject)arr1.get(i);
		                			String name = (String)ObjectInArray.get("name");
		                			System.out.println("name: " + name);
		                		}
		        				break;
		        				
		        			case "downloadPgm" :
		        				JSONArray arr2 = (JSONArray)jsonObj.get("data");		        				
		                		for(int i=0 ; i<arr2.size(); i++) {
		                			JSONObject ObjectInArray = (JSONObject)arr2.get(i);
		                			String url = (String)ObjectInArray.get("url");
		                			System.out.println("url: " + url);
		                		}
		        				break;
		        				
		        			case "pushPgm" :
		        				JSONArray arr3 = (JSONArray)jsonObj.get("data");		        				
		                		for(int i=0 ; i<arr3.size(); i++) {
		                			JSONObject ObjectInArray = (JSONObject)arr3.get(i);
		                			String url = (String)ObjectInArray.get("url");
		                			System.out.println("url: " + url);
		                		}
		        				break;
		        		}
			    		
		        		Singleton MtoS = Singleton.getInstance();
				        MtoS.socket_send(message_is);
				        System.out.println("MQTT -> SOCKET");	
			    		
			    		
			    	}else {
			    		System.out.println("토픽이 일치하지 않습니다! 토픽을 확인해주세요 ..");
			    	}
		    	} catch (Exception e) {
		    		e.printStackTrace();
		    	}
		    	

		    	
		    	
		    	
/*		    	
		        switch (topic) {
		        	case "test/" :
		        		topic_is = topic.toString();
	//			        System.out.println("들어오는 토픽 ? 0 " + topic_is);
		        		System.out.println("들어오는 토픽 ? 0 " + topic);
		        		message_is = msg.toString();
		        		System.out.println("들어오는 메세지 ? 0 " + message_is);
				        			
		        		break;
				        			
		        	case "test/topic" :
				        topic_is = topic.toString();
	//			        System.out.println("들어오는 토픽 ? 1 " + topic_is);
				        System.out.println("들어오는 토픽 ? 1 " + topic);
			//	        String message = arg1.toString();
				        message_is = msg.toString();
				        System.out.println("들어오는 메세지 ? 1 " + message_is);
				        			
				        break;	
				        			
				        			
		        	case "test/1" :
		        		System.out.println("들어오는 토픽 ? 2 " + topic);
		        		message_is = msg.toString();
		        		System.out.println("들어오는 메세지 ? 2 " + message_is);
		        					
		        		break;
		        		
		        	case "test/program" :
		        		System.out.println("들어오는 토픽 ? program " + topic);
		        		message_is = msg.toString();
		        		System.out.println("들어오는 메세지 ? program " + message_is);
		        		
		        		break;
		        		
		        	case "DMT01/browser" :
		        		System.out.println("\n\n들어오는 토픽 ? DMT01 " + topic);
		        		message_is = msg.toString();
		        		System.out.println("들어오는 메세지 ? DMT01 " + message_is);
		        		System.out.println("\n\nMessage to JSON");
		
		        		JSONParser parser = new JSONParser();
		        		Object obj = parser.parse(message_is);
		        		JSONObject jsonObj = (JSONObject)obj;
		        		
//		        		String order = (String)jsonObj.get("order");
//		        		String pgmNM = (String)jsonObj.get("pgmNM");
//	       				String pgmTYPE = (String)jsonObj.get("pgmTYPE");
//   					String pgm = (String)jsonObj.get("pgm");
 					
//   					System.out.println("0. order : " + order);
//						System.out.println("1. pgmNM : " + pgmNM);
//   					System.out.println("2. pgmTYPE : " + pgmTYPE);
//   					System.out.println("3. pgm : " + pgm);
		        		
		        		String cmd = (String)jsonObj.get("cmd");
		        		System.out.println("cmd : " + cmd);
     		
		        		JSONArray arr = (JSONArray)jsonObj.get("data");		        				
		        		for(int i=0 ; i<arr.size(); i++) {
		        			JSONObject ObjectInArray = (JSONObject)arr.get(i);
		        			String name = (String)ObjectInArray.get("name");
		        			System.out.println("name: " + name);
		        		}
 					
		        		break;
		        }
		        			
		        if(message_is.contentEquals(" ")) {
		        	System.out.println("MQTT 초기화 합니다.");
		        }else {
//		        	System.out.println("S result 1 : " + topic + ": " + msg.toString());
//			        MQTT_test_S = "[Subscribe] " + topic + ": " + msg.toString();
//			        System.out.println("S result 2 : " + MQTT_test_S);
			        			        			
			        Singleton MtoS = Singleton.getInstance();
			        MtoS.socket_send(message_is);
			        System.out.println("MQTT -> SOCKET");	
		        }
		        */
		    }
		   
     });	// callback
     
	}
}
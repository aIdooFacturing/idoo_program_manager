package com.unomic.dulink.chart.controller;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.OnError;
import javax.annotation.PostConstruct;
import javax.websocket.OnClose;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.simple.JSONObject;
import org.springframework.web.bind.annotation.SessionAttributes;

@ServerEndpoint(value="/echo")
public class WebSocketServer {
	
	public static String session_id;
	
//	@PostConstruct
//	public void init() throws MqttException {		
//		System.out.println("테스트 1");
//		MQTT_subscribe MS = new MQTT_subscribe();
//		MS.start();
//		System.out.println("테스트 2");
//		MS.init();
		
		
		/*
				MQTT_subscribe MS = new MQTT_subscribe();
				MS.init();
		*/
		
		
//		MS.start();
		
//		System.out.println("테스트 3");		
//		MQTT_publish MP = new MQTT_publish();
//		MP.init();
		
//		System.out.println(WebSocketServer.class.getResource("").getPath());
				
//		MQTT_file file = new MQTT_file();
//		String test_msg = file.test();
//		MP.init2("test/program", test_msg);
		
//		JSONObject jsonObject = new JSONObject();
//		jsonObject.put("order", "GETPGM");
//		jsonObject.put("pgmNM", "o2021");
//		jsonObject.put("pgmTYPE", "PGM");
////		jsonObject.put("pgm", test_msg);
//		MP.init3("DMT01", jsonObject);
//	}
	
	
	
	// HashSet : 데이터를 중복저장할 수 없고, 순서를 보장하지 않는다.
	private static Set<Session> clients = 
			Collections.synchronizedSet(new HashSet<Session>());
	
	// <1> 첫번째 수행 : 객체 생성 
//	public WebSocketServer() {
//		System.out.println("웹소켓(서버) 객체 생성 !");
//	}
	
	// 웹소켓 연결시 호출되는 이벤트
	// <2> 두번째 수행 : 웹소켓 연결 성공 
	@OnOpen
	public void onOpen(Session session) throws IOException, MqttException {
			
		Singleton singleton = Singleton.getInstance();
		singleton.session_test = session;
			
		System.out.println("opOpen-----------------------------------------");
		System.out.println(session.getId() + " : has opened a connection");
		System.out.println("웹소켓 세션 : " + session);
//		sesion_test = session;
		
		session_id = session.getId();
		
	
		
		// Set 에 session 추가..
		clients.add(session);
		
		System.out.println("싱글톤 세션 : " + singleton.session_test);
		
		
		System.out.println(clients.toString());
		
		String client_id[] = (session.toString()).split("@");
		System.out.println(client_id[1]);
		
		
		System.out.println("send tst");
//		singleton.socket_send("test msg");
		
		
		// throws MqttException 적은 이유 
	//	this.init();
	}
	
	
	// 웹 소켓이 닫히면 호출되는 이벤트 
		@OnClose
		public void onClose(Session session) {
			clients.remove(session);
		}
		
	// 웹 소켓으로부터 메시지가 오면 호출되는 이벤트 
	// msg : 웹에서 발신한 메세지 ("Hello WebSocket!")
	@OnMessage
	public static void onMessage(String msg, Session session) throws IOException {
		System.out.println(session.getId() + " : send message!!!!!!!!!!!!");
		System.out.println("[ " + msg + " ] from the client");

		// 세션 개수 체크해서 하나 이상이면 모든 세션에게 메세지를 보내야 함...
		if(clients.size()>1) {
			System.out.println("세션 수 체크 : " + clients.size());			
			for(Session client : clients) {
				client.getBasicRemote().sendText(msg);
			}
		}else {
			synchronized(clients) {
				for(Session client: clients) {
					if(!client.equals(session)) {
						client.getBasicRemote().sendText(msg);
					}
				}
			}		
		}
	}
	
	// 웹 소켓이 에러가 나면 호출되는 이벤트 
	@OnError
	public void onError(Throwable t) {
		System.out.println("ERRORRRRRRRRRRR : " + t.toString());
		t.printStackTrace();
	}
	
}